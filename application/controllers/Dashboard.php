<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('m_dashboard');
        
        $this->load->helper('url');
    }

    public function index()
    {
        $upt = $this->m_dashboard->get_upt();
        $data = array(
            'title' => 'Data dashboard',
            'upt' => $upt,
        );
        $this->my_theme('v_dashboard', $data);
    }

    // get data
    public function get_data($type)
    {
        $upt_id = $this->input->post('upt_id');
        $res = $this->m_dashboard->get_data($type, $upt_id);
        echo json_encode($res);
    }


    // get total user
    public function get_total_user()
    {
        $res = $this->m_dashboard->get_total_user();
        echo json_encode($res);
    }


    // get data all
    public function get_data_all()
    {
        // $upt = $this->m_dashboard->get_data_all();
        $upt = $this->m_dashboard->get_upt();
        $jenis = $this->input->get("jenis");
        $data_mentah = [];
        $list_upt = [];
        $data_aman = [
            "name" => 'Aman',
            "data" => [],
            "color" => "#1cc88a",
        ];
        $data_waspada = [
            "name" => 'Waspada',
            "data" => [],
            "color" => "#f6c23e",
        ];
        $data_kritis = [
            "name" => 'Kritis',
            "data" => [],
            "color" => "#e74a3b",
        ];
        $data_p0 = [
            "name" => 'P0',
            "data" => [],
            "color" => "#1cc88a",
        ];
        $data_p1 = [
            "name" => 'P1',
            "data" => [],
            "color" => "#f6c23e",
        ];
        $data_p2 = [
            "name" => 'P2',
            "data" => [],
            "color" => "#e74a3b",
        ];
        $data_bd = [
            "name" => '-',
            "data" => [],
            "color" => "#5a5c69",
        ];
        foreach ($upt as $k => $v) {
            $data_per_upt = $this->m_dashboard->get_data_upt($v->upt_id, $jenis);
            $list_upt[] = $v->upt_nama;
            $data_aman["data"][] = 0;
            $data_waspada["data"][] = 0;
            $data_kritis["data"][] = 0;
            $data_p0["data"][] = 0;
            $data_p1["data"][] = 0;
            $data_p2["data"][] = 0;
            $data_bd["data"][] = 0;
            foreach ($data_per_upt as $value) {
                if ($value->label == "Aman") {
                    $data_aman["data"][$k] = intval($value->value);
                } elseif ($value->label == "Waspada") {
                    $data_waspada["data"][$k] = intval($value->value);
                } elseif ($value->label == "Kritis") {
                    $data_kritis["data"][$k] = intval($value->value);
                } elseif ($value->label == "P0") {
                    $data_p0["data"][$k] = intval($value->value);
                } elseif ($value->label == "P1") {
                    $data_p1["data"][$k] = intval($value->value);
                }  elseif ($value->label == "P2") {
                    $data_p2["data"][$k] = intval($value->value);
                } else {
                    $data_bd["data"][$k] = intval($value->value);
                }
            }
        }
        $data_mentah = [$data_aman, $data_waspada, $data_kritis, $data_p0, $data_p1, $data_p2, $data_bd];

        // echo '<pre>';
        // print_r($list_upt);
        // print_r($data_mentah);
        // echo '</pre>';

        echo json_encode(['list_upt' => $list_upt, 'data' => $data_mentah]);
    }
}
