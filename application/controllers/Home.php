<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('m_krisis');
		$this->load->helper('url');
	}

	public function index()
	{
		
       $x = array(
            'judul' => 'Data dashboard',
        );
       $this->my_theme('v_home', $x);
	}
}
