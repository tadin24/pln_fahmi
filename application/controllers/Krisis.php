<?php

use phpDocumentor\Reflection\Types\This;

if (!defined('BASEPATH')) exit(header('Location:../'));
class Krisis extends MY_Controller
{
    private $filename = "import_data"; // Kita tentukan nama filenya
    function __construct()
    {
        parent::__construct();
        $this->load->model('m_krisis');
    }

    public function index()
    {
        $data['title'] = "Krisis";
        $data['upt'] = $this->m_krisis->get_upt();
        $data['ultg'] = $this->m_krisis->get_ultg();
        $data['opt_klali'] = $this->m_krisis->opt_reff(3);
        $data['opt_klapo'] = $this->m_krisis->opt_reff(1);
        $data['opt_klahu'] = $this->m_krisis->opt_reff(4);
        $data['opt_kategori'] = $this->m_krisis->opt_reff(5);
        $data['opt_prioritas'] = $this->m_krisis->opt_reff(6);
        $data['penghantar'] = $this->m_krisis->get_penghantar();
        $this->my_theme('v_krisis', $data);
    }


    // save
    public function save()
    {
        $data = $this->input->post();
        $data['tgl'] = date('Y-m-d', strtotime($this->input->post('tgl')));
        $data['update'] = date('Y-m-d', strtotime($this->input->post('update')));
        $data['skoli'] = !empty($this->input->post('skoli')) ? $this->input->post('skoli') : 0;
        $data['skopo'] = !empty($this->input->post('skopo')) ? $this->input->post('skopo') : 0;
        $data['skohu'] = !empty($this->input->post('skohu')) ? $this->input->post('skohu') : 0;
        unset($data['kkp_file']);
        unset($data['kelling_file']);
        unset($data['kelpo_file']);
        unset($data['kelfo_file']);
        unset($data['kelrab_file']);
        unset($data['kelde_file']);
        unset($data['foto1']);
        unset($data['foto2']);
        unset($data['foto3']);
        unset($data['foto4']);
        unset($data['act']);
        if ($this->input->post('act') == 'add') {
            unset($data['id_krisis']);
            $res = $this->m_krisis->insert($data);
        } else {
            $res = $this->m_krisis->update($data);
        }

        $count = count($_FILES);

        if ($count > 0) {
            $this->load->library('upload');
            foreach ($_FILES as $key => $val) {
                $config = [];
                if ($key == 'foto1' || $key == 'foto2' || $key == 'foto3' || $key == 'foto4') {
                    $config['allowed_types'] = 'jpg|jpeg|png|gif';
                    switch ($key) {
                        case 'foto1':
                            $config['file_name'] = $res . "_1";
                            break;
                        case 'foto2':
                            $config['file_name'] = $res . "_2";
                            break;
                        case 'foto3':
                            $config['file_name'] = $res . "_3";
                            break;
                        case 'foto4':
                            $config['file_name'] = $res . "_4";
                            break;
                        default:
                            break;
                    }
                    $config['upload_path'] = 'assets/images/';
                } else {
                    $config['allowed_types'] = 'doc|docx|pdf|xlsx';
                    $config['file_name'] = $key . "_" . $res;
                    $config['upload_path'] = 'assets/documents/';
                }

                $this->upload->initialize($config);

                if ($this->input->post('act') == 'edit') {
                    $krisis = $this->m_krisis->get_file($key, $res);
                    if ($key == 'foto1' || $key == 'foto2' || $key == 'foto3' || $key == 'foto4') {
                        if (!empty($krisis[$key])) {
                            if (file_exists('assets/images/' . $krisis[$key])) {
                                unlink('assets/images/' . $krisis[$key]);
                            }
                        }
                    } else {
                        if (!empty($krisis[$key])) {
                            if (file_exists('assets/documents/' . $krisis[$key])) {
                                unlink('assets/documents/' . $krisis[$key]);
                            }
                        }
                    }
                }


                if ($this->upload->do_upload($key)) {
                    $uploadData = $this->upload->data();

                    $filename = $uploadData['file_name'];
                    $data = [
                        'id_krisis' => $res,
                        $key => $filename,
                    ];
                    $res = $this->m_krisis->update($data);
                }
            }
        }
        echo json_encode($res);
    }


    // get data krisis by id
    public function edit($id)
    {
        $res = null;
        if ($id > 0 && !empty($id)) {
            $res = $this->m_krisis->edit($id);
            $res->tgl = date('d-m-Y', strtotime($res->tgl));
            $res->update = date('d-m-Y', strtotime($res->update));
        }

        echo json_encode($res);
    }


    // get data krisis
    public function get_data()
    {
        $columns = [
            "id_krisis",
            "id_krisis",
            "upt_nama",
            "penghantar",
            "tower",
            "k.tgl",
            "k.update",
            "k.prioritas",
            "k.penanganan",

        ];
        $columns_search = [
            "penghantar",
            "tower",
        ];
        $draw = intval($this->input->post("draw"));
        $where     = "";

        $search = $this->input->post("search");
        $search = $search['value'];

        $upt_kel_id = $this->session->userdata('upt_kel_id');
        if ($upt_kel_id != "0") {
            $where .= " AND k.upt = $upt_kel_id ";
        }

        if (isset($search) && $search != "") {
            $where .= "AND (";
            for ($i = 0; $i < count($columns_search); $i++) {
                $where .= " LOWER(" . $columns_search[$i] . ") LIKE LOWER('%" . ($search) . "%') OR ";
            }
            $where = substr_replace($where, "", -3);
            $where .= ')';
        }
        $totalRecords = $this->m_krisis->get_total($where);
        $length = intval($this->input->post("length"));
        $length = $length < 0 ? $totalRecords : $length;
        $start = intval($this->input->post("start"));
        if (isset($start) && $length != '-1') {
            $limit = "limit " . intval($length) . " offset " . intval($start);
        }
        $records = array();
        $records["data"] = array();
        $order = $this->input->post("order");
        $col = 0;
        $dir = "";
        if (!empty($order)) {
            foreach ($order as $o) {
                $col = $o['column'];
                $dir = $o['dir'];
            }
        }

        if ($dir != "asc" && $dir != "desc") {
            $dir = "desc";
        }

        if (!isset($columns[$col])) {
            $order = null;
        } else {
            $order = "ORDER BY " . $columns[$col] . " " . $dir;
        }

        $data = $this->m_krisis->get_data($columns, $where, $order, $limit);
        $no   = 1 + $start;
        foreach ($data as $row) {
            $action = "";
            $id = $row->id_krisis;
            // $isi = "$id|$row->group_name|$row->group_aktif";

            $action = '<a href="javascript:;" onclick="set_val(\'' . $id . '\')" class="btn btn-info btn-sm" title="Edit"><i class="fa fa-pencil-alt"></i></a>&nbsp;<a href="javascript:;" onclick="del(\'' . $id . '\')" class="btn btn-danger btn-sm" title="Hapus"><i class="fa fa-trash"></i></a>&nbsp;<a href="' . base_url('Krisis/details/' . $id) . '" class="btn btn-success btn-sm" title="Rincian"><i class="fa fa-eye"></a>';
            $records["data"][] = array(
                $no++,
                '<div class="form-check"><input class="form-check-input input-check position-static" type="checkbox" data-id="' . $id . '" onchange="inputCheckOnChange(this)"></div>',
                $row->upt_nama,
                $row->penghantar,
                $row->tower,
                date("d-m-Y", strtotime($row->tgl)),
                date("d-m-Y", strtotime($row->update)),
                ($row->prioritas != '17' ? ($row->prioritas != '18' ? ($row->prioritas != '19' ? "P0" : "P1") : "P2") : "-"),

                $row->penanganan,
                $action,

            );
        }

        $records["draw"] = $draw;
        $records["recordsTotal"] = $totalRecords;
        $records["recordsFiltered"] = $totalRecords;

        echo json_encode($records);
    }


    // delete
    public function delete($id, $is_banyak = 0)
    {
        $res = $this->m_krisis->delete($id);
        if ($is_banyak == 0) {
            echo json_encode($res);
        } else {
            return $res;
        }
    }



    public function details($id)
    {
        $data = $this->m_krisis->data_details($id);
        $x = array(
            'title' => 'Details Data',
            'data' => $data
        );
        $this->my_theme('v_details', $x);
    }

    public function form()
    {
        $data = array('judul' => 'Import Data'); // Buat variabel $data sebagai array

        if (isset($_POST['preview'])) { // Jika user menekan tombol Preview pada form
            // lakukan upload file dengan memanggil function upload yang ada di SiswaModel.php
            // $upload = $this->m_krisis->upload_file($this->filename);

            $this->load->library('upload'); // Load librari upload

            $config['upload_path'] = './excel/';
            $config['allowed_types'] = 'xlsx';
            $config['max_size']  = '2048';
            $config['overwrite'] = true;
            $config['file_name'] = $this->filename;

            $this->upload->initialize($config); // Load konfigurasi uploadnya
            if ($this->upload->do_upload('file_excel')) { // Lakukan upload dan Cek jika proses upload berhasil
                // Jika berhasil :
                include APPPATH . 'third_party/PHPExcel/PHPExcel.php';

                $excelreader = new PHPExcel_Reader_Excel2007();
                $loadexcel = $excelreader->load('excel/' . $this->filename . '.xlsx'); // Load file yang tadi diupload ke folder excel
                $sheet = $loadexcel->getActiveSheet()->toArray(null, true, true, true);

                // Masukan variabel $sheet ke dalam array data yang nantinya akan di kirim ke file form.php
                // Variabel $sheet tersebut berisi data-data yang sudah diinput di dalam excel yang sudha di upload sebelumnya
                $data['sheet'] = $sheet;
            } else {
                // Jika gagal :
                $data['upload_error'] = $this->upload->display_errors(); // Ambil pesan error uploadnya untuk dikirim ke file form dan ditampilkan
            }
        }

        $this->my_theme('v_form', $data);
    }

    public function import()
    {
        // Load plugin PHPExcel nya
        include APPPATH . 'third_party/PHPExcel/PHPExcel.php';

        $upt_data = $this->m_krisis->get_upt();
        $ultg_data = $this->m_krisis->get_ultg();
        $opt_klali_data = $this->m_krisis->opt_reff(3);
        $opt_klapo_data = $this->m_krisis->opt_reff(1);
        $opt_klahu_data = $this->m_krisis->opt_reff(4);
        $opt_kategori_data = $this->m_krisis->opt_reff(5);
        $opt_prioritas_data = $this->m_krisis->opt_reff(6);
        $opt_penghantar_data = $this->m_krisis->get_penghantar();
        $upt = $ultg = $opt_klali = $opt_klapo = $opt_klahu = $opt_kategori = $opt_penghantar = null;

        foreach ($upt_data as $value) {
            $opt_upt[strtolower($value->upt_nama)] = $value->upt_id;
        }

        foreach ($ultg_data as $value) {
            $opt_ultg[strtolower($value->ultg_nama)] = $value->ultg_id;
        }

        foreach ($opt_klali_data as $value) {
            $opt_klali[strtolower($value->reff_nama)] = $value->reff_id;
        }

        foreach ($opt_klapo_data as $value) {
            $opt_klapo[strtolower($value->reff_nama)] = $value->reff_id;
        }

        foreach ($opt_klahu_data as $value) {
            $opt_klahu[strtolower($value->reff_kode)] = $value->reff_id;
        }

        foreach ($opt_kategori_data as $value) {
            $opt_kategori[strtolower($value->reff_nama)] = $value->reff_id;
        }

        foreach ($opt_prioritas_data as $value) {
            $opt_prioritas[strtolower($value->reff_nama)] = $value->reff_id;
        }

        foreach ($opt_penghantar_data as $value) {
            $opt_penghantar[strtolower($value->penghantar_nama)] = $value->penghantar_id;
        }

        $excelreader = new PHPExcel_Reader_Excel2007();
        $loadexcel = $excelreader->load('excel/' . $this->filename . '.xlsx'); // Load file yang telah diupload ke folder excel
        $sheet = $loadexcel->getActiveSheet()->toArray(null, true, true, true);

        // Buat sebuah variabel array untuk menampung array data yg akan kita insert ke database
        $data = array();

        $numrow = 1;
        foreach ($sheet as $row) {
            // Cek jika semua data tidak diisi
            if ($row['A'] == "" && $row['A'] == "" && $row["D"] == "" && $row["E"] == "") {
                continue; // Lewat data pada baris ini (masuk ke looping selanjutnya / baris selanjutnya)
            }
            // Cek $numrow apakah lebih dari 1
            // Artinya karena baris pertama adalah nama-nama kolom
            // Jadi dilewat saja, tidak usah diimport
            if ($numrow > 3 && $numrow < count($sheet) - 5) {
                // Kita push (add) array data ke variabel data
                $klahu = $klapo = $klali = $kategori = $upt = $ultg = 0;
                $penghantar = !empty($opt_penghantar[strtolower($row['F'])]) ? $opt_penghantar[strtolower($row['F'])] : 0;

                if (!empty($opt_klali[strtolower($row["U"])])) {
                    $klali = $opt_klali[strtolower($row["U"])];
                } else {
                    $klali = $opt_klali["belum diasesment"];
                }

                if (!empty($opt_klapo[strtolower($row["V"])])) {
                    $klapo = $opt_klapo[strtolower($row["V"])];
                } else {
                    $klapo = $opt_klapo["belum diasesment"];
                }

                if (!empty($opt_klahu[strtolower($row["W"])])) {
                    $klahu = $opt_klahu[strtolower($row["W"])];
                } else {
                    $klahu = $opt_klahu["-"];
                }

                if (!empty($opt_kategori[strtolower($row["X"])])) {
                    $kategori = $opt_kategori[strtolower($row["X"])];
                } else {
                    $kategori = $opt_kategori["belum diasesment"];
                }

                 if (!empty($opt_prioritas[strtolower($row["I"])])) {
                    $prioritas = $opt_prioritas[strtolower($row["I"])];
                } 

                if (!empty($opt_upt[strtolower($row["D"])])) {
                    $upt = $opt_upt[strtolower($row["D"])];
                    // } else {
                    //     $upt = $opt_upt["belum diasesment"];
                }

                if (!empty($opt_ultg[strtolower($row["E"])])) {
                    $ultg = $opt_ultg[strtolower($row["E"])];
                    // } else {
                    //     $ultg = $opt_ultg["belum diasesment"];
                }

                array_push($data, array(
                    'tgl' => date("Y-m-d", strtotime($row['A'])),
                    'update' => date("Y-m-d", strtotime($row['A'])),
                    'no_folder' => $row['B'],
                    'no_reg' => $row['C'],
                    'upt' => $upt,
                    'ultg' => $ultg,
                    'penghantar' => $row['F'],
                    'penghantar_id' => $penghantar,
                    'kv' => $row['G'],
                    'tower' => $row['H'],
                    'prioritas' => $prioritas,
                    'lat' => $row['J'],
                    'lang' => $row['K'],
                    'kkp' => $row['L'],
                    'kelling' => $row['M'],
                    'kelpo' => $row['N'],
                    'kelfo' => $row['O'],
                    'kelrab' => $row['P'],
                    'kelde' => $row['Q'],
                    'skoli' => $row['R'],
                    'skopo' => $row['S'],
                    'skohu' => $row['T'],
                    'klali' => $klali,
                    'klapo' => $klapo,
                    'klahu' => $klahu,
                    'kategori' => $kategori,
                    'anomali' => $row['Y'],
                    'keterangan' => $row['Z'],
                    'tautan' => $row['AA'],
                    'penanganan' => $row['AB'],
                    'risiko' => $row['AC'],
                    'mitigasi' => $row['AD']
                    // 'gi' => $row['AD'],
                    // 'bay' => $row['AE'],
                    // 'pht' => $row['AF'],
                    // 'status_alat' => $row['AG'],
                    // 'kode_sirkit' => $row['AH'],
                    // 'kode_pst_old' => $row['AI'],
                    // 'kd_status' => $row['AJ'],
                    // 'buatan' => $row['AK'],
                    // 'tahun_buat' => $row['AL'],
                    // 'tgl_operasi' => date("Y-m-d", strtotime(str_replace("/", "-", $row['AM']))),
                    // 'luas_tpk' => $row['AN'],
                    // 'posisi_tower' => $row['AO'],
                    // 'rtrw' => $row['AP'],
                    // 'kelurahan' => $row['AQ'],
                    // 'kecamatan' => $row['AR'],
                    // 'kabupaten' => $row['AS'],
                    // 'propinsi' => $row['AT'],
                    // 'keterangan_tower' => $row['AU'],
                    // 'merk' => $row['AV'],
                    // 'tipe_tower' => $row['AW'],
                    // 'jenis' => $row['AX'],
                    // 'jenis_material' => $row['AY'],
                    // 'jenis_pelapis' => $row['AZ'],
                    // 'diameter' => $row['BA'],
                    // 'jarak_kaki' => $row['BB'],
                    // 'sudut_stub' => $row['BC'],
                    // 'tinggi_body' => $row['BD'],
                    // 'pjg_crossarm' => $row['BE'],
                    // 'jarak_fasa' => $row['BF'],
                    // 'jml_kawat' => $row['BG'],
                    // 'berat_total' => $row['BH'],
                    // 'isolator' => $row['BI'],
                    // 'kode_teg' => $row['BJ'],
                    // 'nomor_aset' => $row['BK'],
                    // 'cons_type' => $row['BL'],
                    // 'teg_oprs' => $row['BM'],
                    // 'techidentno' => $row['BN'],
                    // 'asset' => $row['BO'],
                    // 'eq_number' => $row['BP'],
                    // 'kode_pst' => $row['BQ'],
                    // 'serial_id' => $row['BR'],
                    // 'konstruksi' => $row['BS'],
                    // 'equipment_number' => $row['BT'],
                    // 'id_functloc' => $row['BU'],
                    // 'id_bay' => $row['BV'],
                    // 'wil_kerja' => $row['BW'],
                    // 'tipe_isolator' => $row['BX'],
                    // 'tipe' => $row['BY'],
                    // 'phasa' => $row['BZ'],
                    // 'penempatan' => $row['CA']

                ));
            }

            $numrow++; // Tambah 1 setiap kali looping
        }
        // echo "<pre>";
        // print_r($numrow);
        // print_r($opt_upt);
        // print_r($opt_ultg);
        // print_r($data);
        // echo "</pre>";
        // die;
        // Panggil fungsi insert_multiple yg telah kita buat sebelumnya di model
        $this->m_krisis->insert_multiple($data);

        redirect("krisis"); // Redirect ke halaman awal (ke controller siswa fungsi index)
    }

    public function export()
    {
        // Load plugin PHPExcel nya
        include APPPATH . 'third_party/PHPExcel/PHPExcel.php';

        // Panggil class PHPExcel nya
        $excel = new PHPExcel();

        // Settingan awal fil excel
        $excel->getProperties()->setCreator('PLN UIT JBTB UNIT INDUK')
            ->setLastModifiedBy('Fahmi')
            ->setTitle("Usulan Penanganan Tower Kritis 2021 UIT JBTB")
            ->setSubject("Tower Krisis")
            ->setDescription("Laporan Semua Data Tower Krisis")
            ->setKeywords("Data Tower");

        // Buat sebuah variabel untuk menampung pengaturan style dari header tabel
        $style_col = array(
            'font' => array('bold' => true), // Set font nya jadi bold
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
            ),
            'borders' => array(
                'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
                'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
                'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
                'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
            )
        );

        // Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
        $style_row = array(
            'alignment' => array(
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
            ),
            'borders' => array(
                'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
                'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
                'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
                'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
            )
        );

        $excel->setActiveSheetIndex(0)->setCellValue('A1', "Usulan Penanganan Tower Kritis 2021 UIT JBTB"); // Set kolom A1 dengan tulisan 
        $excel->setActiveSheetIndex(0)->setCellValue('K3', "Kelengkapan Dokumen"); // Set kolom A1 dengan tulisan 
        $excel->setActiveSheetIndex(0)->setCellValue('Q3', "Skor"); // Set kolom A1 dengan tulisan
        $excel->setActiveSheetIndex(0)->setCellValue('T3', "Klafisikasi"); // Set kolom A1 dengan tulisan 
        $excel->setActiveSheetIndex(0)->setCellValue('AA3', "Diisi Oleh UPT"); // Set kolom A1 dengan tulisan 
        $excel->getActiveSheet()->mergeCells('A1:AC1'); // Set Merge Cell pada kolom A1 sampai E1
        $excel->getActiveSheet()->mergeCells('K3:P3'); // Set Merge Cell pada kolom A1 sampai E1
        $excel->getActiveSheet()->mergeCells('Q3:S3'); // Set Merge Cell pada kolom A1 sampai E1
        $excel->getActiveSheet()->mergeCells('T3:V3'); // Set Merge Cell pada kolom A1 sampai E1
        $excel->getActiveSheet()->mergeCells('AA3:AC3'); // Set Merge Cell pada kolom A1 sampai E1
        $excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
        $excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
        $excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1
        $excel->getActiveSheet()->getStyle('K3')->getFont()->setBold(TRUE); // Set bold kolom A1
        $excel->getActiveSheet()->getStyle('K3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1
        $excel->getActiveSheet()->getStyle('Q3')->getFont()->setBold(TRUE); // Set bold kolom A1
        $excel->getActiveSheet()->getStyle('Q3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1
        $excel->getActiveSheet()->getStyle('T3')->getFont()->setBold(TRUE); // Set bold kolom A1
        $excel->getActiveSheet()->getStyle('T3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1
        $excel->getActiveSheet()->getStyle('AA3')->getFont()->setBold(TRUE); // Set bold kolom A1
        $excel->getActiveSheet()->getStyle('AA3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1

        // Buat header tabel nya pada baris ke 3
        $excel->setActiveSheetIndex(0)->setCellValue('A4', "Tanggal");
        $excel->setActiveSheetIndex(0)->setCellValue('B4', "No Folder");
        $excel->setActiveSheetIndex(0)->setCellValue('C4', "No Regional");
        $excel->setActiveSheetIndex(0)->setCellValue('D4', "UPT");
        $excel->setActiveSheetIndex(0)->setCellValue('E4', "ULTG");
        $excel->setActiveSheetIndex(0)->setCellValue('F4', "Penghantar");
        $excel->setActiveSheetIndex(0)->setCellValue('G4', "KV");
        $excel->setActiveSheetIndex(0)->setCellValue('H4', "Tower");
         $excel->setActiveSheetIndex(0)->setCellValue('I4', "Prioritas");
        $excel->setActiveSheetIndex(0)->setCellValue('J4', "Koordinat X");
        $excel->setActiveSheetIndex(0)->setCellValue('K4', "Koordinat Y");
        $excel->setActiveSheetIndex(0)->setCellValue('L4', "KKP");
        $excel->setActiveSheetIndex(0)->setCellValue('M4', "Assesmen Lingkungan");
        $excel->setActiveSheetIndex(0)->setCellValue('N4', "Assesmen Pondasi");
        $excel->setActiveSheetIndex(0)->setCellValue('O4', "Foto");
        $excel->setActiveSheetIndex(0)->setCellValue('P4', "RAB");
        $excel->setActiveSheetIndex(0)->setCellValue('Q4', "Desain");
        $excel->setActiveSheetIndex(0)->setCellValue('R4', "Assesmen Lingkungan");
        $excel->setActiveSheetIndex(0)->setCellValue('S4', "Assesmen Pondasi");
        $excel->setActiveSheetIndex(0)->setCellValue('T4', "Sifat Hujan");
        $excel->setActiveSheetIndex(0)->setCellValue('U4', "Ancaman Lingkungan");
        $excel->setActiveSheetIndex(0)->setCellValue('V4', "Ancaman Pondasi");
        $excel->setActiveSheetIndex(0)->setCellValue('W4', "Sifat Hujan");
        $excel->setActiveSheetIndex(0)->setCellValue('X4', "Kategori Penanganan");
        $excel->setActiveSheetIndex(0)->setCellValue('Y4', "Anomali");
        $excel->setActiveSheetIndex(0)->setCellValue('Z4', "Keterangan");
        $excel->setActiveSheetIndex(0)->setCellValue('AA4', "Tautan");
        $excel->setActiveSheetIndex(0)->setCellValue('AB4', "Penanganan Sementara");
        $excel->setActiveSheetIndex(0)->setCellValue('AC4', "Risiko");
        $excel->setActiveSheetIndex(0)->setCellValue('AD4', "Mitigasi Risiko");


        // Apply style header yang telah kita buat tadi ke masing-masing kolom header
        $excel->getActiveSheet()->getStyle('A4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('B4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('C4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('D4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('E4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('F4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('G4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('H4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('I4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('J4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('K4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('L4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('M4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('N4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('O4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('P4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('Q4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('R4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('S4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('T4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('U4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('V4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('W4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('X4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('Y4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('Z4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('AA4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('AB4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('AC4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('AD4')->applyFromArray($style_col);


        $x = $this->m_krisis->view();

        $no = 1; // Untuk penomoran tabel, di awal set dengan 1
        $numrow = 5; // Set baris pertama untuk isi tabel adalah baris ke 4


        foreach ($x as $data) {
            $excel->setActiveSheetIndex(0)->setCellValue('A' . $numrow, date("d-m-Y", strtotime($data->tgl)));
            $excel->setActiveSheetIndex(0)->setCellValue('B' . $numrow, $data->no_folder);
            $excel->setActiveSheetIndex(0)->setCellValue('C' . $numrow, $data->no_reg);
            $excel->setActiveSheetIndex(0)->setCellValue('D' . $numrow, $data->upt_nama);
            $excel->setActiveSheetIndex(0)->setCellValue('E' . $numrow, $data->ultg_nama);
            $excel->setActiveSheetIndex(0)->setCellValue('F' . $numrow, $data->penghantar);
            $excel->setActiveSheetIndex(0)->setCellValue('G' . $numrow, $data->kv);
            $excel->setActiveSheetIndex(0)->setCellValue('H' . $numrow, $data->tower);
             $excel->setActiveSheetIndex(0)->setCellValue('I' . $numrow, $data->prioritas);
            $excel->setActiveSheetIndex(0)->setCellValue('J' . $numrow, $data->lat);
            $excel->setActiveSheetIndex(0)->setCellValue('K' . $numrow, $data->lang);
            $excel->setActiveSheetIndex(0)->setCellValue('L' . $numrow, $data->kkp);
            $excel->setActiveSheetIndex(0)->setCellValue('M' . $numrow, $data->kelling);
            $excel->setActiveSheetIndex(0)->setCellValue('N' . $numrow, $data->kelpo);
            $excel->setActiveSheetIndex(0)->setCellValue('O' . $numrow, $data->kelfo);
            $excel->setActiveSheetIndex(0)->setCellValue('P' . $numrow, $data->kelrab);
            $excel->setActiveSheetIndex(0)->setCellValue('Q' . $numrow, $data->kelde);
            $excel->setActiveSheetIndex(0)->setCellValue('R' . $numrow, $data->skoli);
            $excel->setActiveSheetIndex(0)->setCellValue('S' . $numrow, $data->skopo);
            $excel->setActiveSheetIndex(0)->setCellValue('T' . $numrow, $data->skohu);
            $excel->setActiveSheetIndex(0)->setCellValue('U' . $numrow, ($data->klali != '0' ? ($data->klali != '1' ? ($data->klali != '2' ? "KRITIS" : "WASPADA") : "AMAN") : "-"));
            $excel->setActiveSheetIndex(0)->setCellValue('V' . $numrow, ($data->klapo != '0' ? ($data->klapo != '1' ? ($data->klapo != '2' ? "KRITIS" : "WASPADA") : "AMAN") : "-"));
            $excel->setActiveSheetIndex(0)->setCellValue('W' . $numrow, ($data->klahu != '0' ? ($data->klahu != '1' ? ($data->klahu != '2' ? "A" : "N") : "B") : "-"));
            $excel->setActiveSheetIndex(0)->setCellValue('X' . $numrow, ($data->kategori != '0' ? ($data->kategori != '1' ? ($data->kategori != '2' ? "KRITIS" : "WASPADA") : "AMAN") : "-"));
            $excel->setActiveSheetIndex(0)->setCellValue('Y' . $numrow, $data->anomali);
            $excel->setActiveSheetIndex(0)->setCellValue('Z' . $numrow, $data->keterangan);
            $excel->setActiveSheetIndex(0)->setCellValue('AA' . $numrow, $data->tautan);
            $excel->setActiveSheetIndex(0)->setCellValue('AB' . $numrow, $data->penanganan);
            $excel->setActiveSheetIndex(0)->setCellValue('AC' . $numrow, $data->risiko);
            $excel->setActiveSheetIndex(0)->setCellValue('AD' . $numrow, $data->mitigasi);

            // Apply style row yang telah kita buat tadi ke masing-masing baris (isi tabel)
            $excel->getActiveSheet()->getStyle('A' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('B' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('C' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('D' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('E' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('F' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('G' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('H' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('I' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('J' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('K' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('L' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('M' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('N' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('O' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('P' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('Q' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('R' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('S' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('T' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('U' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('V' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('W' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('X' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('Y' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('Z' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('AA' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('AB' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('AC' . $numrow)->applyFromArray($style_row);

            $no++; // Tambah 1 setiap kali looping
            $numrow++; // Tambah 1 setiap kali looping
        }

        // Set width kolom
        $excel->getActiveSheet(3)->getStyle('K3:V3')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $excel->getActiveSheet(0)->getStyle('A4:Z4')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $excel->getActiveSheet(0)->getStyle('A4:Z4')->getFill()->getStartColor()->setRGB('1E90FF');
        $excel->getActiveSheet(0)->getStyle('K3:V3')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $excel->getActiveSheet(0)->getStyle('K3:V3')->getFill()->getStartColor()->setRGB('1E90FF');
        $excel->getActiveSheet(0)->getStyle('AA3:AC3')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $excel->getActiveSheet(0)->getStyle('AA3:AC3')->getFill()->getStartColor()->setRGB('00FF00');
        $excel->getActiveSheet(0)->getStyle('AA4:AC4')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $excel->getActiveSheet(0)->getStyle('AA4:AC4')->getFill()->getStartColor()->setRGB('00FF00');
        $excel->getActiveSheet()->getRowDimension('4')->setRowHeight('80');
        $excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true); // Set width kolom A
        $excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true); // Set width kolom B
        $excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true); // Set width kolom C
        $excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true); // Set width kolom D
        $excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true); // Set width kolom E
        $excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true); // Set width kolom A
        $excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true); // Set width kolom B
        $excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true); // Set width kolom C
        $excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true); // Set width kolom D
        $excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true); // Set width kolom E
        $excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true); // Set width kolom D
        $excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true); // Set width kolom E
        $excel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true); // Set width kolom A
        $excel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true); // Set width kolom B
        $excel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true); // Set width kolom C
        $excel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true); // Set width kolom D
        $excel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true); // Set width kolom E
        $excel->getActiveSheet()->getColumnDimension('R')->setAutoSize(true); // Set width kolom D
        $excel->getActiveSheet()->getColumnDimension('S')->setAutoSize(true); // Set width kolom E
        $excel->getActiveSheet()->getColumnDimension('T')->setAutoSize(true); // Set width kolom A
        $excel->getActiveSheet()->getColumnDimension('U')->setAutoSize(true); // Set width kolom B
        $excel->getActiveSheet()->getColumnDimension('V')->setAutoSize(true); // Set width kolom C
        $excel->getActiveSheet()->getColumnDimension('W')->setAutoSize(true); // Set width kolom D
        $excel->getActiveSheet()->getColumnDimension('X')->setAutoSize(true); // Set width kolom E
        $excel->getActiveSheet()->getColumnDimension('Y')->setAutoSize(true); // Set width kolom D
        $excel->getActiveSheet()->getColumnDimension('Z')->setAutoSize(true); // Set width kolom E
        $excel->getActiveSheet()->getColumnDimension('AA')->setAutoSize(true); // Set width kolom A
        $excel->getActiveSheet()->getColumnDimension('AB')->setAutoSize(true); // Set width kolom E
        $excel->getActiveSheet()->getColumnDimension('AC')->setAutoSize(true); // Set width kolom A

        // Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
        $excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);

        // Set orientasi kertas jadi LANDSCAPE
        $excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);

        // Set judul file excel nya
        $excel->getActiveSheet(0)->setTitle("Laporan Data");
        $excel->setActiveSheetIndex(0);

        // Proses file excel
        ob_end_clean();
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="Data Tower.xlsx"'); // Set nama file excel nya
        header('Cache-Control: max-age=0');

        $write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
        $write->save('php://output');
    }

    public function export_map()
    {
        // Load plugin PHPExcel nya
        include APPPATH . 'third_party/PHPExcel/PHPExcel.php';

        // Panggil class PHPExcel nya
        $excel = new PHPExcel();

        // Settingan awal fil excel
        $excel->getProperties()->setCreator('PLN UIT JBTB UNIT INDUK')
            ->setLastModifiedBy('Fahmi')
            ->setTitle("Koordinat Tower Kritis 2021 UIT JBTB")
            ->setSubject("Tower Kritis")
            ->setDescription("Laporan Semua Data Tower Kritis")
            ->setKeywords("Data Tower");

        // Buat sebuah variabel untuk menampung pengaturan style dari header tabel
        $style_col = array(
            'font' => array('bold' => true), // Set font nya jadi bold
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
            ),
            'borders' => array(
                'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
                'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
                'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
                'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
            )
        );

        // Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
        $style_row = array(
            'alignment' => array(
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
            ),
            'borders' => array(
                'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
                'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
                'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
                'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
            )
        );

        $excel->setActiveSheetIndex(0)->setCellValue('A1', "Data Koordinat Tower Kritis"); // Set kolom A1 dengan tulisan 
        $excel->getActiveSheet()->mergeCells('A1:AC1'); // Set Merge Cell pada kolom A1 sampai E1
        $excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
        $excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
        $excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A

        // Buat header tabel nya pada baris ke 3
        $excel->setActiveSheetIndex(0)->setCellValue('A4', "UPT");
        $excel->setActiveSheetIndex(0)->setCellValue('B4', "ULTG");
        $excel->setActiveSheetIndex(0)->setCellValue('C4', "GI");
        $excel->setActiveSheetIndex(0)->setCellValue('D4', "BAY");
        $excel->setActiveSheetIndex(0)->setCellValue('E4', "PHT");
        $excel->setActiveSheetIndex(0)->setCellValue('F4', "Status Alat");
        $excel->setActiveSheetIndex(0)->setCellValue('G4', "KODE_SIRKIT");
        $excel->setActiveSheetIndex(0)->setCellValue('H4', "KODE_PST_OLD");
        $excel->setActiveSheetIndex(0)->setCellValue('I4', "KD_STATUS");
        $excel->setActiveSheetIndex(0)->setCellValue('J4', "NO_TOWER");
        $excel->setActiveSheetIndex(0)->setCellValue('K4', "BUATAN");
        $excel->setActiveSheetIndex(0)->setCellValue('L4', "THN_BUAT");
        $excel->setActiveSheetIndex(0)->setCellValue('M4', "TGL_OPRS");
        $excel->setActiveSheetIndex(0)->setCellValue('N4', "KOORDINAT X");
        $excel->setActiveSheetIndex(0)->setCellValue('O4', "KOORDINAT Y");
        $excel->setActiveSheetIndex(0)->setCellValue('P4', "LUAS_TPK");
        $excel->setActiveSheetIndex(0)->setCellValue('Q4', "POSISI_TOWER");
        $excel->setActiveSheetIndex(0)->setCellValue('R4', "RT_RW");
        $excel->setActiveSheetIndex(0)->setCellValue('S4', "KELURAHAN");
        $excel->setActiveSheetIndex(0)->setCellValue('T4', "KECAMATAN");
        $excel->setActiveSheetIndex(0)->setCellValue('U4', "KAB_KODYA");
        $excel->setActiveSheetIndex(0)->setCellValue('V4', "PROPINSI");
        $excel->setActiveSheetIndex(0)->setCellValue('W4', "KETERANGAN");
        $excel->setActiveSheetIndex(0)->setCellValue('X4', "MERK");
        $excel->setActiveSheetIndex(0)->setCellValue('Y4', "TIPE_TOWER");
        $excel->setActiveSheetIndex(0)->setCellValue('Z4', "JENIS_TOWER");
        $excel->setActiveSheetIndex(0)->setCellValue('AA4', "JENIS_MATERIAL");
        $excel->setActiveSheetIndex(0)->setCellValue('AB4', "JENIS_PELAPIS");
        $excel->setActiveSheetIndex(0)->setCellValue('AC4', "DIAMETER");
        $excel->setActiveSheetIndex(0)->setCellValue('AD4', "JARAK_ANTAR_KAKI");
        $excel->setActiveSheetIndex(0)->setCellValue('AE4', "SUDUT_STUB");
        $excel->setActiveSheetIndex(0)->setCellValue('AF4', "TINGGI_BODY");
        $excel->setActiveSheetIndex(0)->setCellValue('AG4', "PJG_CROSSARM");
        $excel->setActiveSheetIndex(0)->setCellValue('AH4', "JARAK_ANTAR_FASA");
        $excel->setActiveSheetIndex(0)->setCellValue('AI4', "JUMLAH_KAWAT_TANAH");
        $excel->setActiveSheetIndex(0)->setCellValue('AJ4', "BERAT_TOTAL");
        $excel->setActiveSheetIndex(0)->setCellValue('AK4', "ISOLATOR_PER_PHASA");
        $excel->setActiveSheetIndex(0)->setCellValue('AL4', "KODE_TEG");
        $excel->setActiveSheetIndex(0)->setCellValue('AM4', "NOMOR_ASET");
        $excel->setActiveSheetIndex(0)->setCellValue('AN4', "CONS_TYPE");
        $excel->setActiveSheetIndex(0)->setCellValue('AO4', "TEGS_OPRS");
        $excel->setActiveSheetIndex(0)->setCellValue('AP4', "TECHIDENTNO");
        $excel->setActiveSheetIndex(0)->setCellValue('AQ4', "ASSET");
        $excel->setActiveSheetIndex(0)->setCellValue('AR4', "EQ_NUMBER");
        $excel->setActiveSheetIndex(0)->setCellValue('AS4', "KODE_PST");
        $excel->setActiveSheetIndex(0)->setCellValue('AT4', "SERIAL_ID");
        $excel->setActiveSheetIndex(0)->setCellValue('AU4', "KONSTRUKSI");
        $excel->setActiveSheetIndex(0)->setCellValue('AV4', "EQUIPMENT_NUMBER");
        $excel->setActiveSheetIndex(0)->setCellValue('AW4', "ID_FUNCT");
        $excel->setActiveSheetIndex(0)->setCellValue('AX4', "ID_BUY");
        $excel->setActiveSheetIndex(0)->setCellValue('AY4', "WIL_KERJA");
        $excel->setActiveSheetIndex(0)->setCellValue('AZ4', "TIPE_ISOLATOR");
        $excel->setActiveSheetIndex(0)->setCellValue('BA4', "TIPE");
        $excel->setActiveSheetIndex(0)->setCellValue('BB4', "PHASA");
        $excel->setActiveSheetIndex(0)->setCellValue('BC4', "PENEMPATAN");





        // Apply style header yang telah kita buat tadi ke masing-masing kolom header
        $excel->getActiveSheet()->getStyle('A4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('B4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('C4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('D4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('E4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('F4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('G4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('H4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('I4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('J4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('K4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('L4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('M4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('N4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('O4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('P4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('Q4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('R4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('S4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('T4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('U4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('V4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('W4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('X4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('Y4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('Z4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('AA4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('AB4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('AC4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('AD4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('AE4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('AF4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('AG4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('AH4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('AI4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('AJ4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('AK4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('AL4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('AM4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('AN4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('AO4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('AP4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('AQ4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('AR4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('AS4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('AT4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('AU4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('AV4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('AW4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('AX4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('AY4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('AZ4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('BA4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('BB4')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('BC4')->applyFromArray($style_col);


        $x = $this->m_krisis->view();

        $no = 1; // Untuk penomoran tabel, di awal set dengan 1
        $numrow = 5; // Set baris pertama untuk isi tabel adalah baris ke 4


        foreach ($x as $data) {
            $excel->setActiveSheetIndex(0)->setCellValue('A' . $numrow, $data->upt_nama);
            $excel->setActiveSheetIndex(0)->setCellValue('B' . $numrow, $data->ultg_nama);
            $excel->setActiveSheetIndex(0)->setCellValue('C' . $numrow, $data->gi);
            $excel->setActiveSheetIndex(0)->setCellValue('D' . $numrow, $data->bay);
            $excel->setActiveSheetIndex(0)->setCellValue('E' . $numrow, $data->pht);
            $excel->setActiveSheetIndex(0)->setCellValue('F' . $numrow, $data->status_alat);
            $excel->setActiveSheetIndex(0)->setCellValue('G' . $numrow, $data->kode_sirkit);
            $excel->setActiveSheetIndex(0)->setCellValue('H' . $numrow, $data->kode_pst_old);
            $excel->setActiveSheetIndex(0)->setCellValue('I' . $numrow, $data->kd_status);
            $excel->setActiveSheetIndex(0)->setCellValue('J' . $numrow, $data->tower);
            $excel->setActiveSheetIndex(0)->setCellValue('K' . $numrow, $data->buatan);
            $excel->setActiveSheetIndex(0)->setCellValue('L' . $numrow, $data->tahun_buat);
            $excel->setActiveSheetIndex(0)->setCellValue('M' . $numrow, $data->tgl_operasi);
            $excel->setActiveSheetIndex(0)->setCellValue('N' . $numrow, $data->lat);
            $excel->setActiveSheetIndex(0)->setCellValue('O' . $numrow, $data->lang);
            $excel->setActiveSheetIndex(0)->setCellValue('P' . $numrow, $data->luas_tpk);
            $excel->setActiveSheetIndex(0)->setCellValue('Q' . $numrow, $data->posisi_tower);
            $excel->setActiveSheetIndex(0)->setCellValue('R' . $numrow, $data->rtrw);
            $excel->setActiveSheetIndex(0)->setCellValue('S' . $numrow, $data->kelurahan);
            $excel->setActiveSheetIndex(0)->setCellValue('T' . $numrow, $data->kecamatan);
            $excel->setActiveSheetIndex(0)->setCellValue('U' . $numrow, $data->kabupaten);
            $excel->setActiveSheetIndex(0)->setCellValue('V' . $numrow, $data->propinsi);
            $excel->setActiveSheetIndex(0)->setCellValue('W' . $numrow, $data->keterangan_tower);
            $excel->setActiveSheetIndex(0)->setCellValue('X' . $numrow, $data->merk);
            $excel->setActiveSheetIndex(0)->setCellValue('Y' . $numrow, $data->tipe_tower);
            $excel->setActiveSheetIndex(0)->setCellValue('Z' . $numrow, $data->jenis);
            $excel->setActiveSheetIndex(0)->setCellValue('AA' . $numrow, $data->jenis_material);
            $excel->setActiveSheetIndex(0)->setCellValue('AB' . $numrow, $data->jenis_pelapis);
            $excel->setActiveSheetIndex(0)->setCellValue('AC' . $numrow, $data->diameter);
            $excel->setActiveSheetIndex(0)->setCellValue('AD' . $numrow, $data->jarak_kaki);
            $excel->setActiveSheetIndex(0)->setCellValue('AE' . $numrow, $data->sudut_stub);
            $excel->setActiveSheetIndex(0)->setCellValue('AF' . $numrow, $data->tinggi_body);
            $excel->setActiveSheetIndex(0)->setCellValue('AG' . $numrow, $data->pjg_crossarm);
            $excel->setActiveSheetIndex(0)->setCellValue('AH' . $numrow, $data->jarak_fasa);
            $excel->setActiveSheetIndex(0)->setCellValue('AI' . $numrow, $data->jml_kawat);
            $excel->setActiveSheetIndex(0)->setCellValue('AJ' . $numrow, $data->berat_total);
            $excel->setActiveSheetIndex(0)->setCellValue('AK' . $numrow, $data->isolator);
            $excel->setActiveSheetIndex(0)->setCellValue('AL' . $numrow, $data->kode_teg);
            $excel->setActiveSheetIndex(0)->setCellValue('AM' . $numrow, $data->nomor_aset);
            $excel->setActiveSheetIndex(0)->setCellValue('AN' . $numrow, $data->cons_type);
            $excel->setActiveSheetIndex(0)->setCellValue('AO' . $numrow, $data->teg_oprs);
            $excel->setActiveSheetIndex(0)->setCellValue('AP' . $numrow, $data->techidentno);
            $excel->setActiveSheetIndex(0)->setCellValue('AQ' . $numrow, $data->asset);
            $excel->setActiveSheetIndex(0)->setCellValue('AR' . $numrow, $data->eq_number);
            $excel->setActiveSheetIndex(0)->setCellValue('AS' . $numrow, $data->kode_pst);
            $excel->setActiveSheetIndex(0)->setCellValue('AT' . $numrow, $data->serial_id);
            $excel->setActiveSheetIndex(0)->setCellValue('AU' . $numrow, $data->konstruksi);
            $excel->setActiveSheetIndex(0)->setCellValue('AV' . $numrow, $data->equipment_number);
            $excel->setActiveSheetIndex(0)->setCellValue('AW' . $numrow, $data->id_functloc);
            $excel->setActiveSheetIndex(0)->setCellValue('AX' . $numrow, $data->id_bay);
            $excel->setActiveSheetIndex(0)->setCellValue('AY' . $numrow, $data->wil_kerja);
            $excel->setActiveSheetIndex(0)->setCellValue('AZ' . $numrow, $data->tipe_isolator);
            $excel->setActiveSheetIndex(0)->setCellValue('BA' . $numrow, $data->tipe);
            $excel->setActiveSheetIndex(0)->setCellValue('BB' . $numrow, $data->phasa);
            $excel->setActiveSheetIndex(0)->setCellValue('BC' . $numrow, $data->penempatan);

            // Apply style row yang telah kita buat tadi ke masing-masing baris (isi tabel)
            $excel->getActiveSheet()->getStyle('A' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('B' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('C' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('D' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('E' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('F' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('G' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('H' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('I' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('J' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('K' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('L' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('M' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('N' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('O' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('P' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('Q' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('R' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('S' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('T' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('U' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('V' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('W' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('X' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('Y' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('Z' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('AA' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('AB' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('AC' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('AD' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('AE' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('AF' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('AG' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('AH' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('AI' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('AJ' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('AK' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('AL' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('AM' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('AN' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('AO' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('AP' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('AQ' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('AR' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('AS' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('AT' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('AU' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('AV' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('AW' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('AX' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('AZ' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('BA' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('BB' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('BC' . $numrow)->applyFromArray($style_row);

            $no++; // Tambah 1 setiap kali looping
            $numrow++; // Tambah 1 setiap kali looping
        }

        // Set width kolom
        $excel->getActiveSheet(3)->getStyle('K3:V3')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $excel->getActiveSheet(0)->getStyle('A4:Z4')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $excel->getActiveSheet(0)->getStyle('A4:Z4')->getFill()->getStartColor()->setRGB('1E90FF');
        $excel->getActiveSheet()->getRowDimension('4')->setRowHeight('80');
        $excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true); // Set width kolom A
        $excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true); // Set width kolom B
        $excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true); // Set width kolom C
        $excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true); // Set width kolom D
        $excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true); // Set width kolom E
        $excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true); // Set width kolom A
        $excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true); // Set width kolom B
        $excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true); // Set width kolom C
        $excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true); // Set width kolom D
        $excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true); // Set width kolom E
        $excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true); // Set width kolom D
        $excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true); // Set width kolom E
        $excel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true); // Set width kolom A
        $excel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true); // Set width kolom B
        $excel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true); // Set width kolom C
        $excel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true); // Set width kolom D
        $excel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true); // Set width kolom E
        $excel->getActiveSheet()->getColumnDimension('R')->setAutoSize(true); // Set width kolom D
        $excel->getActiveSheet()->getColumnDimension('S')->setAutoSize(true); // Set width kolom E
        $excel->getActiveSheet()->getColumnDimension('T')->setAutoSize(true); // Set width kolom A
        $excel->getActiveSheet()->getColumnDimension('U')->setAutoSize(true); // Set width kolom B
        $excel->getActiveSheet()->getColumnDimension('V')->setAutoSize(true); // Set width kolom C
        $excel->getActiveSheet()->getColumnDimension('W')->setAutoSize(true); // Set width kolom D
        $excel->getActiveSheet()->getColumnDimension('X')->setAutoSize(true); // Set width kolom E
        $excel->getActiveSheet()->getColumnDimension('Y')->setAutoSize(true); // Set width kolom D
        $excel->getActiveSheet()->getColumnDimension('Z')->setAutoSize(true); // Set width kolom E
        $excel->getActiveSheet()->getColumnDimension('AA')->setAutoSize(true); // Set width kolom A
        $excel->getActiveSheet()->getColumnDimension('AB')->setAutoSize(true); // Set width kolom E
        $excel->getActiveSheet()->getColumnDimension('AC')->setAutoSize(true); // Set width kolom A
        $excel->getActiveSheet()->getColumnDimension('AD')->setAutoSize(true); // Set width kolom A
        $excel->getActiveSheet()->getColumnDimension('AF')->setAutoSize(true); // Set width kolom B
        $excel->getActiveSheet()->getColumnDimension('AG')->setAutoSize(true); // Set width kolom C
        $excel->getActiveSheet()->getColumnDimension('AH')->setAutoSize(true); // Set width kolom D
        $excel->getActiveSheet()->getColumnDimension('AI')->setAutoSize(true); // Set width kolom E
        $excel->getActiveSheet()->getColumnDimension('AJ')->setAutoSize(true); // Set width kolom A
        $excel->getActiveSheet()->getColumnDimension('AK')->setAutoSize(true); // Set width kolom B
        $excel->getActiveSheet()->getColumnDimension('AL')->setAutoSize(true); // Set width kolom C
        $excel->getActiveSheet()->getColumnDimension('AM')->setAutoSize(true); // Set width kolom D
        $excel->getActiveSheet()->getColumnDimension('AN')->setAutoSize(true); // Set width kolom E
        $excel->getActiveSheet()->getColumnDimension('AO')->setAutoSize(true); // Set width kolom D
        $excel->getActiveSheet()->getColumnDimension('AP')->setAutoSize(true); // Set width kolom E
        $excel->getActiveSheet()->getColumnDimension('AQ')->setAutoSize(true); // Set width kolom A
        $excel->getActiveSheet()->getColumnDimension('AR')->setAutoSize(true); // Set width kolom B
        $excel->getActiveSheet()->getColumnDimension('AS')->setAutoSize(true); // Set width kolom C
        $excel->getActiveSheet()->getColumnDimension('AT')->setAutoSize(true); // Set width kolom D
        $excel->getActiveSheet()->getColumnDimension('AU')->setAutoSize(true); // Set width kolom E
        $excel->getActiveSheet()->getColumnDimension('AW')->setAutoSize(true); // Set width kolom D
        $excel->getActiveSheet()->getColumnDimension('AX')->setAutoSize(true); // Set width kolom E
        $excel->getActiveSheet()->getColumnDimension('AY')->setAutoSize(true); // Set width kolom A
        $excel->getActiveSheet()->getColumnDimension('AZ')->setAutoSize(true); // Set width kolom B
        $excel->getActiveSheet()->getColumnDimension('BA')->setAutoSize(true); // Set width kolom C
        $excel->getActiveSheet()->getColumnDimension('BB')->setAutoSize(true); // Set width kolom D
        $excel->getActiveSheet()->getColumnDimension('BC')->setAutoSize(true); // Set width kolom E

        // Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
        $excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);

        // Set orientasi kertas jadi LANDSCAPE
        $excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);

        // Set judul file excel nya
        $excel->getActiveSheet(0)->setTitle("Laporan Data");
        $excel->setActiveSheetIndex(0);

        // Proses file excel
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="Data Tower.xlsx"'); // Set nama file excel nya
        header('Cache-Control: max-age=0');

        $write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
        $write->save('php://output');
    }


    // hapus beberapa data
    public function delete_some()
    {
        $id_krisis = $this->input->post('id_krisis');
        $res = 0;

        foreach ($id_krisis as $v) {
            if ($this->delete($v, 1)) {
                $res = $res + 1;
            } else {
                $res = $res + 0;
            }
        }
        echo json_encode($res);
    }
}
