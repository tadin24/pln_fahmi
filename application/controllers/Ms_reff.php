<?php
if (!defined('BASEPATH')) exit(header('Location:../'));
class Ms_reff extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('m_ms_reff');
    }

    public function index()
    {
        $data['title'] = "Master Referensi";
        $data['opt_reffcat'] = $this->m_ms_reff->opt_reffcat();
        $this->my_theme('v_ms_reff', $data);
    }


    public function get_data()
    {
        $columns = [
            "reff_id",
            "reff_kode",
            "reff_nama",
            "reff_status",
            "reffcat_id",
        ];
        $columns_search = [
            "reff_kode",
            "reff_nama",
        ];
        $draw = intval($this->input->post("draw"));
        $where     = "";

        $reffcat_id = $this->input->post("f_reffcat_id");
        $where     .= " AND reffcat_id = $reffcat_id ";

        $search = $this->input->post("search");
        $search = $search['value'];
        if (isset($search) && $search != "") {
            $where .= "AND (";
            for ($i = 0; $i < count($columns_search); $i++) {
                $where .= " LOWER(" . $columns_search[$i] . ") LIKE LOWER('%" . ($search) . "%') OR ";
            }
            $where = substr_replace($where, "", -3);
            $where .= ')';
        }
        $totalRecords = $this->m_ms_reff->get_total($where);
        $length = intval($this->input->post("length"));
        $length = $length < 0 ? $totalRecords : $length;
        $start = intval($this->input->post("start"));
        if (isset($start) && $length != '-1') {
            $limit = "limit " . intval($length) . " offset " . intval($start);
        }
        $records = array();
        $records["data"] = array();
        $order = $this->input->post("order");
        $col = 0;
        $dir = "";
        if (!empty($order)) {
            foreach ($order as $o) {
                $col = $o['column'];
                $dir = $o['dir'];
            }
        }

        if ($dir != "asc" && $dir != "desc") {
            $dir = "desc";
        }

        if (!isset($columns[$col])) {
            $order = null;
        } else {
            $order = "ORDER BY " . $columns[$col] . " " . $dir;
        }

        $data = $this->m_ms_reff->get_data($columns, $where, $order, $limit);
        $no   = 1 + $start;
        foreach ($data as $row) {
            $status = ($row->reff_status == "0") ? "<span class='badge badge-danger'>Non Aktif</span>" : "<span class='badge badge-success'>Aktif</span>";
            $action = "";
            $id = $row->reff_id;
            $isi = "$id|$row->reff_kode|$row->reff_nama|$row->reff_status|$row->reffcat_id";

            $action .= '<a id="edit" onclick="edit(' . "'$isi'" . ')"><i class="fa fa-pencil-alt text-primary"></i></a>&nbsp;<a id="delete" onclick="del(' . $id . ')"><i class="fa fa-trash text-danger"></i></a>';
            $records["data"][] = array(
                $no++,
                $row->reff_kode,
                $row->reff_nama,
                $status,
                $action
            );
        }

        $records["draw"] = $draw;
        $records["recordsTotal"] = $totalRecords;
        $records["recordsFiltered"] = $totalRecords;

        echo json_encode($records);
    }


    // save
    public function save()
    {
        $res = "";
        $act = $this->input->post('act', true);

        $data = [
            "reff_nama" => $this->input->post("reff_nama"),
            "reff_kode" => $this->input->post("reff_kode"),
            "reff_status" => $this->input->post("reff_status"),
            "reffcat_id" => $this->input->post("reffcat_id"),
            "update_at" => date("Y-m-d H:i:s"),
            "update_by" => $this->session->userdata('user_id'),
        ];

        if ($act == "add") {
            $res = $this->m_ms_reff->add($data);
        } else {
            $id = $this->input->post('reff_id');
            $res = $this->m_ms_reff->update($data, $id);
        }

        echo json_encode($res);
    }


    // delete
    public function delete($id = 0)
    {
        $res = "";
        if ($id != 0) {
            $res = $this->m_ms_reff->delete($id);
        } else {
            $res = "false";
        }

        echo json_encode($res);
    }


    // cek kode
    public function cek_reff_kode()
    {
        $res = "";
        $where = "";

        $where .= " AND reff_kode = '" .
            $this->input->post('reff_kode') . "'";
        $where .= " AND reffcat_id = " .
            $this->input->post('reffcat_id') . " ";

        if ($this->input->post("act") == "edit") {
            $where .= " AND reff_kode != '" .
                $this->input->post('reff_kode_lama') . "'";
        }

        if ($this->m_ms_reff->cek_reff_kode($where) > 0) {
            $res = "Kode sudah dipakai. Buat kode yang lain";
        } else {
            $res = "true";
        }

        echo json_encode($res);
    }
}
