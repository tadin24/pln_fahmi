<?php
if (!defined('BASEPATH')) exit(header('Location:../'));
class Ms_reffcat extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->load->model('m_ms_reffcat');
    }

    public function index()
    {
        $data['title'] = "Master Kategori Referensi";
        $this->my_theme('v_ms_reffcat', $data);
    }


    public function get_data()
    {
        $columns = [
            "reffcat_id",
            "reffcat_nama",
            "reffcat_status",
        ];
        $columns_search = [
            "reffcat_nama",
        ];
        $draw = intval($this->input->post("draw"));
        $where     = "";

        $search = $this->input->post("search");
        $search = $search['value'];
        if (isset($search) && $search != "") {
            $where .= "AND (";
            for ($i = 0; $i < count($columns_search); $i++) {
                $where .= " LOWER(" . $columns_search[$i] . ") LIKE LOWER('%" . ($search) . "%') OR ";
            }
            $where = substr_replace($where, "", -3);
            $where .= ')';
        }
        $totalRecords = $this->m_ms_reffcat->get_total($where);
        $length = intval($this->input->post("length"));
        $length = $length < 0 ? $totalRecords : $length;
        $start = intval($this->input->post("start"));
        if (isset($start) && $length != '-1') {
            $limit = "limit " . intval($length) . " offset " . intval($start);
        }
        $records = array();
        $records["data"] = array();
        $order = $this->input->post("order");
        $col = 0;
        $dir = "";
        if (!empty($order)) {
            foreach ($order as $o) {
                $col = $o['column'];
                $dir = $o['dir'];
            }
        }

        if ($dir != "asc" && $dir != "desc") {
            $dir = "desc";
        }

        if (!isset($columns[$col])) {
            $order = null;
        } else {
            $order = "ORDER BY " . $columns[$col] . " " . $dir;
        }

        $data = $this->m_ms_reffcat->get_data($columns, $where, $order, $limit);
        $no   = 1 + $start;
        foreach ($data as $row) {
            $status = ($row->reffcat_status == "0") ? "<span class='badge badge-danger'>Non Aktif</span>" : "<span class='badge badge-success'>Aktif</span>";
            $action = "";
            $id = $row->reffcat_id;
            $isi = "$id|$row->reffcat_nama|$row->reffcat_status";

            $action .= '<a id="edit" onclick="edit(' . "'$isi'" . ')"><i class="fa fa-pencil-alt text-primary"></i></a>&nbsp;<a id="delete" onclick="del(' . $id . ')"><i class="fa fa-trash text-danger"></i></a>';
            $records["data"][] = array(
                $no++,
                $row->reffcat_nama,
                $status,
                $action
            );
        }

        $records["draw"] = $draw;
        $records["recordsTotal"] = $totalRecords;
        $records["recordsFiltered"] = $totalRecords;

        echo json_encode($records);
    }


    // save
    public function save()
    {
        $res = "";
        $act = $this->input->post('act', true);

        $data = [
            "reffcat_nama" => $this->input->post("reffcat_nama"),
            "reffcat_status" => $this->input->post("reffcat_status"),
            "update_at" => date("Y-m-d H:i:s"),
            "update_by" => $this->session->userdata('user_id'),
        ];

        if ($act == "add") {
            $res = $this->m_ms_reffcat->add($data);
        } else {
            $id = $this->input->post('reffcat_id');
            $res = $this->m_ms_reffcat->update($data, $id);
        }

        echo json_encode($res);
    }


    // delete
    public function delete($id = 0)
    {
        $res = "";
        if ($id != 0) {
            $res = $this->m_ms_reffcat->delete($id);
        } else {
            $res = "false";
        }

        echo json_encode($res);
    }


    // cek nama
    public function cek_reffcat_nama()
    {
        $res = "";
        $where = "";

        $where .= " AND reffcat_nama = '" .
            $this->input->post('reffcat_nama') . "'";

        if ($this->input->post("act") == "edit") {
            $where .= " AND reffcat_nama != '" .
                $this->input->post('reffcat_nama_lama') . "'";
        }

        if ($this->m_ms_reffcat->cek_reffcat_nama($where) > 0) {
            $res = "Nama sudah dipakai. Buat nama yang lain";
        } else {
            $res = "true";
        }

        echo json_encode($res);
    }
}
