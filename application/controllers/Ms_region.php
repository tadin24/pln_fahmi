<?php
if (!defined('BASEPATH')) exit(header('Location:../'));
class Ms_region extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->load->model('m_ms_region');
    }

    public function index()
    {
        $data['title'] = "Master Region";
        $this->my_theme('v_ms_region', $data);
    }


    public function get_data()
    {
        $columns = [
            "mr.reg_code",
            "mr.reg_name",
            "parent.reg_name as region_parent_name",
            "mr.reg_active",
            "mr.reg_level",
            "mr.reg_parent",
            "coalesce(child.total, 0) as total",
        ];
        $columns_order = [
            "mr.reg_code",
            "mr.reg_code",
            "mr.reg_name",
            "parent.reg_name",
            "mr.reg_active",
        ];

        $draw = intval($this->input->post("draw"));
        $where     = "";

        $search = $this->input->post("search");
        $search = $search['value'];
        if (isset($search) && $search != "") {
            $where .= "AND (";
            for ($i = 0; $i < count($columns_order); $i++) {
                $where .= " LOWER(" . $columns_order[$i] . ") LIKE LOWER('%" . ($search) . "%') OR ";
            }
            $where = substr_replace($where, "", -3);
            $where .= ')';
        }
        $totalRecords = $this->m_ms_region->get_total($where);
        $length = intval($this->input->post("length"));
        $length = $length < 0 ? $totalRecords : $length;
        $start = intval($this->input->post("start"));
        if (isset($start) && $length != '-1') {
            $limit = "limit " . intval($length) . " offset " . intval($start);
        }
        $records = array();
        $records["data"] = array();
        $order = $this->input->post("order");
        $col = 0;
        $dir = "";
        if (!empty($order)) {
            foreach ($order as $o) {
                $col = $o['column'];
                $dir = $o['dir'];
            }
        }

        if ($dir != "asc" && $dir != "desc") {
            $dir = "desc";
        }

        if (!isset($columns_order[$col])) {
            $order = null;
        } else {
            $order = "ORDER BY " . $columns_order[$col] . " " . $dir;
        }

        $data = $this->m_ms_region->get_data($columns, $where, $order, $limit);
        $no   = 1 + $start;
        foreach ($data as $row) {
            $status = ($row->reg_active == "0") ? "<span class='badge badge-danger'>Non Aktif</span>" : "<span class='badge badge-success'>Aktif</span>";
            $action = "";
            $id = $row->reg_code;
            $isi = "$id|$row->reg_name|$row->reg_active|$row->reg_level|$row->reg_parent|$row->region_parent_name";

            $action .= '<a id="edit" onclick="edit(' . "'$isi'" . ')"><i class="fa fa-pencil-alt text-primary"></i></a>&nbsp;';
            if ($row->total <= 0) {
                $action .= '<a id="delete" onclick="del(' . $id . ')"><i class="fa fa-trash text-danger"></i></a>';
            }
            $records["data"][] = array(
                $no++,
                $row->reg_code,
                $row->reg_name,
                $row->region_parent_name,
                $status,
                $action
            );
        }

        $records["draw"] = $draw;
        $records["recordsTotal"] = $totalRecords;
        $records["recordsFiltered"] = $totalRecords;

        echo json_encode($records);
    }


    // save
    public function save()
    {
        $res = "";
        $act = $this->input->post('act', true);

        $data = [
            "reg_code" => $this->input->post("reg_code"),
            "reg_name" => $this->input->post("reg_name"),
            "reg_level" => $this->input->post("reg_level"),
            "reg_active" => $this->input->post("reg_active"),
            "reg_parent" => $this->input->post("reg_parent"),
        ];

        if ($act == "add") {
            $res = $this->m_ms_region->add($data);
        } else {
            $id = $this->input->post('reg_code');
            $parent_id_lama = $this->input->post('reg_code_lama');
            $res = $this->m_ms_region->update($data, $parent_id_lama);
            if ($parent_id_lama != $data["reg_parent"]) {
                $data["reg_level_lama"] = $this->input->post('reg_level_lama');
                $res = $this->m_ms_region->update_child($data, $id);
            }
        }

        echo json_encode($res);
    }


    // delete
    public function delete($id = 0)
    {
        $res = "";
        if ($id != 0) {
            $res = $this->m_ms_region->delete($id);
        } else {
            $res = "false";
        }

        echo json_encode($res);
    }


    // cek kode
    public function cek_reg_code()
    {
        $res = "";
        $where = "";

        $where .= " AND reg_code = '" .
            $this->input->post('reg_code') . "'";

        if ($this->input->post("act") == "edit") {
            $where .= " AND reg_code != '" .
                $this->input->post('reg_code_lama') . "'";
        }

        if ($this->m_ms_region->cek_reg_code($where) > 0) {
            $res = "Kode sudah dipakai. Buat kode yang lain";
        } else {
            $res = "true";
        }

        echo json_encode($res);
    }


    // cek parent kode
    public function cek_parent()
    {
        $res = "";

        $parent_id = $this->input->post('reg_parent');

        $reg_code = $this->input->post('reg_code');

        if ($parent_id == $reg_code) {
            $res = "Parent Kode Tidak Boleh Sama Dengan Kode Baru Yang Akan Disimpan";
        } else {
            $res = "true";
        }

        echo json_encode($res);
    }


    // get_region
    public function get_region()
    {
        $res = null;
        $res = $this->m_ms_region->get_region();
        echo json_encode($res);
    }
}
