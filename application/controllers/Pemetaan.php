<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Pemetaan extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->library('pagination');
        $this->load->helper('url');
        $this->load->model('m_krisis');
    }
    public function index()
    {
        $data = array(
            'judul' => 'Data',
        );
        $data['data'] = $this->m_krisis->show_data();
        $this->my_theme('v_map', $data);
    }
    public function tower_json()
    {
        $data = $this->db->get('krisis')->result();
        echo json_encode($data);
    }
    public function shelter()
    {
        $x = array(
            'judul' => 'Data',
        );
        $this->my_theme('v_shelter', $x);
    }

    public function get_data()
    {
        $columns = [
            "id_krisis",
            "penghantar",
            "bay",
            "pht",
            "k.lat",
            "k.lang",
            "k.klali",
            "k.klapo",

        ];
        $columns_search = [
            "klali",
            "klapo",
            "pht",
        ];
        $draw = intval($this->input->post("draw"));
        $where     = "";
        // $f_mmsi = $this->input->post("f_mmsi");
        // $where .= " AND fd.mmsi = '$f_mmsi' ";

        $search = $this->input->post("search");
        $search = $search['value'];
        if (isset($search) && $search != "") {
            $where .= "AND (";
            for ($i = 0; $i < count($columns_search); $i++) {
                $where .= " LOWER(" . $columns_search[$i] . ") LIKE LOWER('%" . ($search) . "%') OR ";
            }
            $where = substr_replace($where, "", -3);
            $where .= ')';
        }
        $totalRecords = $this->m_krisis->get_total($where);
        $length = intval($this->input->post("length"));
        $length = $length < 0 ? $totalRecords : $length;
        $start = intval($this->input->post("start"));
        if (isset($start) && $length != '-1') {
            $limit = "limit " . intval($length) . " offset " . intval($start);
        }
        $records = array();
        $records["data"] = array();
        $order = $this->input->post("order");
        $col = 0;
        $dir = "";
        if (!empty($order)) {
            foreach ($order as $o) {
                $col = $o['column'];
                $dir = $o['dir'];
            }
        }

        if ($dir != "asc" && $dir != "desc") {
            $dir = "desc";
        }

        if (!isset($columns[$col])) {
            $order = null;
        } else {
            $order = "ORDER BY " . $columns[$col] . " " . $dir;
        }

        $data = $this->m_krisis->get_data($columns, $where, $order, $limit);
        $no   = 1 + $start;
        foreach ($data as $row) {
            $action = "";
            $id = $row->id_krisis;
            // $isi = "$id|$row->group_name|$row->group_aktif";

            $action = '<a href="' . base_url('Pemetaan/details_tower/' . $id) . '" class="btn btn-success btn-sm" title="Rincian"><i class="fa fa-eye"></a>';
            $records["data"][] = array(
                $no++,
                $row->penghantar,
                $row->bay,
                $row->pht,
                $row->lat,
                $row->lang,
                ($row->klali != '0' ? ($row->klali != '1' ? ($row->klali != '2' ? "KRITIS" : "WASPADA") : "AMAN") : "-"),
                ($row->klapo != '0' ? ($row->klapo != '1' ? ($row->klapo != '2' ? "KRITIS" : "WASPADA") : "AMAN") : "-"),
                $action,
            );
        }

        $records["draw"] = $draw;
        $records["recordsTotal"] = $totalRecords;
        $records["recordsFiltered"] = $totalRecords;

        echo json_encode($records);
    }


    public function details_tower($id)
    {
        $data = $this->m_krisis->data_details_tower($id);
        $x = array(
            'title' => 'Details Data',
            'data' => $data
        );
        $this->my_theme('v_details_tower', $x);
    }
}
