<?php
if (!defined('BASEPATH')) exit(header('Location:../'));

class Penghantar extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('m_penghantar');
    }

    public function index()
    {
        $data['title'] = "penghantar";
        $this->my_theme('v_penghantar', $data);
    }

    // get data krisis
    public function get_data()
    {
        $columns = [
            "penghantar_id",
            "penghantar_nama",
            "penghantar_status",

        ];
        $columns_search = [
            "penghantar_nama",
        ];
        $draw = intval($this->input->post("draw"));
        $where     = "";
        $search = $this->input->post("search");
        $search = $search['value'];
        if (isset($search) && $search != "") {
            $where .= "AND (";
            for ($i = 0; $i < count($columns_search); $i++) {
                $where .= " LOWER(" . $columns_search[$i] . ") LIKE LOWER('%" . ($search) . "%') OR ";
            }
            $where = substr_replace($where, "", -3);
            $where .= ')';
        }
        $totalRecords = $this->m_penghantar->get_total($where);
        $length = intval($this->input->post("length"));
        $length = $length < 0 ? $totalRecords : $length;
        $start = intval($this->input->post("start"));
        if (isset($start) && $length != '-1') {
            $limit = "limit " . intval($length) . " offset " . intval($start);
        }
        $records = array();
        $records["data"] = array();
        $order = $this->input->post("order");
        $col = 0;
        $dir = "";
        if (!empty($order)) {
            foreach ($order as $o) {
                $col = $o['column'];
                $dir = $o['dir'];
            }
        }

        if ($dir != "asc" && $dir != "desc") {
            $dir = "desc";
        }

        if (!isset($columns[$col])) {
            $order = null;
        } else {
            $order = "ORDER BY " . $columns[$col] . " " . $dir;
        }

        $data = $this->m_penghantar->get_data($columns, $where, $order, $limit);
        $no   = 1 + $start;
        foreach ($data as $row) {
            $action = "";
            $id = $row->penghantar_id;

            $action = '<a href="javascript:;" onclick="set_val(\'' . $id . '\')" class="btn btn-info btn-sm" title="Edit"><i class="fa fa-pencil-alt"></i></a>&nbsp;<a href="javascript:;" onclick="del(\'' . $id . '\')" class="btn btn-danger btn-sm" title="Hapus"><i class="fa fa-trash"></i></a>&nbsp;';

            $status = $row->penghantar_status == '1' ? '<span class="badge badge-success">Aktif</span>' : '<span class="badge badge-danger">Non Aktif</span>';

            $records["data"][] = array(
                $no++,
                $row->penghantar_nama,
                $status,
                $action,
            );
        }

        $records["draw"] = $draw;
        $records["recordsTotal"] = $totalRecords;
        $records["recordsFiltered"] = $totalRecords;

        echo json_encode($records);
    }


    // save
    public function save()
    {
        $data = $this->input->post();
        unset($data['act']);
        if ($this->input->post('act') == 'add') {
            unset($data['penghantar_id']);
            $res = $this->m_penghantar->insert($data);
        } else {
            $res = $this->m_penghantar->update($data);
        }
        echo json_encode($res);
    }


    // get data penghantar by id
    public function edit($id)
    {
        $res = null;
        if ($id > 0 && !empty($id)) {
            $res = $this->m_penghantar->edit($id);
        }

        echo json_encode($res);
    }


    // delete
    public function delete($id)
    {
        $res = $this->m_penghantar->delete($id);
        echo json_encode($res);
    }
}
