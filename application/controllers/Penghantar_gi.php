<?php
if (!defined('BASEPATH')) exit(header('Location:../'));

class Penghantar_gi extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('m_penghantar_gi');
        $this->load->model('m_common');
    }

    public function index()
    {
        $data['title'] = "Penghantar Gardu Induk";
        $data['opt_prov'] = $this->m_common->get_wilayah('0');
        $this->my_theme('v_penghantar_gi', $data);
    }

    // get data krisis
    public function get_data()
    {
        $columns = [
            "penghantar_id",
            "penghantar_nama",
            "penghantar_status",
            "kec.reg_name",
            "prov_kode",
            "kab_kode",
            "kec_kode",
        ];
        $columns_search = [
            "penghantar_nama",
        ];
        $draw = intval($this->input->post("draw"));
        $where     = "";
        $f_prov_kode = $this->input->post("f_prov_kode");
        $f_kab_kode = $this->input->post("f_kab_kode");
        $f_kondisi = $this->input->post("f_kondisi");

        if ($f_kondisi == '1') {
            $where .= " AND prov_kode = '$f_prov_kode' AND kab_kode = '$f_kab_kode' AND kec_kode is not null AND kec_kode != ''";
        } else {
            $where .= " AND (prov_kode = '' OR kab_kode = '' OR kec_kode = '' OR kab_kode is null OR prov_kode is null OR kec_kode is null) ";
        }

        $search = $this->input->post("search");
        $search = $search['value'];
        if (isset($search) && $search != "") {
            $where .= "AND (";
            for ($i = 0; $i < count($columns_search); $i++) {
                $where .= " LOWER(" . $columns_search[$i] . ") LIKE LOWER('%" . ($search) . "%') OR ";
            }
            $where = substr_replace($where, "", -3);
            $where .= ')';
        }
        $totalRecords = $this->m_penghantar_gi->get_total($where);
        $length = intval($this->input->post("length"));
        $length = $length < 0 ? $totalRecords : $length;
        $start = intval($this->input->post("start"));
        if (isset($start) && $length != '-1') {
            $limit = "limit " . intval($length) . " offset " . intval($start);
        }
        $records = array();
        $records["data"] = array();
        $order = $this->input->post("order");
        $col = 0;
        $dir = "";
        if (!empty($order)) {
            foreach ($order as $o) {
                $col = $o['column'];
                $dir = $o['dir'];
            }
        }

        if ($dir != "asc" && $dir != "desc") {
            $dir = "desc";
        }

        if (!isset($columns[$col])) {
            $order = null;
        } else {
            $order = "ORDER BY " . $columns[$col] . " " . $dir;
        }

        $data = $this->m_penghantar_gi->get_data($columns, $where, $order, $limit);
        $no   = 1 + $start;
        foreach ($data as $row) {
            $action = "";
            $id = $row->penghantar_id;

            $action = '<a href="javascript:;" onclick="set_val(\'' . $id . '\')" class="btn btn-info btn-sm" title="Edit"><i class="fa fa-pencil-alt"></i></a>&nbsp;<a href="javascript:;" onclick="del(\'' . $id . '\')" class="btn btn-danger btn-sm" title="Hapus"><i class="fa fa-trash"></i></a>&nbsp;';

            $status = $row->penghantar_status == '1' ? '<span class="badge badge-success">Aktif</span>' : '<span class="badge badge-danger">Non Aktif</span>';

            $records["data"][] = array(
                $no++,
                $row->penghantar_nama,
                $row->reg_name,
                $status,
                $action,
            );
        }

        $records["draw"] = $draw;
        $records["recordsTotal"] = $totalRecords;
        $records["recordsFiltered"] = $totalRecords;

        echo json_encode($records);
    }


    // save
    public function save()
    {
        $data = $this->input->post();
        unset($data['act']);
        if ($this->input->post('act') == 'add') {
            unset($data['penghantar_id']);
            $res = $this->m_penghantar_gi->insert($data);
        } else {
            $res = $this->m_penghantar_gi->update($data);
        }
        echo json_encode($res);
    }


    // get data penghantar_gi by id
    public function edit($id)
    {
        $res = null;
        if ($id > 0 && !empty($id)) {
            $res = $this->m_penghantar_gi->edit($id);
        }

        echo json_encode($res);
    }


    // delete
    public function delete($id)
    {
        $res = $this->m_penghantar_gi->delete($id);
        echo json_encode($res);
    }


    // get wil
    public function get_wil()
    {
        $res = $this->m_common->get_wilayah($this->input->post('parent'));
        echo json_encode($res);
    }
}
