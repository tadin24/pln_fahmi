<?php

use phpDocumentor\Reflection\Types\This;

if (!defined('BASEPATH')) exit(header('Location:../'));
class Pentanahan_bali extends MY_Controller
{
    private $filename = "import_data"; // Kita tentukan nama filenya
    function __construct()
    {
        parent::__construct();
        $this->load->model('m_pentanahan_bali');
    }

    public function index()
    {
        $data['title'] = "Pentanahan Surabaya";
        $data['penghantar'] = $this->m_pentanahan_bali->get_penghantar();
        $data['gi'] = $this->m_pentanahan_bali->get_gi();
        $this->my_theme('v_pentanahan_bali', $data);
    }


    // save
    public function save()
    {
        $data = $this->input->post();
        $data['tgl'] = date('Y-m-d', strtotime($this->input->post('tgl')));
        unset($data['act']);
        if ($this->input->post('act') == 'add') {
            unset($data['id_pentanahan']);
            $res = $this->m_pentanahan_bali->insert($data);
        } else {
            $res = $this->m_pentanahan_bali->update($data);
        }

        $count = count($_FILES);

        if ($count > 0) {
            $this->load->library('upload');
            foreach ($_FILES as $key => $val) {
                $config = [];
                if ($key == 'foto1' || $key == 'foto2' || $key == 'foto3' || $key == 'foto4') {
                    $config['allowed_types'] = 'jpg|jpeg|png|gif';
                    switch ($key) {
                        case 'foto1':
                            $config['file_name'] = $res . "_1";
                            break;
                        case 'foto2':
                            $config['file_name'] = $res . "_2";
                            break;
                        case 'foto3':
                            $config['file_name'] = $res . "_3";
                            break;
                        case 'foto4':
                            $config['file_name'] = $res . "_4";
                            break;
                        default:
                            break;
                    }
                    $config['upload_path'] = 'assets/images/';
                } else {
                    $config['allowed_types'] = 'doc|docx|pdf|xlsx';
                    $config['file_name'] = $key . "_" . $res;
                    $config['upload_path'] = 'assets/documents/';
                }

                $this->upload->initialize($config);

                if ($this->input->post('act') == 'edit') {
                    $pentanahan_bali = $this->m_pentanahan_bali->get_file($key, $res);
                    if ($key == 'foto1' || $key == 'foto2' || $key == 'foto3' || $key == 'foto4') {
                        if (!empty($pentanahan_bali[$key])) {
                            if (file_exists('assets/images/' . $pentanahan_bali[$key])) {
                                unlink('assets/images/' . $pentanahan_bali[$key]);
                            }
                        }
                    } else {
                        if (!empty($pentanahan_bali[$key])) {
                            if (file_exists('assets/documents/' . $pentanahan_bali[$key])) {
                                unlink('assets/documents/' . $pentanahan_bali[$key]);
                            }
                        }
                    }
                }


                if ($this->upload->do_upload($key)) {
                    $uploadData = $this->upload->data();

                    $filename = $uploadData['file_name'];
                    $data = [
                        'id_pentanahan' => $res,
                        $key => $filename,
                    ];
                    $res = $this->m_pentanahan_bali->update($data);
                }
            }
        }
        echo json_encode($res);
    }


  
    public function edit($id)
    {
        $res = null;
        if ($id > 0 && !empty($id)) {
            $res = $this->m_pentanahan_bali->edit($id);
            $res->tgl = date('d-m-Y', strtotime($res->tgl));
        }

        echo json_encode($res);
    }



    public function get_data()
    {
        $columns = [
            "id_pentanahan",
            "id_pentanahan",
            "tgl",
            "gi",
            "penghantar",
            "no_tower",
            "hasil",
            "metode",
            "anomali",

        ];
        $columns_search = [
            "penghantar",
            "gi",
        ];
        $draw = intval($this->input->post("draw"));
        $where     = "";

        $search = $this->input->post("search");
        $search = $search['value'];

        if (isset($search) && $search != "") {
            $where .= "AND (";
            for ($i = 0; $i < count($columns_search); $i++) {
                $where .= " LOWER(" . $columns_search[$i] . ") LIKE LOWER('%" . ($search) . "%') OR ";
            }
            $where = substr_replace($where, "", -3);
            $where .= ')';
        }
        $totalRecords = $this->m_pentanahan_bali->get_total($where);
        $length = intval($this->input->post("length"));
        $length = $length < 0 ? $totalRecords : $length;
        $start = intval($this->input->post("start"));
        if (isset($start) && $length != '-1') {
            $limit = "limit " . intval($length) . " offset " . intval($start);
        }
        $records = array();
        $records["data"] = array();
        $order = $this->input->post("order");
        $col = 0;
        $dir = "";
        if (!empty($order)) {
            foreach ($order as $o) {
                $col = $o['column'];
                $dir = $o['dir'];
            }
        }

        if ($dir != "asc" && $dir != "desc") {
            $dir = "desc";
        }

        if (!isset($columns[$col])) {
            $order = null;
        } else {
            $order = "ORDER BY " . $columns[$col] . " " . $dir;
        }

        $data = $this->m_pentanahan_bali->get_data($columns, $where, $order, $limit);
        $no   = 1 + $start;
        foreach ($data as $row) {
            $action = "";
            $id = $row->id_pentanahan;
            // $isi = "$id|$row->group_name|$row->group_aktif";

            $action = '<a href="javascript:;" onclick="set_val(\'' . $id . '\')" class="btn btn-info btn-sm" title="Edit"><i class="fa fa-pencil-alt"></i></a>&nbsp;<a href="javascript:;" onclick="del(\'' . $id . '\')" class="btn btn-danger btn-sm" title="Hapus"><i class="fa fa-trash"></i></a>&nbsp;<a href="' . base_url('Pentanahan_bali/details/' . $id) . '" class="btn btn-success btn-sm" title="Rincian"><i class="fa fa-eye"></a>';
            $records["data"][] = array(
                $no++,
                '<div class="form-check"><input class="form-check-input input-check position-static" type="checkbox" data-id="' . $id . '" onchange="inputCheckOnChange(this)"></div>',
                date("d-m-Y", strtotime($row->tgl)),
                $row->gi,
                $row->penghantar,
                $row->no_tower,
                $row->hasil,
                $row->metode,
                $row->anomali,
                $action,

            );
        }

        $records["draw"] = $draw;
        $records["recordsTotal"] = $totalRecords;
        $records["recordsFiltered"] = $totalRecords;

        echo json_encode($records);
    }


    // delete
    public function delete($id, $is_banyak = 0)
    {
        $res = $this->m_pentanahan_bali->delete($id);
        if ($is_banyak == 0) {
            echo json_encode($res);
        } else {
            return $res;
        }
    }



    public function details($id)
    {
        $data = $this->m_pentanahan_bali->data_details($id);
        $x = array(
            'title' => 'Details Data',
            'data' => $data
        );
        $this->my_theme('v_details_pentanahan', $x);
    }

    public function form()
    {
        $data = array('judul' => 'Import Data'); // Buat variabel $data sebagai array

        if (isset($_POST['preview'])) { // Jika user menekan tombol Preview pada form
            // lakukan upload file dengan memanggil function upload yang ada di SiswaModel.php
            // $upload = $this->m_pentanahan_bali->upload_file($this->filename);

            $this->load->library('upload'); // Load librari upload

            $config['upload_path'] = './excel/';
            $config['allowed_types'] = 'xlsx';
            $config['max_size']  = '2048';
            $config['overwrite'] = true;
            $config['file_name'] = $this->filename;

            $this->upload->initialize($config); // Load konfigurasi uploadnya
            if ($this->upload->do_upload('file_excel')) { // Lakukan upload dan Cek jika proses upload berhasil
                // Jika berhasil :
                include APPPATH . 'third_party/PHPExcel/PHPExcel.php';

                $excelreader = new PHPExcel_Reader_Excel2007();
                $loadexcel = $excelreader->load('excel/' . $this->filename . '.xlsx'); // Load file yang tadi diupload ke folder excel
                $sheet = $loadexcel->getActiveSheet()->toArray(null, true, true, true);

                // Masukan variabel $sheet ke dalam array data yang nantinya akan di kirim ke file form.php
                // Variabel $sheet tersebut berisi data-data yang sudah diinput di dalam excel yang sudha di upload sebelumnya
                $data['sheet'] = $sheet;
            } else {
                // Jika gagal :
                $data['upload_error'] = $this->upload->display_errors(); // Ambil pesan error uploadnya untuk dikirim ke file form dan ditampilkan
            }
        }

        $this->my_theme('v_form_pentanahan_bali', $data);
    }

    public function import()
    {
        // Load plugin PHPExcel nya
        include APPPATH . 'third_party/PHPExcel/PHPExcel.php';

        $opt_penghantar_data = $this->m_pentanahan_bali->get_penghantar();
        $opt_gi_data = $this->m_pentanahan_bali->get_gi();
        $opt_gi = $opt_penghantar = null;


        foreach ($opt_penghantar_data as $value) {
            $opt_penghantar[strtolower($value->penghantar_nama)] = $value->penghantar_id;
        }
        foreach ($opt_gi_data as $value) {
            $opt_gi[strtolower($value->gi_nama)] = $value->gi_id;
        }

        $excelreader = new PHPExcel_Reader_Excel2007();
        $loadexcel = $excelreader->load('excel/' . $this->filename . '.xlsx'); // Load file yang telah diupload ke folder excel
        $sheet = $loadexcel->getActiveSheet()->toArray(null, true, true, true);

        // Buat sebuah variabel array untuk menampung array data yg akan kita insert ke database
        $data = array();

        $numrow = 1;
        foreach ($sheet as $row) {
            
            if ($numrow > 9 && $numrow < count($sheet) - -1) {
                // Kita push (add) array data ke variabel data
                $gi = !empty($opt_gi[strtolower($row['B'])]) ? $opt_gi[strtolower($row['B'])] : 0;
                $penghantar = !empty($opt_penghantar[strtolower($row['C'])]) ? $opt_penghantar[strtolower($row['C'])] : 0;

                array_push($data, array(
                    'gi' => $row['B'],
                    'gi_id' => $gi,
                    'penghantar' => $row['C'],
                    'penghantar_id' => $penghantar,
                    'no_tower' => $row['D'],
                    'tipe_tower' => $row['E'],
                    'nilai_arde' => $row['F'],
                    'arde_tower' => $row['G'],
                    'arde_a' => $row['H'],
                    'arde_b' => $row['I'],
                    'arde_c' => $row['J'],
                    'arde_d' => $row['K'],
                    'tower_arde' => $row['L'],
                    'arde_gsw' => $row['M'],
                    'semen_tower' => $row['N'],
                    'semen_a' => $row['O'],
                    'semen_b' => $row['P'],
                    'semen_c' => $row['Q'],
                    'semen_d' => $row['R'],
                    'tower_semen' => $row['S'],
                    'semen_gsw' => $row['T'],
                    'hasil' => $row['U'],
                    'metode' => $row['V'],
                    'anomali' => $row['W'],
                    'tgl' => date("Y-m-d", strtotime(str_replace("/", "-", $row['X']))),
                    'tembok' => $row['Y'],
                    'luas' => $row['Z'],
                    'kondisi_tanah' => $row['AA'],
                    'desa' => $row['AB'],
                    'kecamatan' => $row['AC']
                    

                ));
            }

            $numrow++; // Tambah 1 setiap kali looping
        }

        // Panggil fungsi insert_multiple yg telah kita buat sebelumnya di model
        $this->m_pentanahan_bali->insert_multiple($data);

        redirect("pentanahan_bali"); // Redirect ke halaman awal (ke controller siswa fungsi index)
    }

    public function export()
    {
        // Load plugin PHPExcel nya
        include APPPATH . 'third_party/PHPExcel/PHPExcel.php';

        // Panggil class PHPExcel nya
        $excel = new PHPExcel();

        // Settingan awal fil excel
        $excel->getProperties()->setCreator('PLN UIT JBTB UNIT INDUK')
            ->setLastModifiedBy('Fahmi')
            ->setTitle("FORM ASSESMEN PENGUKURAN TAHANAN PENTANAHAN KAKI TOWER LATIC TAHUN 2020")
            ->setSubject("")
            ->setDescription("Laporan Pengukuran Tahanan Pentanahan")
            ->setKeywords("Data Pentanahan");

        // Buat sebuah variabel untuk menampung pengaturan style dari header tabel
        $style_col = array(
            'font' => array('bold' => true), // Set font nya jadi bold
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
            ),
            'borders' => array(
                'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
                'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
                'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
                'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
            )
        );

        // Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
        $style_row = array(
            'alignment' => array(
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
            ),
            'borders' => array(
                'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
                'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
                'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
                'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
            )
        );

        $excel->setActiveSheetIndex(0)->setCellValue('A2', "FORM ASSESMEN PENGUKURAN TAHANAN PENTANAHAN KAKI TOWER LATIC dan POLICE TAHUN 2020"); // Set kolom A1 dengan tulisan 
        $excel->setActiveSheetIndex(0)->setCellValue('Y2', "Kelengkapan Dokumen");
        $excel->setActiveSheetIndex(0)->setCellValue('Y3', "Edisi / Revisi");
        $excel->setActiveSheetIndex(0)->setCellValue('Y4', "Berlaku Efektif");
        $excel->setActiveSheetIndex(0)->setCellValue('Y5', "Halaman");
        $excel->setActiveSheetIndex(0)->setCellValue('AA2', "FC20.TJBTB.02");
        $excel->setActiveSheetIndex(0)->setCellValue('AA3', "00/01");
        $excel->setActiveSheetIndex(0)->setCellValue('AA2', "01 Januari 2020");
        $excel->setActiveSheetIndex(0)->setCellValue('AA3', "01 dari 01"); 
        $excel->getActiveSheet()->mergeCells('A2:X5'); // Set Merge Cell pada kolom A1 sampai E1
        $excel->getActiveSheet()->mergeCells('Y2:Z2'); // Set Merge Cell pada kolom A1 sampai E1
        $excel->getActiveSheet()->mergeCells('Y3:Z3'); // Set Merge Cell pada kolom A1 sampai E1
        $excel->getActiveSheet()->mergeCells('Y4:Z4'); // Set Merge Cell pada kolom A1 sampai E1
        $excel->getActiveSheet()->mergeCells('AA2:AC2'); 
        $excel->getActiveSheet()->mergeCells('AA3:AC3');
        $excel->getActiveSheet()->mergeCells('AA4:AC4');
        $excel->getActiveSheet()->mergeCells('AA5:AC5');
        $excel->getActiveSheet()->mergeCells('A6:A8');
        $excel->getActiveSheet()->mergeCells('B6:B8');
        $excel->getActiveSheet()->mergeCells('C6:C8');
        $excel->getActiveSheet()->mergeCells('D6:D8');
        $excel->getActiveSheet()->mergeCells('E6:E8');
        $excel->getActiveSheet()->mergeCells('F6:F8');
        $excel->getActiveSheet()->mergeCells('G6:M6');
        $excel->getActiveSheet()->mergeCells('G7:G8');
        $excel->getActiveSheet()->mergeCells('H7:K7');
        $excel->getActiveSheet()->mergeCells('L7:L8');
        $excel->getActiveSheet()->mergeCells('M7:M8');
        $excel->getActiveSheet()->mergeCells('N6:T6');
        $excel->getActiveSheet()->mergeCells('N7:N8');
        $excel->getActiveSheet()->mergeCells('O7:R7');
        $excel->getActiveSheet()->mergeCells('S7:S8');
        $excel->getActiveSheet()->mergeCells('T7:T8');
        $excel->getActiveSheet()->mergeCells('U6:U8');
        $excel->getActiveSheet()->mergeCells('V6:V8');
        $excel->getActiveSheet()->mergeCells('W6:W8');
        $excel->getActiveSheet()->mergeCells('X6:X8');
        $excel->getActiveSheet()->mergeCells('Y6:AA6');
        $excel->getActiveSheet()->mergeCells('AB6:AC6');
        $excel->getActiveSheet()->mergeCells('Y7:Y8');
        $excel->getActiveSheet()->mergeCells('Z7:Z8');
        $excel->getActiveSheet()->mergeCells('AA7:AA8');
        $excel->getActiveSheet()->mergeCells('AB7:AB8');
        $excel->getActiveSheet()->mergeCells('AC7:AC8');
        $excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(TRUE); // Set bold kolom A1
        $excel->getActiveSheet()->getStyle('A2')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
        $excel->getActiveSheet()->getStyle('A6:AC8')->getFont()->setSize(10);
        $excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1
        $excel->getActiveSheet()->getStyle('D6')->getAlignment()->setWrapText(true);
        $excel->getActiveSheet()->getStyle('F6')->getAlignment()->setWrapText(true);
        $excel->getActiveSheet()->getStyle('G7')->getAlignment()->setWrapText(true);
        $excel->getActiveSheet()->getStyle('M7')->getAlignment()->setWrapText(true);
        $excel->getActiveSheet()->getStyle('N7')->getAlignment()->setWrapText(true);
        $excel->getActiveSheet()->getStyle('S7')->getAlignment()->setWrapText(true);
        $excel->getActiveSheet()->getStyle('T7')->getAlignment()->setWrapText(true);
        $excel->getActiveSheet()->getStyle('U6')->getAlignment()->setWrapText(true);
        $excel->getActiveSheet()->getStyle('V6')->getAlignment()->setWrapText(true);
        $excel->getActiveSheet()->getStyle('X6')->getAlignment()->setWrapText(true);
        $excel->getActiveSheet()->getStyle('W6')->getAlignment()->setWrapText(true);
        $excel->getActiveSheet()->getStyle('Y7')->getAlignment()->setWrapText(true);
        $excel->getActiveSheet()->getStyle('AA7')->getAlignment()->setWrapText(true);

        // Buat header tabel nya pada baris ke 3
        $excel->setActiveSheetIndex(0)->setCellValue('A6', "NO");
        $excel->setActiveSheetIndex(0)->setCellValue('B6', "GARDU INDUK");
        $excel->setActiveSheetIndex(0)->setCellValue('C6', "PENGHANTAR");
        $excel->setActiveSheetIndex(0)->setCellValue('D6', "NO TOWER");
        $excel->setActiveSheetIndex(0)->setCellValue('E6', "TIPE TOWER");
        $excel->setActiveSheetIndex(0)->setCellValue('F6', "NILAI ARDE TERBESAR SEBELUMNYA");
        $excel->setActiveSheetIndex(0)->setCellValue('G6', "HASIL PENGUKURAN");
        $excel->setActiveSheetIndex(0)->setCellValue('G7', "ARDE & TOWER (PARALEL)");
        $excel->setActiveSheetIndex(0)->setCellValue('H7', "ARDE KAKI TOWER");
        $excel->setActiveSheetIndex(0)->setCellValue('H8', "A");
        $excel->setActiveSheetIndex(0)->setCellValue('I8', "B");
        $excel->setActiveSheetIndex(0)->setCellValue('J8', "C");
        $excel->setActiveSheetIndex(0)->setCellValue('K8', "D");
        $excel->setActiveSheetIndex(0)->setCellValue('L7', "TOWER");
        $excel->setActiveSheetIndex(0)->setCellValue('M7', "GSW LANGSUNG");
        $excel->setActiveSheetIndex(0)->setCellValue('N6', "HASIL PENGUKURAN SEMEN KONDUKTIF");
        $excel->setActiveSheetIndex(0)->setCellValue('N7', "ARDE & TOWER (PARALEL)");
        $excel->setActiveSheetIndex(0)->setCellValue('O7', "ARDE KAKI TOWER");
        $excel->setActiveSheetIndex(0)->setCellValue('O8', "A");
        $excel->setActiveSheetIndex(0)->setCellValue('P8', "B");
        $excel->setActiveSheetIndex(0)->setCellValue('Q8', "C");
        $excel->setActiveSheetIndex(0)->setCellValue('R8', "D");
        $excel->setActiveSheetIndex(0)->setCellValue('S7', "TOWER");
        $excel->setActiveSheetIndex(0)->setCellValue('T7', "GSW LANGSUNG");
        $excel->setActiveSheetIndex(0)->setCellValue('U6', "HASIL PENGUKURAN TERBESAR");
        $excel->setActiveSheetIndex(0)->setCellValue('V6', "METODE PEMASANGAN");
        $excel->setActiveSheetIndex(0)->setCellValue('W6', "ANOMALI");
        $excel->setActiveSheetIndex(0)->setCellValue('X6', "TANGGAL PENGUKURAN");
        $excel->setActiveSheetIndex(0)->setCellValue('Y6', "KEADAAN TANAH TOWER");
        $excel->setActiveSheetIndex(0)->setCellValue('Y7', "TEMBOK PENAHAN TANAH");
        $excel->setActiveSheetIndex(0)->setCellValue('Z7', "LUAS M2");
        $excel->setActiveSheetIndex(0)->setCellValue('AA7', "KONIDISI TANAH");
        $excel->setActiveSheetIndex(0)->setCellValue('AB6', "LOKASI");
        $excel->setActiveSheetIndex(0)->setCellValue('AB7', "DESA");
        $excel->setActiveSheetIndex(0)->setCellValue('AC7', "KECAMATAN");


        // Apply style header yang telah kita buat tadi ke masing-masing kolom header
        $excel->getActiveSheet()->getStyle('A6')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('B6')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('C6')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('D6')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('E6')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('F6')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('G6')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('G7')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('H7')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('H8')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('I8')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('J8')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('K8')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('L7')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('M7')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('N6')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('N7')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('O7')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('O8')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('P8')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('Q8')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('R8')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('S7')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('T7')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('U6')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('V6')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('W6')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('X6')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('Y6')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('Y7')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('Z7')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('AA7')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('AB6')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('AB7')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('AC7')->applyFromArray($style_col);


        $x = $this->m_pentanahan_bali->view();

        $no = 1; // Untuk penomoran tabel, di awal set dengan 1
        $numrow = 10; // Set baris pertama untuk isi tabel adalah baris ke 4


        foreach ($x as $data) {
            $excel->setActiveSheetIndex(0)->setCellValue('A' . $numrow, $no);
            $excel->setActiveSheetIndex(0)->setCellValue('B' . $numrow, $data->gi);
            $excel->setActiveSheetIndex(0)->setCellValue('C' . $numrow, $data->penghantar);
            $excel->setActiveSheetIndex(0)->setCellValue('D' . $numrow, $data->no_tower);
            $excel->setActiveSheetIndex(0)->setCellValue('E' . $numrow, $data->tipe_tower);
            $excel->setActiveSheetIndex(0)->setCellValue('F' . $numrow, $data->nilai_arde);
            $excel->setActiveSheetIndex(0)->setCellValue('G' . $numrow, $data->arde_tower);
            $excel->setActiveSheetIndex(0)->setCellValue('H' . $numrow, $data->arde_a);
            $excel->setActiveSheetIndex(0)->setCellValue('I' . $numrow, $data->arde_b);
            $excel->setActiveSheetIndex(0)->setCellValue('J' . $numrow, $data->arde_c);
            $excel->setActiveSheetIndex(0)->setCellValue('K' . $numrow, $data->arde_d);
            $excel->setActiveSheetIndex(0)->setCellValue('L' . $numrow, $data->tower_arde);
            $excel->setActiveSheetIndex(0)->setCellValue('M' . $numrow, $data->arde_gsw);
            $excel->setActiveSheetIndex(0)->setCellValue('N' . $numrow, $data->semen_tower);
            $excel->setActiveSheetIndex(0)->setCellValue('O' . $numrow, $data->semen_a);
            $excel->setActiveSheetIndex(0)->setCellValue('P' . $numrow, $data->semen_b);
            $excel->setActiveSheetIndex(0)->setCellValue('Q' . $numrow, $data->semen_c);
            $excel->setActiveSheetIndex(0)->setCellValue('R' . $numrow, $data->semen_d);
            $excel->setActiveSheetIndex(0)->setCellValue('S' . $numrow, $data->tower_semen);
            $excel->setActiveSheetIndex(0)->setCellValue('T' . $numrow, $data->semen_gsw);
            $excel->setActiveSheetIndex(0)->setCellValue('U' . $numrow, $data->hasil);
            $excel->setActiveSheetIndex(0)->setCellValue('V' . $numrow, $data->metode);
            $excel->setActiveSheetIndex(0)->setCellValue('W' . $numrow, $data->anomali);
            $excel->setActiveSheetIndex(0)->setCellValue('X' . $numrow, date("d-m-Y", strtotime($data->tgl)));
            $excel->setActiveSheetIndex(0)->setCellValue('Y' . $numrow, $data->tembok);
            $excel->setActiveSheetIndex(0)->setCellValue('Z' . $numrow, $data->luas);
            $excel->setActiveSheetIndex(0)->setCellValue('AA' . $numrow, $data->kondisi_tanah);
            $excel->setActiveSheetIndex(0)->setCellValue('AB' . $numrow, $data->desa);
            $excel->setActiveSheetIndex(0)->setCellValue('AC' . $numrow, $data->kecamatan);

            // Apply style row yang telah kita buat tadi ke masing-masing baris (isi tabel)
            $excel->getActiveSheet()->getStyle('A' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('B' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('C' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('D' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('E' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('F' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('G' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('H' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('I' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('J' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('K' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('L' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('M' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('N' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('O' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('P' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('Q' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('R' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('S' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('T' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('U' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('V' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('W' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('X' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('Y' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('Z' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('AA' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('AB' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('AC' . $numrow)->applyFromArray($style_row);

            $no++; // Tambah 1 setiap kali looping
            $numrow++; // Tambah 1 setiap kali looping
        }

        // Set width kolom
        $excel->getActiveSheet(0)->getStyle('G6:T6')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $excel->getActiveSheet(0)->getStyle('G6:T6')->getFill()->getStartColor()->setRGB('6B8E23');
        $excel->getActiveSheet(0)->getStyle('G7:G8')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $excel->getActiveSheet(0)->getStyle('G7:G8')->getFill()->getStartColor()->setRGB('6B8E23');
        $excel->getActiveSheet(0)->getStyle('H7:L8')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $excel->getActiveSheet(0)->getStyle('H7:L8')->getFill()->getStartColor()->setRGB('1E90FF');
        $excel->getActiveSheet(0)->getStyle('M7:M8')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $excel->getActiveSheet(0)->getStyle('M7:M8')->getFill()->getStartColor()->setRGB('6B8E23');
        $excel->getActiveSheet(0)->getStyle('N6:T6')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
         $excel->getActiveSheet(0)->getStyle('N6:T6')->getFill()->getStartColor()->setRGB('6B8E23');
         $excel->getActiveSheet(0)->getStyle('N7:N8')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $excel->getActiveSheet(0)->getStyle('N7:N8')->getFill()->getStartColor()->setRGB('6B8E23');
        $excel->getActiveSheet(0)->getStyle('O7:S8')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $excel->getActiveSheet(0)->getStyle('O7:S8')->getFill()->getStartColor()->setRGB('1E90FF');
        $excel->getActiveSheet(0)->getStyle('T7:T8')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $excel->getActiveSheet(0)->getStyle('T7:T8')->getFill()->getStartColor()->setRGB('6B8E23');

        foreach(range('G','AA') as $columnID) {
        $excel->getActiveSheet()->getColumnDimension($columnID)
        ->setAutoSize(true);
        }

        $excel->getActiveSheet()->getColumnDimension('F')->setWidth('20');
        $excel->getActiveSheet()->getColumnDimension('G')->setWidth('10');
        $excel->getActiveSheet()->getColumnDimension('M')->setWidth('10');
        $excel->getActiveSheet()->getColumnDimension('N')->setWidth('10');
        $excel->getActiveSheet()->getColumnDimension('U')->setWidth('10');
        $excel->getActiveSheet()->getColumnDimension('V')->setWidth('15');
        $excel->getActiveSheet()->getColumnDimension('W')->setWidth('20');
        $excel->getActiveSheet()->getColumnDimension('Y')->setWidth('10');

        $excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true); // Set width kolom A
        $excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true); // Set width kolom B
        $excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true); // Set width kolom C
        $excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true); // Set width kolom D
        $excel->getActiveSheet()->getColumnDimension('G7')->setAutoSize(true); // Set width kolom D
        $excel->getActiveSheet()->getColumnDimension('X')->setAutoSize(true); // Set width kolom D
        $excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true); // Set width kolom E
        $excel->getActiveSheet()->getColumnDimension('AB')->setAutoSize(true); // Set width kolom E
        $excel->getActiveSheet()->getColumnDimension('AC')->setAutoSize(true); // Set width kolom A

        // Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
        $excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);

        // Set orientasi kertas jadi LANDSCAPE
        $excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);

        // Set judul file excel nya
        $excel->getActiveSheet(0)->setTitle("Laporan Data");
        $excel->setActiveSheetIndex(0);

        // Proses file excel
        ob_end_clean();
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="Pengukuran Pentanahan UIT JBM.xlsx"'); // Set nama file excel nya
        header('Cache-Control: max-age=0');

        $write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
        $write->save('php://output');
    }

    // hapus beberapa data
    public function delete_some()
    {
        $id_pentanahan = $this->input->post('id_pentanahan');
        $res = 0;

        foreach ($id_pentanahan as $v) {
            if ($this->delete($v, 1)) {
                $res = $res + 1;
            } else {
                $res = $res + 0;
            }
        }
        echo json_encode($res);
    }
}
