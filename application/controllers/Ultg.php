<?php
if (!defined('BASEPATH')) exit(header('Location:../'));

class Ultg extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('m_ultg');
    }

    public function index()
    {
        $data['title'] = "ULTG";
        $this->my_theme('v_ultg', $data);
    }

    // get data
    public function get_data()
    {
        $columns = [
            "ultg_id",
            "ultg_kode",
            "ultg_nama",
            "ultg_status",

        ];
        $columns_search = [
            "ultg_kode",
            "ultg_nama",

        ];
        $draw = intval($this->input->post("draw"));
        $where     = "";
        // $f_mmsi = $this->input->post("f_mmsi");
        // $where .= " AND fd.mmsi = '$f_mmsi' ";

        $search = $this->input->post("search");
        $search = $search['value'];
        if (isset($search) && $search != "") {
            $where .= "AND (";
            for ($i = 0; $i < count($columns_search); $i++) {
                $where .= " LOWER(" . $columns_search[$i] . ") LIKE LOWER('%" . ($search) . "%') OR ";
            }
            $where = substr_replace($where, "", -3);
            $where .= ')';
        }
        $totalRecords = $this->m_ultg->get_total($where);
        $length = intval($this->input->post("length"));
        $length = $length < 0 ? $totalRecords : $length;
        $start = intval($this->input->post("start"));
        if (isset($start) && $length != '-1') {
            $limit = "limit " . intval($length) . " offset " . intval($start);
        }
        $records = array();
        $records["data"] = array();
        $order = $this->input->post("order");
        $col = 0;
        $dir = "";
        if (!empty($order)) {
            foreach ($order as $o) {
                $col = $o['column'];
                $dir = $o['dir'];
            }
        }

        if ($dir != "asc" && $dir != "desc") {
            $dir = "desc";
        }

        if (!isset($columns[$col])) {
            $order = null;
        } else {
            $order = "ORDER BY " . $columns[$col] . " " . $dir;
        }

        $data = $this->m_ultg->get_data($columns, $where, $order, $limit);
        $no   = 1 + $start;
        foreach ($data as $row) {
            $action = "";
            $id = $row->ultg_id;
            $status = $row->ultg_status == '1' ? '<span class="badge badge-success">Aktif</span>' : '<span class="badge badge-danger">Non Aktif</span>';

            $action = '<a href="javascript:;" onclick="set_val(\'' . $id . '\')" class="btn btn-info btn-sm" title="Edit"><i class="fa fa-pencil-alt"></i></a>&nbsp;<a href="javascript:;" onclick="del(\'' . $id . '\')" class="btn btn-danger btn-sm" title="Hapus"><i class="fa fa-trash"></i></a>';
            $records["data"][] = array(
                $no++,
                $row->ultg_kode,
                $row->ultg_nama,
                $status,
                $action,
            );
        }

        $records["draw"] = $draw;
        $records["recordsTotal"] = $totalRecords;
        $records["recordsFiltered"] = $totalRecords;

        echo json_encode($records);
    }

    // save
    public function save()
    {
        $data = $this->input->post();
        unset($data['act']);
        if ($this->input->post('act') == 'add') {
            unset($data['ultg_id']);
            $res = $this->m_ultg->insert($data);
        } else {
            $res = $this->m_ultg->update($data);
        }
        echo json_encode($res);
    }


    // get data upt by id
    public function edit($id)
    {
        $res = null;
        if ($id > 0 && !empty($id)) {
            $res = $this->m_ultg->edit($id);
        }

        echo json_encode($res);
    }


    // delete
    public function delete($id)
    {
        $res = $this->m_ultg->delete($id);
        echo json_encode($res);
    }
}
