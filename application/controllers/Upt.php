<?php
if (!defined('BASEPATH')) exit(header('Location:../'));

class Upt extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('m_upt');
    }

    public function index()
    {
        $data['title'] = "UPT";
        $this->my_theme('v_upt', $data);
    }

    // get data krisis
    public function get_data()
    {
        $columns = [
            "upt_id",
            "upt_kode",
            "upt_nama",
            "upt_status",

        ];
        $columns_search = [
            "upt_nama",
            "upt_kode",
        ];
        $draw = intval($this->input->post("draw"));
        $where     = "";
        // $f_mmsi = $this->input->post("f_mmsi");
        // $where .= " AND fd.mmsi = '$f_mmsi' ";

        $search = $this->input->post("search");
        $search = $search['value'];
        if (isset($search) && $search != "") {
            $where .= "AND (";
            for ($i = 0; $i < count($columns_search); $i++) {
                $where .= " LOWER(" . $columns_search[$i] . ") LIKE LOWER('%" . ($search) . "%') OR ";
            }
            $where = substr_replace($where, "", -3);
            $where .= ')';
        }
        $totalRecords = $this->m_upt->get_total($where);
        $length = intval($this->input->post("length"));
        $length = $length < 0 ? $totalRecords : $length;
        $start = intval($this->input->post("start"));
        if (isset($start) && $length != '-1') {
            $limit = "limit " . intval($length) . " offset " . intval($start);
        }
        $records = array();
        $records["data"] = array();
        $order = $this->input->post("order");
        $col = 0;
        $dir = "";
        if (!empty($order)) {
            foreach ($order as $o) {
                $col = $o['column'];
                $dir = $o['dir'];
            }
        }

        if ($dir != "asc" && $dir != "desc") {
            $dir = "desc";
        }

        if (!isset($columns[$col])) {
            $order = null;
        } else {
            $order = "ORDER BY " . $columns[$col] . " " . $dir;
        }

        $data = $this->m_upt->get_data($columns, $where, $order, $limit);
        $no   = 1 + $start;
        foreach ($data as $row) {
            $action = "";
            $id = $row->upt_id;
            // $isi = "$id|$row->group_name|$row->group_aktif";

            $status = $row->upt_status == '1' ? '<span class="badge badge-success">Aktif</span>' : '<span class="badge badge-danger">Non Aktif</span>';
            $action = '<a href="javascript:;" onclick="set_val(\'' . $id . '\')" class="btn btn-info btn-sm" title="Edit"><i class="fa fa-pencil-alt"></i></a>&nbsp;<a href="javascript:;" onclick="del(\'' . $id . '\')" class="btn btn-danger btn-sm" title="Hapus"><i class="fa fa-trash"></i></a>';
            $records["data"][] = array(
                $no++,
                $row->upt_kode,
                $row->upt_nama,
                $status,
                $action,
            );
        }

        $records["draw"] = $draw;
        $records["recordsTotal"] = $totalRecords;
        $records["recordsFiltered"] = $totalRecords;

        echo json_encode($records);
    }

    // save
    public function save()
    {
        $kab_kode = $this->input->post('kab_kode');
        $data = $this->input->post();
        unset($data['act']);
        unset($data['kab_kode']);
        if ($this->input->post('act') == 'add') {
            unset($data['upt_id']);
            $res = $this->m_upt->insert($data);
        } else {
            $res = $this->m_upt->update($data);
        }

        if (count($kab_kode) > 0 && $res > 0) {
            $data_kab = [];
            foreach ($kab_kode as $v) {
                $data_kab[] = [
                    'upt_id' => $res,
                    'reg_code' => $v
                ];
            }

            $res = $this->m_upt->insert_kab($data_kab, $res);
        }

        echo json_encode($res);
    }

    // get data krisis by id
    public function edit($id)
    {
        $res = null;
        if ($id > 0 && !empty($id)) {
            $res = $this->m_upt->edit($id);
            $res->upt_data = $this->m_upt->get_daerah_upt($id);
        }

        echo json_encode($res);
    }


    // delete
    public function delete($id)
    {
        $res = $this->m_upt->delete($id);
        echo json_encode($res);
    }


    // get_daerah
    public function get_daerah()
    {
        $request = $this->input->post('q');
        $res = $this->m_upt->get_daerah($request);
        echo json_encode($res);
    }
}
