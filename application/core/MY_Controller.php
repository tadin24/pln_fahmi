<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{

    protected $userData = "";

    public function __construct()
    {
        parent::__construct();
        // check_login_user();
        $this->load->model("m_common");
        require_once('vendor/autoload.php');
    }

    public function my_theme($url, $data)
    {

        $footer = array(
            "copyright" => "2021 @ copyright all reserved by Tim IT",
            "logout"    => site_url("auth/logout"),
        );

        $header = array(
            "title"    => !empty($data['title']) ? $data['title'] : "PLN",
            "user_id"   => $this->session->userdata('user_id'),
            "username"  => $this->session->userdata('username'),
        );

        $sidebar["menu"] = $this->m_common->gen_menu(0, $this->session->userdata('user_id')); ## Lukman Menu
        // $sidebar["menu"] = "";

        $this->load->view('template/header', $header);
        $this->load->view('template/sidebar', $sidebar);
        $this->load->view($url, $data);
        $this->load->view('template/footer', $footer);
    }
}
