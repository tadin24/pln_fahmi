<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

//-- check logged user
if (!function_exists('check_login_user')) {
    function check_login_user()
    {
        $ci = get_instance();
        if ($ci->session->userdata('is_login') != TRUE) {
            $ci->session->sess_destroy();
            redirect(base_url('auth'));
        }
    }
}
