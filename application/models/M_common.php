<?php
class M_common extends CI_Model
{

    public function gen_menu($parent = 0, $user_id = 0)
    {
        $s = "SELECT
                    m.*
                from
                    (
                    select
                        *
                    from
                        ms_user
                    where
                        user_id = $user_id) ug
                join ms_group_menu mgm on
                    mgm.group_id = ug.group_id
                join ms_menu m on
                    m.menu_id = mgm.menu_id 
                where
                    m.menu_aktif = true
                    and m.menu_parent_id = $parent
                order by
                    m.menu_kode ";
        $ret = "";
        $d  = $this->db->query($s);

        foreach ($d->result() as $key => $value) {
            // cek ada anak
            $sa = "SELECT
                    m.*
                from
                    (
                    select
                        *
                    from
                        ms_user
                    where
                        user_id = $user_id) ug
                join ms_group_menu mgm on
                    mgm.group_id = ug.group_id
                join ms_menu m on
                    m.menu_id = mgm.menu_id 
                where
                    m.menu_aktif = true
                    and m.menu_parent_id = $value->menu_id";
            $da  = $this->db->query($sa);
            if ($da->num_rows() == 0 && $parent == 0) { // tidak ada anak
                $url = !empty($value->menu_link) ? $value->menu_link : '#';
                $ret .= "<li class='nav-item'>
                            <a class='nav-link' href='" . base_url($url) . "'>
                                <i class='$value->menu_icon'></i>
                                <span>$value->menu_name</span>
                            </a>
                        </li>";
            } elseif ($da->num_rows() > 0 && $parent == 0) { // ada anak tapi parent ke 0
                $ret .= '<li class="nav-item">
                            <a class="nav-link collapsed" href="javascript:;" data-toggle="collapse" data-target="#collapse' . $value->menu_id . '" aria-expanded="true" aria-controls="collapse' . $value->menu_id . '">
                                <i class="' . $value->menu_icon . '"></i>
                                <span>' . $value->menu_name . '</span>
                            </a>
                            <div id="collapse' . $value->menu_id . '" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                                <div class="bg-white py-2 collapse-inner rounded">';
                $ret .= $this->gen_menu($value->menu_id, $user_id);
                $ret .= '
                                </div>
                            </div>
                        </li>';
            } elseif ($da->num_rows() == 0 && $parent > 0) { // tidak ada anak,  tapi bukan parent ke 0
                $url = !empty($value->menu_link) ? $value->menu_link : '#';
                $ret .=  '<a class="collapse-item" href="' . base_url($url) . '">' . $value->menu_name . '</a>';
            } else { // ada anak dan bukan parent ke 0
                $ret .= '<div>
                            <a class="collapse-item sub-nav-link collapsed" href="javascript:;" data-toggle="collapse" data-target="#collapse' . $value->menu_id . '" aria-expanded="true" aria-controls="collapse' . $value->menu_id . '">
                                <i class="' . $value->menu_icon . '"></i>
                                <span>' . $value->menu_name . '</span>
                            </a>
                            <div id="collapse' . $value->menu_id . '" class="collapse" aria-labelledby="headingTwo" data-parent="#collapse' . $value->menu_parent_id . '">
                                <div class="bg-white py-2">';
                $ret .= $this->gen_menu($value->menu_id, $user_id);
                $ret .= '
                                </div>
                            </div>
                        </div>';
            }
        }
        return $ret;
    }


    // Tadin(3 Juli 2021) : Fungsi mengambil data daerah
    // get wilayah
    public function get_wilayah($parent_code)
    {
        $where = '';
        if (gettype($parent_code) == 'array') {
            $where = " AND reg_parent in (" . implode(",", $parent_code) . ")";
        } else {
            $where = " AND reg_parent = '$parent_code'";
        }

        $sql =
            "SELECT
                reg_code,
                reg_name,
                reg_parent
            from
                ms_region mr
            where
                reg_active = 1
                $where
            order by
                mr.reg_name";

        $res = $this->db->query($sql)->result();
        return $res;
    }


    // Tadin(3 Juli 2021) : Fungsi mengambil data penghantar gardu induk
    // get penghantar
    public function get_penghantar($id = null)
    {
        $where = '';
        if (gettype($id) == 'array') {
            $where = " AND kab_kode in (" . implode(",", $id) . ")";
        } else {
            if ($id != '' && $id != 0 && !empty($id)) {
                $where = " AND kab_kode = '$id'";
            } else {
            }
        }
        $sql =
            "SELECT
                *
            from
                penghantar_gi
            where
                penghantar_status = 1
                $where
            order by
                penghantar_nama";

        $res = $this->db->query($sql)->result();
        return $res;
    }


    // Tadin(3 Juli 2021) : Fungsi mengambil data gardu induk
    // get GI
    public function get_gi($id = null)
    {
        $where = '';
        if (gettype($id) == 'array') {
            $where = " AND kab_kode in (" . implode(",", $id) . ")";
        } else {
            if ($id != '' && $id != 0 && !empty($id)) {
                $where = " AND kab_kode = '$id'";
            } else {
            }
        }
        $sql =
            "SELECT
                *
            from
                ms_garduinduk
            where
                gi_status = 1
                $where
            order by
                gi_nama";

        $res = $this->db->query($sql)->result();
        return $res;
    }
}
