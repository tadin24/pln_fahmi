<?php
class M_dashboard extends CI_Model
{
    public function get_data($type, $upt_id)
    {
        $join_on = "";
        $group_by = $type;
        if ($type == "klali") {
            $join_on = " k.klali = mr.reff_id ";
            // $selector =
            //     " case
            //         when klali = 1 then 'AMAN'
            //         when klali = 2 then 'WASPADA'
            //         when klali = 3 then 'KRITIS'
            //         else 'BELUM DIASESMENT'
            //     end as label ";
        } elseif ($type == "klapo") {
            $join_on = " k.klapo = mr.reff_id ";
            // $selector =
            //     " case
            //         when klapo = 1 then 'AMAN'
            //         when klapo = 2 then 'WASPADA'
            //         when klapo = 3 then 'KRITIS'
            //         else 'BELUM DIASESMENT'
            //     end as label ";
        } elseif ($type == "klahu") {
            $join_on = " k.klahu = mr.reff_id ";
            // $selector =
            //     " case
            //         when klahu = 1 then 'AMAN'
            //         when klahu = 2 then 'WASPADA'
            //         when klahu = 3 then 'KRITIS'
            //         else 'BELUM DIASESMENT'
            //     end as label ";
        }elseif ($type == "prioritas") {
            $join_on = " k.prioritas = mr.reff_id ";
            // $selector =
            //     " case
            //         when klahu = 1 then 'AMAN'
            //         when klahu = 2 then 'WASPADA'
            //         when klahu = 3 then 'KRITIS'
            //         else 'BELUM DIASESMENT'
            //     end as label ";
        }
        $sql =
            "SELECT
                count(reff_id) as value,
                reff_nama as label
            from
                ms_reff mr
            left join krisis k on
                $join_on
            where
                mr.reff_status = 1
                and k.upt = $upt_id
            group by
                label
            order by
                mr.reff_kode
            ";
        // $sql =
        // "SELECT
        //     count(*) as value,
        //     $selector
        // from
        //     krisis k
        // where 
        //     k.upt = $upt_id
        // group by
        //     $group_by
        // ";

        $res = $this->db->query($sql)->result();
        return $res;
    }




    public function get_total_user()
    {
        $sql =
            "SELECT
                count(*) value,
                mg.group_name label
            from
                ms_user mu
            inner join ms_group mg on
                mg.group_id = mu.group_id
            group by
                mg.group_id
            order by
                group_name
            ";

        $res = $this->db->query($sql)->result();
        return $res;
    }


    // get data upt
    public function get_upt()
    {
        $sql =
            "SELECT
                *
            from
                ms_upt mu
            order by
                mu.upt_nama
            ";

        $res = $this->db->query($sql)->result();
        return $res;
    }


    // get data upt
    public function get_data_all()
    {
        $sql =
            "SELECT
                mu.upt_nama ,
                coalesce(count(k.klali), 0 ) as value,
                k.klali
            from
                ms_upt mu
            left join krisis k on
                k.upt = mu.upt_id
            where
                mu.upt_status is true
            group by
                mu.upt_nama,
                k.klali
            order by
                mu.upt_nama,
                k.klali";

        $res = $this->db->query($sql)->result_array();
        return $res;
    }

     



    // get data upt
    public function get_data_upt($upt_id, $jenis = "")
    {
        if ($jenis == "klali") {
            $join_on = "k.klali = mr.reff_id";
        } else if ($jenis == "klapo") {
            $join_on = "k.klapo = mr.reff_id";
        } else if ($jenis == "prioritas") {
            $join_on = "k.prioritas = mr.reff_id";
        }
        $sql =
            "SELECT
                COALESCE (count(reff_id), 0) as value,
                reff_nama as label
            from
                ms_reff mr
            left join krisis k on
                $join_on
            where
                mr.reff_status = 1
                and k.upt = $upt_id
            group by
                label
            order by
                label";

        $res = $this->db->query($sql)->result();
        return $res;
    }

}
