<?php
class M_gi extends CI_Model
{

    // insert
    public function insert($data)
    {
        $this->db->insert('ms_garduinduk', $data);
        if ($this->db->affected_rows() > 0) {
            return $this->db->insert_id();
        } else {
            return false;
        }
    }


    // update
    public function update($data)
    {
        $this->db->where('gi_id', $data['gi_id']);
        $res = $this->db->update('ms_garduinduk', $data);
        if ($res) {
            return $data['gi_id'];
        } else {
            return false;
        }
    }


    // edit
    public function edit($id)
    {
        $res = $this->db->get_where('ms_garduinduk', array('gi_id' => $id))->row();
        return $res;
    }


    // delete
    public function delete($id)
    {
        $res = false;
        $this->db->from('ms_garduinduk');
        $this->db->where('gi_id', $id);
        $file = $this->db->get();
        $this->db->where('gi_id', $id);
        $this->db->delete('ms_garduinduk');
        if ($this->db->affected_rows() > 0) {
            $res = true;
        }
        return $res;
    }

    public function get_total($where)
    {
        $sql =
            "SELECT
                count(*) total
            from
                ms_garduinduk mg
            left join ms_region kec 
                on kec.reg_code = kec_kode
            where
                0 = 0 $where";

        $res = $this->db->query($sql)->row()->total;
        return $res;
    }

    // get data
    public function get_data($columns, $where, $order, $limit)
    {
        $selector = implode(",", $columns);
        $sql =
            "SELECT
                $selector
            from
                ms_garduinduk mg
            left join ms_region kec 
                on kec.reg_code = kec_kode
            where
                0 = 0 $where
            $order $limit";

        $res = $this->db->query($sql)->result();
        return $res;
    }
}
