<?php
class M_krisis extends CI_Model
{

    //View Krisis Export
    public function view()
    {
        // return $this->db->get('krisis')->result();
        return $this->db
            ->select('k.*,mu.upt_nama,mu2.ultg_nama')
            ->from('krisis k')
            ->join('ms_upt mu', 'k.upt = mu.upt_id', 'inner')
            ->join('ms_ultg mu2', 'k.ultg = mu2.ultg_id', 'inner')
            ->get()->result();
    }


    // get upt
    public function get_upt()
    {
        $sql =
            "SELECT
                *
            from
                ms_upt
            order by
                upt_nama";

        $res = $this->db->query($sql)->result();
        return $res;
    }


    // get ultg
    public function get_ultg()
    {
        $sql =
            "SELECT
                *
            from
                ms_ultg
            order by
                ultg_nama";

        $res = $this->db->query($sql)->result();
        return $res;
    }


    // get penghantar
    public function get_penghantar()
    {
        $sql =
            "SELECT
                *
            from
                penghantar_kritis
            order by
                penghantar_nama";

        $res = $this->db->query($sql)->result();
        return $res;
    }


    // get detail
    public function data_details($id)
    {

        // return $this->db->get_where('krisis', $data);
        $res = $this->db
            ->select('k.*,mu.upt_nama,mu2.ultg_nama')
            ->from('krisis k')
            ->join('ms_upt mu', 'k.upt = mu.upt_id', 'inner')
            ->join('ms_ultg mu2', 'k.ultg = mu2.ultg_id', 'inner')
            ->where('id_krisis', $id)
            ->get()->row();
        return $res;
    }



    // Buat sebuah fungsi untuk melakukan insert lebih dari 1 data
    public function insert_multiple($data)
    {
        $this->db->insert_batch('krisis', $data);
    }
    // get total
    public function get_total($where)
    {
        $sql =
            "SELECT
                count(*) total
            from
                krisis k
            where
                0 = 0 $where";

        $res = $this->db->query($sql)->row()->total;
        return $res;
    }


    // Fungsi untuk melakukan proses upload file
    public function upload_file($filename)
    {
        $this->load->library('upload'); // Load librari upload

        $config['upload_path'] = './excel/';
        $config['allowed_types'] = 'xlsx';
        $config['max_size']  = '2048';
        $config['overwrite'] = true;
        $config['file_name'] = $filename;

        $this->upload->initialize($config); // Load konfigurasi uploadnya
        if ($this->upload->do_upload('file')) { // Lakukan upload dan Cek jika proses upload berhasil
            // Jika berhasil :
            $return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
            return $return;
        } else {
            // Jika gagal :
            $return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
            return $return;
        }
    }


    // insert
    public function insert($data)
    {
        $this->db->insert('krisis', $data);
        if ($this->db->affected_rows() > 0) {
            return $this->db->insert_id();
        } else {
            return false;
        }
    }


    // update
    public function update($data)
    {
        $this->db->where('id_krisis', $data['id_krisis']);
        $res = $this->db->update('krisis', $data);
        if ($res) {
            return $data['id_krisis'];
        } else {
            return false;
        }
    }


    // edit
    public function edit($id)
    {
        $res = $this->db->get_where('krisis', array('id_krisis' => $id))->row();
        return $res;
    }


    // delete
    public function delete($id)
    {
        $res = false;
        $this->db->select('foto1,foto2,foto3,foto4,kkp_file,kelling_file,kelpo_file,kelfo_file,kelrab_file,kelde_file');
        $this->db->from('krisis');
        $this->db->where('id_krisis', $id);
        $file = $this->db->get();
        if ($file->num_rows() > 0) {
            foreach ($file->row_array() as $key => $value) {
                if (!empty($value)) {
                    if ($key == 'foto1' || $key == 'foto2' || $key == 'foto3' || $key == 'foto4') {
                        if (file_exists('assets/images/' . $value)) {
                            unlink('assets/images/' . $value);
                        }
                    } else {
                        if (file_exists('assets/documents/' . $value)) {
                            unlink('assets/documents/' . $value);
                        }
                    }
                }
            }
        }
        $this->db->where('id_krisis', $id);
        $this->db->delete('krisis');
        if ($this->db->affected_rows() > 0) {
            $res = true;
        }
        return $res;
    }


    // get name file uploaded by form to check existence of file
    public function get_file($key, $id)
    {
        $this->db->select($key);
        $this->db->from('krisis');
        $this->db->where('id_krisis', $id);
        $res = $this->db->get()->row_array();
        return $res;
    }


    // get data
    public function get_data($columns, $where, $order, $limit)
    {
        $selector = implode(",", $columns);
        $sql =
            "SELECT
                $selector
            from
                krisis k
            inner join
                ms_upt on k.upt = ms_upt.upt_id
            inner join
                ms_ultg on k.ultg = ms_ultg.ultg_id
            where
                0 = 0 $where
            $order $limit";

        $res = $this->db->query($sql)->result();
        return $res;
    }

    function show_data()
    {
        $total = $this->db->query("SELECT * FROM krisis");
        return $total;
    }

    public function get_a_location($where = array())
    {
        return $this->db
            ->select('*')
            ->where($where)
            ->get('krisis')
            ->row();
    }

    public function data_details_tower($id)
    {

        // return $this->db->get_where('krisis', $data);
        $res = $this->db
            ->select('k.*,mu.upt_nama,mu2.ultg_nama')
            ->from('krisis k')
            ->join('ms_upt mu', 'k.upt = mu.upt_id', 'inner')
            ->join('ms_ultg mu2', 'k.ultg = mu2.ultg_id', 'inner')
            ->where('id_krisis', $id)
            ->get()->row();
        return $res;
    }


    // opt reff
    public function opt_reff($id)
    {
        $this->db->select('reff_id,reff_nama,reff_kode');
        $this->db->from('ms_reff');
        $this->db->where('reffcat_id', $id);
        $this->db->where('reff_status', '1');
        $this->db->order_by('reff_kode', 'asc');
        $res = $this->db->get()->result();
        return $res;
    }


    // get list name of file
    public function get_list_file($id)
    {
        $list_id = implode(',', $id);
        $res = $this->db->query("SELECT * from krisis where id_krisis in ($list_id)")->result_array();
        return $res;
    }
}
