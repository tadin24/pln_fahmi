<?php
class M_ms_reff extends CI_model
{
    // get total data
    public function get_total($where)
    {

        $sql = "SELECT
                    count(reff_id) as total
                from
                    ms_reff mr
                where
                    0 = 0
                    $where
        ";

        $res = $this->db->query($sql);
        $result = $res->row()->total;
        return $result;
    }


    // get data
    public function get_data($columns, $where, $order, $limit)
    {

        $selector = implode(",", $columns);
        $sql = "SELECT
                    $selector
                from
                    ms_reff mr
                where
                    0 = 0 $where
                $order $limit
        ";

        $res = $this->db->query($sql);
        $result = $res->result();
        return $result;
    }


    // add
    public function add($data)
    {
        $res = "";
        $this->db->insert("ms_reff", $data);
        if ($this->db->affected_rows() > 0) {
            $res = "true";
        } else {
            $res = "false";
        }
        return $res;
    }


    // update
    public function update($data, $id)
    {
        $res = "";
        $this->db->where("reff_id", $id);
        $result = $this->db->update("ms_reff", $data);
        if ($result) {
            $res = "true";
        } else {
            $res = "false";
        }
        return $res;
    }


    // delete
    public function delete($id)
    {
        $res = "";
        $this->db->where("reff_id", $id);
        $this->db->delete("ms_reff");
        if ($this->db->affected_rows() > 0) {
            $res = "true";
        } else {
            $res = "false";
        }
        return $res;
    }


    // cek reff_kode
    public function cek_reff_kode($where)
    {
        $sql = "SELECT * from ms_reff where 0=0 $where";
        $res = $this->db->query($sql)->num_rows();

        return $res;
    }


    // cek opt reffcat
    public function opt_reffcat()
    {
        $sql = "SELECT * from ms_reffcat mr where mr.reffcat_status = 1 order by mr.reffcat_nama";
        $res = $this->db->query($sql)->result();

        return $res;
    }
}
