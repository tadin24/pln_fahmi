<?php
class M_ms_reffcat extends CI_model
{
    // get total data
    public function get_total($where)
    {

        $sql = "SELECT
                    count(reffcat_id) as total
                from
                    ms_reffcat mr
                where
                    0 = 0
                    $where
        ";

        $res = $this->db->query($sql);
        $result = $res->row()->total;
        return $result;
    }


    // get data
    public function get_data($columns, $where, $order, $limit)
    {

        $selector = implode(",", $columns);
        $sql = "SELECT
                    $selector
                from
                    ms_reffcat mr
                where
                    0 = 0 $where
                $order $limit
        ";

        $res = $this->db->query($sql);
        $result = $res->result();
        return $result;
    }


    // add
    public function add($data)
    {
        $res = "";
        $this->db->insert("ms_reffcat", $data);
        if ($this->db->affected_rows() > 0) {
            $res = "true";
        } else {
            $res = "false";
        }
        return $res;
    }


    // update
    public function update($data, $id)
    {
        $res = "";
        $this->db->where("reffcat_id", $id);
        $result = $this->db->update("ms_reffcat", $data);
        if ($result) {
            $res = "true";
        } else {
            $res = "false";
        }
        return $res;
    }


    // delete
    public function delete($id)
    {
        $res = "";
        $this->db->where("reffcat_id", $id);
        $this->db->delete("ms_reffcat");
        if ($this->db->affected_rows() > 0) {
            $res = "true";
        } else {
            $res = "false";
        }
        return $res;
    }


    // cek reffcat_nama
    public function cek_reffcat_nama($where)
    {
        $sql = "SELECT * from ms_reffcat where 0=0 $where";
        $res = $this->db->query($sql)->num_rows();

        return $res;
    }
}
