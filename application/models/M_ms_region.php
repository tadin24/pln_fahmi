<?php
class M_ms_region extends CI_model
{
    // get total data
    public function get_total($where)
    {

        $sql = "SELECT
                    count(*) total
                from
                    ms_region mr
                left join ms_region parent on
                    parent.reg_code = mr.reg_parent
                left join (
                    select
                        count(*) total, mr2.reg_parent
                    from
                        ms_region mr2
                    group by
                        mr2.reg_parent) child on
                    child.reg_parent = mr.reg_code
                where
                    0 = 0 $where
        ";

        $res = $this->db->query($sql)->row()->total;
        return $res;
    }


    // get data
    public function get_data($columns, $where, $order, $limit)
    {

        $selector = implode(",", $columns);
        $sql = "SELECT
                    $selector
                from
                    ms_region mr
                left join ms_region parent on
                    parent.reg_code = mr.reg_parent
                left join (
                    select
                        count(*) total, mr2.reg_parent
                    from
                        ms_region mr2
                    group by
                        mr2.reg_parent) child on
                    child.reg_parent = mr.reg_code 
                where
                    0 = 0 $where
                $order $limit
        ";

        $res = $this->db->query($sql)->result();
        return $res;
    }


    // add
    public function add($data)
    {
        $res = "";
        $this->db->insert("ms_region", $data);
        if ($this->db->affected_rows() > 0) {
            $res = "true";
        } else {
            $res = "false";
        }
        return $res;
    }


    // update
    public function update($data, $id)
    {
        $res = "";
        $this->db->where("reg_code", $id);
        $result = $this->db->update("ms_region", $data);
        if ($result) {
            $res = "true";
        } else {
            $res = "false";
        }
        return $res;
    }


    // delete
    public function delete($id)
    {
        $res = "";
        $this->db->where("reg_code", $id);
        $this->db->delete("ms_region");
        if ($this->db->affected_rows() > 0) {
            $res = "true";
        } else {
            $res = "false";
        }
        return $res;
    }


    // cek reg_code
    public function cek_reg_code($where)
    {
        $sql = "SELECT * from ms_region where 0=0 $where";
        $res = $this->db->query($sql)->num_rows();

        return $res;
    }


    // get_reg
    public function get_region()
    {
        $sql = "SELECT * from ms_region order by reg_code";
        $res = $this->db->query($sql)->result();

        return $res;
    }


    // update child
    public function update_child($data)
    {
        $res = null;
        $sql = "SELECT * from ms_region where reg_parent like '" . $data['reg_code'] . "%' order by reg_code";
        $child = $this->db->query($sql);
        // echo "<pre>";
        // print_r($child->result());
        // echo "</pre>";
        // die;

        if ($child->num_rows() > 0) {
            $data_child = [];
            foreach ($child->result() as $key => $value) {
                $data_child[] = [
                    "reg_code" => $value->reg_code,
                    "reg_level" => intval($value->reg_level) - intval($data["reg_level_lama"]) + intval($data["reg_level"]),
                ];

                $this->db->update_batch("ms_region", $data_child, "reg_code");
                $res = "true";
            }
        } else {
            $res = "true";
        }

        return $res;
    }
}
