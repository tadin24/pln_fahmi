<?php
class M_penghantar extends CI_Model
{

    //View Krisis Export
    public function view()
    {
        return $this->db->get('penghantar_kritis')->result();
    }

     // insert
    public function insert($data)
    {
        $this->db->insert('penghantar_kritis', $data);
        if ($this->db->affected_rows() > 0) {
            return $this->db->insert_id();
        } else {
            return false;
        }
    }


    // update
    public function update($data)
    {
        $this->db->where('penghantar_kritis_id', $data['penghantar_kritis_id']);
        $res = $this->db->update('penghantar_kritis', $data);
        if ($res) {
            return $data['penghantar_kritis_id'];
        } else {
            return false;
        }
    }


    // edit
    public function edit($id)
    {
        $res = $this->db->get_where('penghantar_kritis', array('penghantar_kritis_id' => $id))->row();
        return $res;
    }


    // delete
    public function delete($id)
    {
        $res = false;
        $this->db->from('penghantar_kritis');
        $this->db->where('penghantar_kritis_id', $id);
        $file = $this->db->get();
        $this->db->where('penghantar_kritis_id', $id);
        $this->db->delete('penghantar_kritis');
        if ($this->db->affected_rows() > 0) {
            $res = true;
        }
        return $res;
    }

     public function get_total($where)
    {
        $sql =
            "SELECT
                count(*) total
            from
                penghantar_kritis k
            where
                0 = 0 $where";

        $res = $this->db->query($sql)->row()->total;
        return $res;
    }

     // get data
    public function get_data($columns, $where, $order, $limit)
    {
        $selector = implode(",", $columns);
        $sql =
            "SELECT
                $selector
            from
                penghantar_kritis k
            where
                0 = 0 $where
            $order $limit";

        $res = $this->db->query($sql)->result();
        return $res;
    }

}