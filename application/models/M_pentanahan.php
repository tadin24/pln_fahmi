<?php
class M_pentanahan extends CI_Model
{
    public function get_total($where)
    {
        $sql =
            "SELECT
                count(*) total
            from
                pentanahan p
            where
                0 = 0 $where";

        $res = $this->db->query($sql)->row()->total;
        return $res;
    }


    // get detail
    public function data_details($id)
    {

        $res = $this->db
            ->select('p.*')
            ->from('pentanahan p')
            ->where('id_pentanahan', $id)
            ->get()->row();
        return $res;
    }


    // Buat sebuah fungsi untuk melakukan insert lebih dari 1 data
    public function insert_multiple($data)
    {
        $this->db->insert_batch('pentanahan', $data);
    }


    // insert
    public function insert($data)
    {
        $this->db->insert('pentanahan', $data);
        if ($this->db->affected_rows() > 0) {
            return $this->db->insert_id();
        } else {
            return false;
        }
    }


    // update
    public function update($data)
    {
        $this->db->where('id_pentanahan', $data['id_pentanahan']);
        $res = $this->db->update('pentanahan', $data);
        if ($res) {
            return $data['id_pentanahan'];
        } else {
            return false;
        }
    }


    // edit
    public function edit($id)
    {
        $res = $this->db->get_where('pentanahan', array('id_pentanahan' => $id))->row();
        return $res;
    }


    // delete
    public function delete($id)
    {
        $res = false;
        $this->db->from('pentanahan');
        $this->db->where('id_pentanahan', $id);
        $this->db->delete('pentanahan');
        if ($this->db->affected_rows() > 0) {
            $res = true;
        }
        return $res;
    }


    // get data
    public function get_data($columns, $where, $order, $limit)
    {
        $selector = implode(",", $columns);
        $sql =
            "SELECT
                $selector
            from
                pentanahan
            where
                0 = 0 $where
            $order $limit";

        $res = $this->db->query($sql)->result();
        return $res;
    }


    // get list name of file
    public function get_upt_name($id)
    {
        $res = $this->db->query("SELECT mu.upt_nama from ms_upt mu WHERE mu.upt_id = $id")->row()->upt_nama;
        return $res;
    }


    // get list name of file
    public function opt_desa($id)
    {
        $where = '';
        if (gettype($id) == 'array') {
            $where = " AND kec.reg_parent in (" . implode(",", $id) . ")";
        } else {
            $where = "and kab.reg_code = '$id'";
        }
        $res = $this->db->query(
            "SELECT
                desa.reg_code ,
                desa.reg_name
            from
                ms_region kab
            inner join ms_region kec on
                kec.reg_parent = kab.reg_code
            inner join ms_region desa on
                desa.reg_parent = kec.reg_code
            where
                desa.reg_active = 1
                $where"
        )->result();
        return $res;
    }


    // get list name of file
    public function get_kab_by_upt($id)
    {
        $res = $this->db->query(
            "SELECT
                du.reg_code ,
                mr.reg_name
            from
                daerah_upt du
            inner join ms_region mr on
                mr.reg_code = du.reg_code
            where
                du.upt_id = $id
                AND mr.reg_active = 1
            ORDER BY
                mr.reg_name"
        )->result();
        return $res;
    }


    // Tadin(3 Juli 2021) : Fungsi mengambil data penghantar gardu induk
    // get penghantar
    public function get_penghantar_by_upt($id = null)
    {

        if ($id != '' && $id != 0 && !empty($id)) {
            $where = "'$id'";
        } else {
            $where = "''";
        }
        $sql =
            "SELECT
                *
            from
                penghantar_gi pg
            where
                penghantar_status = 1
                and pg.kab_kode in (
                select
                    du.reg_code
                FROM
                    daerah_upt du
                where
                    du.upt_id = $where)
            order by
                penghantar_nama";

        $res = $this->db->query($sql)->result();
        return $res;
    }


    // Tadin(3 Juli 2021) : Fungsi mengambil data gardu induk
    // get GI
    public function get_gi_by_upt($id = null)
    {

        if ($id != '' && $id != 0 && !empty($id)) {
            $where = "'$id'";
        } else {
            $where = "''";
        }
        $sql =
            "SELECT
                *
            from
                ms_garduinduk mg
            where
                gi_status = 1
                and mg.kab_kode in (
                select
                    du.reg_code
                FROM
                    daerah_upt du
                where
                    du.upt_id = $where)
            order by
                gi_nama";

        $res = $this->db->query($sql)->result();
        return $res;
    }
}
