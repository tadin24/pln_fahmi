<?php
class M_pentanahan_sby extends CI_Model
{

    //View Krisis Export
    public function view()
    {
        return $this->db->get('pentanahan_sby')->result();
    }


    // get penghantar
    public function get_penghantar()
    {
        $sql =
            "SELECT
                *
            from
                penghantar_gi
            order by
                penghantar_nama";

        $res = $this->db->query($sql)->result();
        return $res;
    }

     public function get_total($where)
    {
        $sql =
            "SELECT
                count(*) total
            from
                pentanahan_sby p
            where
                0 = 0 $where";

        $res = $this->db->query($sql)->row()->total;
        return $res;
    }

    // get GI
    public function get_gi()
    {
        $sql =
            "SELECT
                *
            from
                ms_garduinduk
            order by
                gi_nama";

        $res = $this->db->query($sql)->result();
        return $res;
    }


    // get detail
    public function data_details($id)
    {

        $res = $this->db
            ->select('k.*')
            ->from('pentanahan_sby k')
            ->where('id_pentanahan', $id)
            ->get()->row();
        return $res;
    }


    // Buat sebuah fungsi untuk melakukan insert lebih dari 1 data
    public function insert_multiple($data)
    {
        $this->db->insert_batch('pentanahan_sby', $data);
    }


    // Fungsi untuk melakukan proses upload file
    public function upload_file($filename)
    {
        $this->load->library('upload'); // Load librari upload

        $config['upload_path'] = './excel/';
        $config['allowed_types'] = 'xlsx';
        $config['max_size']  = '2048';
        $config['overwrite'] = true;
        $config['file_name'] = $filename;

        $this->upload->initialize($config); // Load konfigurasi uploadnya
        if ($this->upload->do_upload('file')) { // Lakukan upload dan Cek jika proses upload berhasil
            // Jika berhasil :
            $return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
            return $return;
        } else {
            // Jika gagal :
            $return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
            return $return;
        }
    }


    // insert
    public function insert($data)
    {
        $this->db->insert('pentanahan_sby', $data);
        if ($this->db->affected_rows() > 0) {
            return $this->db->insert_id();
        } else {
            return false;
        }
    }


    // update
    public function update($data)
    {
        $this->db->where('id_pentanahan', $data['id_pentanahan']);
        $res = $this->db->update('pentanahan_sby', $data);
        if ($res) {
            return $data['id_pentanahan'];
        } else {
            return false;
        }
    }


    // edit
    public function edit($id)
    {
        $res = $this->db->get_where('pentanahan_sby', array('id_pentanahan' => $id))->row();
        return $res;
    }


   // delete
    public function delete($id)
    {
        $res = false;
        $this->db->from('pentanahan_sby');
        $this->db->where('id_pentanahan', $id);
        $this->db->delete('pentanahan_sby');
        if ($this->db->affected_rows() > 0) {
            $res = true;
        }
        return $res;
    }

    // get name file uploaded by form to check existence of file
    public function get_file($key, $id)
    {
        $this->db->select($key);
        $this->db->from('pentanahan_sby');
        $this->db->where('id_pentanahan', $id);
        $res = $this->db->get()->row_array();
        return $res;
    }


    // get data
    public function get_data($columns, $where, $order, $limit)
    {
        $selector = implode(",", $columns);
        $sql =
            "SELECT
                $selector
            from
                pentanahan_sby
            where
                0 = 0 $where
            $order $limit";

        $res = $this->db->query($sql)->result();
        return $res;
    }

    function show_data()
    {
        $total = $this->db->query("SELECT * FROM pentanahan_sby");
        return $total;
    }


    // opt reff
    public function opt_reff($id)
    {
        $this->db->select('reff_id,reff_nama,reff_kode');
        $this->db->from('ms_reff');
        $this->db->where('reffcat_id', $id);
        $this->db->where('reff_status', '1');
        $this->db->order_by('reff_kode', 'asc');
        $res = $this->db->get()->result();
        return $res;
    }


    // get list name of file
    public function get_list_file($id)
    {
        $list_id = implode(',', $id);
        $res = $this->db->query("SELECT * from pentanahan_sby where id_pentanahan in ($list_id)")->result_array();
        return $res;
    }
}
