<?php
class M_ultg extends CI_Model
{

    // insert
    public function insert($data)
    {
        $this->db->insert('ms_ultg', $data);
        if ($this->db->affected_rows() > 0) {
            return $this->db->insert_id();
        } else {
            return false;
        }
    }


    // update
    public function update($data)
    {
        $this->db->where('ultg_id', $data['ultg_id']);
        $res = $this->db->update('ms_ultg', $data);
        if ($res) {
            return $data['ultg_id'];
        } else {
            return false;
        }
    }


    // edit
    public function edit($id)
    {
        $res = $this->db->get_where('ms_ultg', array('ultg_id' => $id))->row();
        return $res;
    }


    // delete
    public function delete($id)
    {
        $res = false;
        $this->db->where('ultg_id', $id);
        $this->db->delete('ms_ultg');
        if ($this->db->affected_rows() > 0) {
            $res = true;
        }
        return $res;
    }


    // get total data
    public function get_total($where)
    {
        $sql =
            "SELECT
                count(*) total
            from
                ms_ultg k
            where
                0 = 0 $where";

        $res = $this->db->query($sql)->row()->total;
        return $res;
    }


    // get data
    public function get_data($columns, $where, $order, $limit)
    {
        $selector = implode(",", $columns);
        $sql =
            "SELECT
                $selector
            from
                ms_ultg k
            where
                0 = 0 $where
            $order $limit";

        $res = $this->db->query($sql)->result();
        return $res;
    }
}
