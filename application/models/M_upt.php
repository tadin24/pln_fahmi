<?php
class M_upt extends CI_Model
{

    // insert
    public function insert($data)
    {
        $this->db->insert('ms_upt', $data);
        if ($this->db->affected_rows() > 0) {
            return $this->db->insert_id();
        } else {
            return false;
        }
    }


    // update
    public function update($data)
    {
        $this->db->where('upt_id', $data['upt_id']);
        $res = $this->db->update('ms_upt', $data);
        if ($res) {
            return $data['upt_id'];
        } else {
            return false;
        }
    }


    // edit
    public function edit($id)
    {
        $res = $this->db->get_where('ms_upt', array('upt_id' => $id))->row();
        return $res;
    }


    // delete
    public function delete($id)
    {
        $res = false;
        $this->db->where('upt_id', $id);
        $this->db->delete('ms_upt');
        if ($this->db->affected_rows() > 0) {
            $res = true;
        }
        return $res;
    }

    public function get_total($where)
    {
        $sql =
            "SELECT
                count(*) total
            from
                ms_upt k
            where
                0 = 0 $where";

        $res = $this->db->query($sql)->row()->total;
        return $res;
    }

    // get data
    public function get_data($columns, $where, $order, $limit)
    {
        $selector = implode(",", $columns);
        $sql =
            "SELECT
                $selector
            from
                ms_upt k
            where
                0 = 0 $where
            $order $limit";

        $res = $this->db->query($sql)->result();
        return $res;
    }


    // get daerah
    public function get_daerah($req)
    {
        $sql =
            "SELECT
                reg_code as id,
                reg_name as text
            from
                ms_region mr
            where
                reg_level = 2
                and reg_active = 1
                and (reg_code like '%$req%'
                    or lower(reg_name) LIKE lower('%$req%'))
            order by
                reg_name";

        $res = $this->db->query($sql)->result();
        return $res;
    }


    // insert
    public function insert_kab($data, $upt_id)
    {
        $this->db->where('upt_id', $upt_id);
        $this->db->delete('daerah_upt');
        $this->db->insert_batch('daerah_upt', $data);
        return 1;
    }


    // get daerah upt
    public function get_daerah_upt($upt_id)
    {
        $sql =
            "SELECT
                mr.reg_code ,
                mr.reg_name 
            from
                daerah_upt du
            inner join ms_region mr on
                mr.reg_code = du.reg_code
            where
                du.upt_id = $upt_id
            ORDER BY
                mr.reg_name";

        $res = $this->db->query($sql)->result();
        return $res;
    }
}
