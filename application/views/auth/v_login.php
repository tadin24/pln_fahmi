<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?= $judul ?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <!-- <link rel="stylesheet" href="<?= base_url('template/admin/') ?>/bower_components/bootstrap/dist/css/bootstrap.min.css"> -->
  <link rel="stylesheet" href="<?= base_url('') ?>node_modules/startbootstrap-sb-admin-2/css/sb-admin-2.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?= base_url('template/admin/') ?>/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?= base_url('template/admin/') ?>/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url('template/admin/') ?>/dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?= base_url('template/admin/') ?>/plugins/iCheck/square/blue.css">
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>


  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <style>
    body {
      background-image: url('assets/img/tower.jpeg');
      background-size: cover
    }

    #navTop {
      background-color: #4682B4;
    }

    #btnLogin {
      background-color: rgba(36, 74, 128, .7);
      color: #fff;
    }

    #btnLogin:hover {
      background-color: rgba(36, 74, 128, 1);
    }

    @media screen and (max-width:991px) {
      .navbar-collapse.collapse {
        display: flex !important;
        flex-basis: auto !important;
      }
    }

    @media screen and (max-width:669px) {
      .navbar-collapse.collapse {
        display: block !important;
      }
    }
  </style>
</head>

<body>
  <?= $this->session->flashdata('pesan'); ?>
  <div>
    <nav class="navbar navbar-expand-lg navbar-light" id="navTop">
      <a class="navbar-brand" href="#">
        <img src="template/logo2.png" height="45">
      </a>

      <div class="navbar-collapse collapse" aria-expanded="true" style="height: 0px;">
        <form class="form-inline my-2 my-lg-0 ml-auto" action="" method="post">
          <input class="form-control mr-sm-2" type="text" placeholder="Username" name="username" id="username">
          <input class="form-control mr-sm-2" type="password" placeholder="Password" name="password" id="password">
          <button class="btn btn-outline-none my-2 my-sm-0" id="btnLogin" name="login" type="submit">Login</button>
        </form>
      </div>
    </nav>
  </div>

  <!-- jQuery 3 -->
  <script src="<?= base_url('template/admin/') ?>/bower_components/jquery/dist/jquery.min.js"></script>
  <!-- Bootstrap 3.3.7 -->
  <script src="<?= base_url('template/admin/') ?>/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
</body>

</html>