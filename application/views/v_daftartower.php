<div class="row" id="formContainer" style="display: none;">
  <div class="col-lg-12">
    <div class="card shadow mb-4">
      <div class="card-header py-2 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 font-weight-bold text-primary">Form Master Koordinat MAP</h6>
        <button class="btn btn-primary btn-icon-split" id="btnListData">
          <span class="icon text-white-50">
            <i class="fas fa-database"></i>
          </span>
          <span class="text">List Data</span>
        </button>
      </div>
      <div class="card-body">
        <form action="" method="POST" enctype="multipart/form-data" id="form_vendor">
          <input type="hidden" name="act" id="act" value="add">
          <input type="hidden" name="id_krisis" id="id_krisis" value="">
          <div class="form-group row">
            <label class="control-label col-md-3">Tanggal</label>
            <div class="col-md-3">
              <input type="text" name="tgl" id="tgl" class="form-control tanggal" value="<?= date('d-m-Y') ?>">
            </div>
          </div>
          <div class="form-group row">
            <label class="control-label col-md-3">Update</label>
            <div class="col-md-3">
              <input type="text" name="update" id="update" class="form-control tanggal" value="<?= date('d-m-Y') ?>">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">No Folder</label>
            <div class="col-md-6">
              <input type="text" name="no_folder" id="no_folder" class="form-control">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">No Regional</label>
            <div class="col-md-6">
              <input type="text" name="no_reg" id="no_reg" class="form-control">
            </div>
          </div>
          <div class="form-group row">
            <label class="control-label col-md-3">UPT</label>
            <div class="col-md-6">
              <select name="upt" id="upt" class="form-control">
                <?php foreach ($upt as $value) : ?>
                  <option value="<?= $value->upt_id ?>"><?= $value->upt_nama ?></option>
                <?php endforeach; ?>
              </select>
            </div>
          </div>
          <div class="form-group row">
            <label class="control-label col-md-3">ULTG</label>
            <div class="col-md-6">
              <select name="ultg" id="ultg" class="form-control">
                <?php foreach ($ultg as $value) : ?>
                  <option value="<?= $value->ultg_id ?>"><?= $value->ultg_nama ?></option>
                <?php endforeach; ?>
              </select>
            </div>
          </div>
          <div class="form-group row">
            <label class="control-label col-md-3">Penghantar</label>
            <div class="col-md-6">
              <select name="penghantar_id" id="penghantar_id" class="form-control">
                <?php foreach ($penghantar as $value) : ?>
                  <option value="<?= $value->penghantar_id ?>"><?= $value->penghantar_nama ?></option>
                <?php endforeach; ?>
              </select>
            </div>
          </div>
          <div class="form-group row">
            <label class="control-label col-md-3">KV</label>
            <div class="col-md-6">
              <select name="kv" id="kv" class="form-control">
                <option value="70">70</option>
                <option value="150">150</option>
                <option value="500">500</option>
              </select>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Tipe Tower</label>
            <div class="col-md-6">
              <input type="text" name="jenis" id="jenis" class="form-control">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">No Tower</label>
            <div class="col-md-6">
              <input type="text" name="tower" id="tower" class="form-control">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Koordinat X</label>
            <div class="col-md-6">
              <input type="text" name="lat" id="lat" class="form-control">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Koordinat Y</label>
            <div class="col-md-6">
              <input type="text" name="lang" id="lang" class="form-control">
            </div>
          </div>
          <div class="card mb-3">
            <div class="card-header">
              KELENGKAPAN DOKUMEN
            </div>
            <div class="card-body">
              <div class="form-group row">
                <label class="col-md-3 control-label">KKP</label>
                <div class="col-md-3">
                  <select class="form-control input-doc" name="kkp" id="kkp" data-id-file="view_kkp">
                    <option value="NOK">NOK</option>
                    <option value="OK">OK</option>
                    <option value="-">-</option>
                  </select>
                </div>
                <div class="col-md-6" id="view_kkp" style="display: none;">
                  <div class="custom-file">
                    <input type="file" class="custom-file-input" id="kkp_file" name="kkp_file" accept="application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/pdf">
                    <label class="custom-file-label" for="kkp_file">Pilih file</label>
                  </div>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-md-3 control-label">Assesmen Lingkungan</label>
                <div class="col-md-3">
                  <select class="form-control input-doc" name="kelling" id="kelling" data-id-file="view_kelling">
                    <option value="NIHIL">NIHIL</option>
                    <option value="NOK">NOK</option>
                    <option value="OK">OK</option>
                    <option value="-">-</option>
                  </select>
                </div>
                <div class="col-md-6" id="view_kelling" style="display:none">
                  <div class="custom-file">
                    <input type="file" class="custom-file-input" id="kelling_file" name="kelling_file" accept="application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/pdf">
                    <label class="custom-file-label" for="kelling_file">Pilih file</label>
                  </div>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-md-3 control-label">Assesmen Pondasi</label>
                <div class="col-md-3">
                  <select class="form-control input-doc" name="kelpo" id="kelpo" data-id-file="view_kelpo">
                    <option value="NIHIL">NIHIL</option>
                    <option value="NOK">NOK</option>
                    <option value="OK">OK</option>
                    <option value="-">-</option>
                  </select>
                </div>
                <div class="col-md-6" id="view_kelpo" style="display:none">
                  <div class="custom-file">
                    <input type="file" class="custom-file-input" id="kelpo_file" name="kelpo_file" accept="application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/pdf">
                    <label class="custom-file-label" for="kelpo_file">Pilih file</label>
                  </div>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-md-3 control-label">Kelengkapan Foto</label>
                <div class="col-md-3">
                  <select class="form-control input-doc" name="kelfo" id="kelfo" data-id-file="view_kelfo">
                    <option value="NOK">NOK</option>
                    <option value="OK">OK</option>
                    <option value="-">-</option>
                  </select>
                </div>
                <div class="col-md-6" id="view_kelfo" style="display:none">
                  <div class="custom-file">
                    <input type="file" class="custom-file-input" id="kelfo_file" name="kelfo_file" accept="application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/pdf">
                    <label class="custom-file-label" for="kelfo_file">Pilih file</label>
                  </div>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-md-3 control-label">Assesmen RAB</label>
                <div class="col-md-3">
                  <select class="form-control input-doc" name="kelrab" id="kelrab" data-id-file="view_kelrab">
                    <option value="NIHIL">NIHIL</option>
                    <option value="NOK">NOK</option>
                    <option value="OK">OK</option>
                    <option value="-">-</option>
                  </select>
                </div>
                <div class="col-md-6" id="view_kelrab" style="display:none">
                  <div class="custom-file">
                    <input type="file" class="custom-file-input" id="kelrab_file" name="kelrab_file" accept="application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/pdf">
                    <label class="custom-file-label" for="kelrab_file">Pilih file</label>
                  </div>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-md-3 control-label">Assesmen Desain</label>
                <div class="col-md-3">
                  <select class="form-control input-doc" name="kelde" id="kelde" data-id-file="view_kelde">
                    <option value="NIHIL">NIHIL</option>
                    <option value="NOK">NOK</option>
                    <option value="OK">OK</option>
                    <option value="-">-</option>
                  </select>
                </div>
                <div class="col-md-6" id="view_kelde" style="display:none">
                  <div class="custom-file">
                    <input type="file" class="custom-file-input" id="kelde_file" name="kelde_file" accept="application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/pdf">
                    <label class="custom-file-label" for="kelde_file">Pilih file</label>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="card mb-3">
            <div class="card-header">
              SKOR
            </div>
            <div class="card-body">
              <div class="form-group row">
                <label class="col-md-3 control-label">Assesmen Lingkungan</label>
                <div class="col-md-3">
                  <input type="text" class="form-control" name="skoli" id="skoli">
                </div>
              </div>
              <div class="form-group row">
                <label class="col-md-3 control-label">Assesmen Pondasi</label>
                <div class="col-md-3">
                  <input type="text" name="skopo" id="skopo" class="form-control" value="">
                </div>
              </div>
              <div class="form-group row">
                <label class="col-md-3 control-label">Sifat Hujan</label>
                <div class="col-md-3">
                  <input type="text" name="skohu" id="skohu" class="form-control" value="">
                </div>
              </div>
            </div>
          </div>
          <div class="card mb-3">
            <div class="card-header">
              KLASIFIKASI
            </div>
            <div class="card-body">
              <div class="form-group row">
                <label class="col-md-3 control-label">Assesmen Lingkungan</label>
                <div class="col-md-3">
                  <select class="form-control" name="klali" id="klali">
                    <option value="1">AMAN</option>
                    <option value="2">WASPADA</option>
                    <option value="3">KRITIS</option>
                    <option value="0">-</option>
                  </select>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-md-3 control-label">Assesmen Pondasi</label>
                <div class="col-md-3">
                  <select class="form-control" name="klapo" id="klapo">
                    <option value="1">AMAN</option>
                    <option value="2">WASPADA</option>
                    <option value="3">KRITIS</option>
                    <option value="0">-</option>
                  </select>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-md-3 control-label">Sifat Hujan</label>
                <div class="col-md-3">
                  <select class="form-control" name="klahu" id="klahu">
                    <option value="1">ATAS NORMAL</option>
                    <option value="2">NORMAL</option>
                    <option value="3">BAWAH NORMAL</option>
                    <option value="0">-</option>
                  </select>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-md-3 control-label">Kategori</label>
                <div class="col-md-3">
                  <select class="form-control" name="kategori" id="kategori">
                    <option value="1">AMAN</option>
                    <option value="2">WASPADA</option>
                    <option value="3">KRITIS</option>
                    <option value="0">-</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Anomali</label>
            <div class="col-md-6">
              <input type="text" name="anomali" id="anomali" class="form-control" value="">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Keterangan</label>
            <div class="col-md-6">
              <input type="text" name="keterangan" id="keterangan" class="form-control" value="">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Tautan Folder LINK</label>
            <div class="col-md-6">
              <input type="text" name="tautan" id="tautan" class="form-control" value="">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Penanganan</label>
            <div class="col-md-6">
              <input type="text" name="penanganan" id="penanganan" class="form-control" value="">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Risiko</label>
            <div class="col-md-6">
              <input type="text" name="risiko" id="risiko" class="form-control" value="">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Mitigasi Risiko</label>
            <div class="col-md-6">
              <input type="text" name="mitigasi" id="mitigasi" class="form-control" value="">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Gardu Induk</label>
            <div class="col-md-6">
              <input type="text" name="gi" id="gi" class="form-control" value="">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Bay</label>
            <div class="col-md-6">
              <input type="text" name="bay" id="bay" class="form-control" value="">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">PHT</label>
            <div class="col-md-6">
              <input type="text" name="pht" id="pht" class="form-control" value="">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Status Alat</label>
            <div class="col-md-6">
              <input type="text" name="status_alat" id="status_alat" class="form-control" value="">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Kode Sirkit</label>
            <div class="col-md-6">
              <input type="number" name="kode_sirkit" id="kode_sirkit" class="form-control" value="">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Kode PST Old</label>
            <div class="col-md-6">
              <input type="text" name="kode_pst_old" id="kode_pst_old" class="form-control" value="">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Kode Status</label>
            <div class="col-md-6">
              <input type="number" name="kd_status" id="kd_status" class="form-control" value="">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Buatan</label>
            <div class="col-md-6">
              <input type="text" name="buatan" id="buatan" class="form-control" value="">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Tahun Buat</label>
            <div class="col-md-6">
              <input type="number" name="tahun_buat" id="tahun_buat" class="form-control" value="">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Tanggal Operasi</label>
            <div class="col-md-6">
              <input type="date" name="tgl_operasi" id="tgl_operasi" class="form-control" value="">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Luas TPK</label>
            <div class="col-md-6">
              <input type="number" name="luas_tpk" id="luas_tpk" class="form-control" value="">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Posisi Tower</label>
            <div class="col-md-6">
              <input type="text" name="posisi_tower" id="posisi_tower" class="form-control" value="">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">RT/RW</label>
            <div class="col-md-6">
              <input type="text" name="rtrw" id="rtrw" class="form-control" value="">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Kelurahan</label>
            <div class="col-md-6">
              <input type="text" name="kelurahan" id="kelurahan" class="form-control" value="">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Kecamatan</label>
            <div class="col-md-6">
              <input type="text" name="kecamatan" id="kecamatan" class="form-control" value="">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Kabupaten</label>
            <div class="col-md-6">
              <input type="text" name="kabupaten" id="kabupaten" class="form-control" value="">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Propinsi</label>
            <div class="col-md-6">
              <input type="text" name="propinsi" id="propinsi" class="form-control" value="">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Keterangan Tower</label>
            <div class="col-md-6">
              <input type="text" name="keterangan_tower" id="keterangan_tower" class="form-control" value="">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Merk</label>
            <div class="col-md-6">
              <input type="text" name="merk" id="merk" class="form-control" value="">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Tipe Tower</label>
            <div class="col-md-6">
              <input type="text" name="tipe_tower" id="tipe_tower" class="form-control" value="">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Jenis Material</label>
            <div class="col-md-6">
              <input type="text" name="jenis_material" id="jenis_material" class="form-control" value="">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Jenis Pelapis</label>
            <div class="col-md-6">
              <input type="text" name="jenis_pelapis" id="jenis_pelapis" class="form-control" value="">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Jarak Antar Kaki</label>
            <div class="col-md-6">
              <input type="number" name="jarak_kaki" id="jarak_kaki" class="form-control" value="">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Sudut Stub</label>
            <div class="col-md-6">
              <input type="number" name="sudut_stub" id="sudut_stub" class="form-control" value="">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Tinggi Body</label>
            <div class="col-md-6">
              <input type="number" name="tinggi_body" id="tinggi_body" class="form-control" value="">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Panjang Crossarm</label>
            <div class="col-md-6">
              <input type="number" name="pjg_crossarm" id="pjg_crossarm" class="form-control" value="">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Jarak Fasa</label>
            <div class="col-md-6">
              <input type="number" name="jarak_fasa" id="jarak_fasa" class="form-control" value="">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Jumlah Kawat</label>
            <div class="col-md-6">
              <input type="number" name="jml_kawat" id="jml_kawat" class="form-control" value="">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Berat Total</label>
            <div class="col-md-6">
              <input type="number" name="berat_total" id="berat_total" class="form-control" value="">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Isolator</label>
            <div class="col-md-6">
              <input type="number" name="isolator" id="isolator" class="form-control" value="">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Kode Tegangan</label>
            <div class="col-md-6">
              <input type="number" name="kode_teg" id="kode_teg" class="form-control" value="">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Nomor Aset</label>
            <div class="col-md-6">
              <input type="number" name="nomor_aset" id="nomor_aset" class="form-control" value="">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Cons Type</label>
            <div class="col-md-6">
              <input type="number" name="cons_type" id="cons_type" class="form-control" value="">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Tegangangan Oprasional</label>
            <div class="col-md-6">
              <input type="number" name="teg_oprs" id="teg_oprs" class="form-control" value="">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Techidentno</label>
            <div class="col-md-6">
              <input type="text" name="techidentno" id="techidentno" class="form-control" value="">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Asset</label>
            <div class="col-md-6">
              <input type="text" name="asset" id="asset" class="form-control" value="">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">EQ Number</label>
            <div class="col-md-6">
              <input type="text" name="eq_number" id="eq_number" class="form-control" value="">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Kode PST</label>
            <div class="col-md-6">
              <input type="number" name="kode_pst" id="kode_pst" class="form-control" value="">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Serial ID</label>
            <div class="col-md-6">
              <input type="number" name="serial_id" id="serial_id" class="form-control" value="">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Konstruksi</label>
            <div class="col-md-6">
              <input type="text" name="konstruksi" id="konstruksi" class="form-control" value="">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Equipment Number</label>
            <div class="col-md-6">
              <input type="number" name="equipment_number" id="equipment_number" class="form-control" value="">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">ID Functloc</label>
            <div class="col-md-6">
              <input type="text" name="id_functloc" id="id_functloc" class="form-control" value="">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">ID Bay</label>
            <div class="col-md-6">
              <input type="text" name="id_bay" id="id_bay" class="form-control" value="">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Wil Kerja</label>
            <div class="col-md-6">
              <input type="number" name="wil_kerja" id="wil_kerja" class="form-control" value="">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Tipe Isolator</label>
            <div class="col-md-6">
              <input type="text" name="tipe_isolator" id="tipe_isolator" class="form-control" value="">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Tipe</label>
            <div class="col-md-6">
              <input type="text" name="tipe" id="tipe" class="form-control" value="">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Phasa</label>
            <div class="col-md-6">
              <input type="text" name="phasa" id="phasa" class="form-control" value="">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Penempatan</label>
            <div class="col-md-6">
              <input type="text" name="penempatan" id="penempatan" class="form-control" value="">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Foto1</label>
            <div class="col-md-6">
              <div class="custom-file">
                <input type="file" class="custom-file-input" id="foto1" name="foto1" accept="image/*">
                <label class="custom-file-label" for="foto1">Pilih file</label>
              </div>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Foto2</label>
            <div class="col-md-6">
              <div class="custom-file">
                <input type="file" class="custom-file-input" id="foto2" name="foto2" accept="image/*">
                <label class="custom-file-label" for="foto2">Pilih file</label>
              </div>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Foto3</label>
            <div class="col-md-6">
              <div class="custom-file">
                <input type="file" class="custom-file-input" id="foto3" name="foto3" accept="image/*">
                <label class="custom-file-label" for="foto3">Pilih file</label>
              </div>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Foto4</label>
            <div class="col-md-6">
              <div class="custom-file">
                <input type="file" class="custom-file-input" id="foto4" name="foto4" accept="image/*">
                <label class="custom-file-label" for="foto4">Pilih file</label>
              </div>
            </div>
          </div>
        </form>
        <div class="box-footer">
          <div class="row">
            <div class="col-md-12 text-center">
              <button type="button" class="btn btn-primary" id="saveData">Save</button>
              <button type="button" class="btn btn-default" id="cancelData">Cancel</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row" id="tableContainer">
  <div class="col-lg-12">
    <div class="card shadow mb-4">
      <div class="card-header py-2 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 font-weight-bold text-primary">Master Group Koordinat MAP</h6>

        <!-- <a href="<?= base_url('Krisis/krisis_tambah/') ?>" class="btn btn-primary btn-icon-split" id="btnFormData">
          <span class="icon text-white-50">
            <i class="fas fa-plus"></i>
          </span>
          <span class="text">Tambah</span>
        </a>
        
        <a href="<?= base_url('Krisis/form/') ?>" class="btn btn-primary btn-icon-split" id="btnFormData">
          <span class="icon text-white-50">
            <i class="fa fa-plus"></i>
          </span>
          <span class="text">Tambah</span>
        </a> -->
        <button class="btn btn-primary btn-icon-split" id="btnFormData">
          <span class="icon text-white-50">
            <i class="fas fa-plus"></i>
          </span>
              <span class="text">Tambah</span>
        </button>
      </div>

      <div class="card-body">
      
         
        <div class="table-responsive">
          <table class="table table-bordered" id="refrensi-table" width="100%" cellspacing="0">
            <thead>
              <tr>

                <th class="text-center">No</th>
                <th class="text-center">Penghantar</th>
                <th class="text-center">UPT</th>
                <th class="text-center">ULTG</th>
                <th class="text-center">Latitude</th>
                <th class="text-center">Longitude</th>
                <th class="text-center">Lingkungan</th>
                <th class="text-center">Pondasi</th>
                
                <th class="text-center">Aksi</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  var tableAdvancedInit = function() {

    var initTable1 = function() {
      var target = '#refrensi-table';
      var oTable = $(target).dataTable({
        "displayStart": 0,
        "pageLength": 10,
        "lengthMenu": [
          [10, 25, 50, 100, -1],
          [10, 25, 50, 100, "All"]
        ],
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        // Load data for the table's content from an Ajax source
        "ajax": {
          "url": "<?php echo base_url('pagination/get_data') ?>",
          "dataType": "json",
          "type": "POST",
          // "data": function(d) {
          //   d.f_mmsi = $('#f_mmsi').val();
          // },
        },
        //Set column definition initialisation properties.
        columnDefs: [{
            "targets": [0, -1, -2, -3, -4], //last column
            "orderable": false, //set not orderable
          },
          {
            "targets": [0],
            "width": "5%",
            "className": "text-center"
          },
          {
            "targets": [1],
            "width": "15%"
          },
          {
            "targets": [2],
            "width": "15%"
          },
          {
            "targets": [3],
            "width": "18%"
          },
          {
            "targets": [4],
            "width": "10%",
            "className": "text-center"
          },
          {
            "targets": [5],
            "width": "10%",
            "className": "text-center"
          },
          {
            "targets": [6],
            "width": "10%"
          },
          {
            "targets": [7],
            "width": "10%",
            "className": "text-center"
          },
          {
            "targets": [8],
            "width": "10%"
          },
        ],
        "order": [
          [1, "asc"]
        ],

        "language": {
          // language settings
          "lengthMenu": "Display _MENU_ records",
          "search": "Search _INPUT_ <a class='btn default bts' href='javascript:void(0);'><i class='fa fa-search'></i></a>",
          "processing": '<img src="assets/plugins/global/images/owl.carousel/ajax-loader.gif"/><span>&nbsp;&nbsp;Loading...</span>',
          "infoEmpty": "No records found to show",
          "ajaxRequestGeneralError": "Could not complete request. Please check your internet connection",
          "emptyTable": "No data available in table",
          "zeroRecords": "No matching records found",
          "paginate": {
            "previous": "Prev",
            "next": "Next",
            "page": "Page",
            "pageOf": "of"
          }
        },
        "autoWidth": true, // disable fixed width and enable fluid table
        "orderCellsTop": true, // make sortable only the first row in thead
        "pagingType": "full_numbers", // pagination type(bootstrap, bootstrap_full_number or bootstrap_extended)
      });

      jQuery(target + '_wrapper .dataTables_filter input').addClass("form-control input-small input-inline"); // modify table search input
      jQuery(target + '_wrapper .dataTables_length select').addClass("form-control input-small"); // modify table per page dropdown
      jQuery(target + '_wrapper .dataTables_length select').select2(); // initialize select2 dropdown
      jQuery(target + '_wrapper .dataTables_filter input').unbind();
      jQuery(target + '_wrapper .dataTables_filter input').bind('keyup', function(e) {
        if (e.keyCode == 13) {
          oTable.fnFilter(this.value);
        }
      });
      jQuery(target + '_wrapper .dataTables_filter a').bind('click', function(e) {
        var key = jQuery(target + '_wrapper .dataTables_filter input').val();
        oTable.fnFilter(key);
      });
    }

    return {
      // public functions
      init: function() {
        if (!jQuery().dataTable) {
          return;
        }
        initTable1();
      }
    };
  }();
  $(document).ready(function() {
    tableAdvancedInit.init()


    $('#btnListData,#cancelData').click(function() {
      resetForm()
      $('#tableContainer').slideDown(500)
      $('#formContainer').slideUp(500)
    })


    $('#btnFormData').click(function() {
      resetForm()
      $('.input-doc').trigger('change')
      $('#tableContainer').slideUp(500)
      $('#formContainer').slideDown(500)
    })


    // #saveData on click
    $('#saveData').click(e => {
      $('#form_vendor').submit()
    })


    // validasi form_vendor
    $("#form_vendor").validate({
      // define validation rules
      rules: {
        foto1: {
          filesize: 1000000,
          extension: "png|jpg|jpeg|gif"
        },
        foto2: {
          filesize: 1000000,
          extension: "png|jpg|jpeg|gif"
        },
        foto3: {
          filesize: 1000000,
          extension: "png|jpg|jpeg|gif"
        },
        foto4: {
          filesize: 1000000,
          extension: "png|jpg|jpeg|gif"
        },
      },
      messages: {
        foto1: {
          filesize: "Ukuran File Tidak boleh melebihi 1MB"
        },
        foto2: {
          filesize: "Ukuran File Tidak boleh melebihi 1MB"
        },
        foto3: {
          filesize: "Ukuran File Tidak boleh melebihi 1MB"
        },
        foto4: {
          filesize: "Ukuran File Tidak boleh melebihi 1MB"
        },
      },
      submitHandler: function(form) {
        $('#saveData').attr('disabled', true).text("Loading...");
        const formData = new FormData();
        const params = $('#form_vendor').serializeArray()
        const files = $('#form_vendor').find('.custom-file-input');

        $.each(files, (i, item) => {
          formData.append($(item).attr('name'), $(item)[0].files[0])
        })
        $.each(params, (i, item) => {
          formData.append(item.name, item.value)
        })
        formData.append('penghantar', $('#penghantar_id option:selected').text())
        $.ajax({
          url: '<?php echo base_url('krisis/save') ?>',
          type: "POST",
          data: formData,
          dataType: "json",
          cache: false,
          mimeType: "multipart/form-data",
          success: function(response, status, xhr, $form) {
            if (response > 0) {
              $('#saveData').attr('disabled', false).text("Save");
              $("#cancelData").click();
              Swal.fire({
                icon: 'success',
                title: 'Success!',
                text: 'Data Berhasil Disimpan!',
                showConfirmButton: false,
                timer: 1500
              })
              loadTbl();
            }
          },
          contentType: false,
          processData: false
        }, 'json');
      }
    });
  });


  // FUNGSI edit menggunakan ARROW FUNCTION JS
  const set_val = id => {
    resetForm();
    $('.input-doc').trigger('change')
    $('#act').val("edit");
    $.ajax({
      url: '<?= base_url() ?>krisis/edit/' + id,
      dataType: 'json',
      cache: false,
      success: res => {
        console.log(res);
        $('#id_krisis').val(res.id_krisis);
        $('#tgl').val(res.tgl);
        $('#update').val(res.update);
        $('#no_folder').val(res.no_folder);
        $('#no_reg').val(res.no_reg);
        $('#upt_id').val(res.upt_id);
        $('#ultg_id').val(res.ultg_id);
        $('#penghantar_id').val(res.penghantar_id);
        $('#jenis').val(res.jenis);
        $('#tower').val(res.tower);
        $('#lat').val(res.lat);
        $('#lang').val(res.lang);
        $('#kkp').val(res.kkp);
        $('#kkp').trigger('change');
        if (res.kkp == 'OK' && res.kkp_file != '' && res.kkp_file) {
          $('#kkp_file').parent().find('.custom-file-label').text(res.kkp_file);
        }
        $('#kelling').val(res.kelling);
        $('#kelling').trigger('change');
        if (res.kelling == 'OK' && res.kelling_file != '' && res.kelling_file) {
          $('#kelling_file').parent().find('.custom-file-label').text(res.kelling_file);
        }
        $('#kelpo').val(res.kelpo);
        $('#kelpo').trigger('change');
        if (res.kelpo == 'OK' && res.kelpo_file != '' && res.kelpo_file) {
          $('#kelpo_file').parent().find('.custom-file-label').text(res.kelpo_file);
        }
        $('#kelfo').val(res.kelfo);
        $('#kelfo').trigger('change');
        if (res.kelfo == 'OK' && res.kelfo_file != '' && res.kelfo_file) {
          $('#kelfo_file').parent().find('.custom-file-label').text(res.kelfo_file);
        }
        $('#kelrab').val(res.kelrab);
        $('#kelrab').trigger('change');
        if (res.kelrab == 'OK' && res.kelrab_file != '' && res.kelrab_file) {
          $('#kelrab_file').parent().find('.custom-file-label').text(res.kelrab_file);
        }
        $('#kelde').val(res.kelde);
        $('#kelde').trigger('change');
        if (res.kelde == 'OK' && res.kelde_file != '' && res.kelde_file) {
          $('#kelde_file').parent().find('.custom-file-label').text(res.kelde_file);
        }
        $('#skoli').val(res.skoli);
        $('#skopo').val(res.skopo);
        $('#skohu').val(res.skohu);
        $('#klali').val(res.klali);
        $('#klapo').val(res.klapo);
        $('#klahu').val(res.klahu);
        $('#kategori').val(res.kategori);
        $('#anomali').val(res.anomali);
        $('#keterangan').val(res.keterangan);
        $('#tautan').val(res.tautan);
        $('#penanganan').val(res.penanganan);
        $('#risiko').val(res.risiko);
        $('#mitigasi').val(res.mitigasi);
        $('#gi').val(res.gi);
        $('#bay').val(res.bay);
        $('#pht').val(res.pht);
        $('#status_alat').val(res.status_alat);
        $('#kode_sirkit').val(res.kode_sirkit);
        $('#kode_pst_old').val(res.kode_pst_old);
        $('#kd_status').val(res.kd_status);
        $('#buatan').val(res.buatan);
        $('#tahun_buat').val(res.tahun_buat);
        $('#tgl_operasi').val(res.tgl_operasi);
        $('#luas_tpk').val(res.luas_tpk);
        $('#posisi_tower').val(res.posisi_tower);
        $('#rtrw').val(res.rtrw);
        $('#kelurahan').val(res.kelurahan);
        $('#kecamatan').val(res.kecamatan);
        $('#kabupaten').val(res.kabupaten);
        $('#propinsi').val(res.propinsi);
        $('#keterangan_tower').val(res.keterangan_tower);
        $('#merk').val(res.merk);
        $('#tipe_tower').val(res.tipe_tower);
        $('#jenis_material').val(res.jenis_material);
        $('#jenis_pelapis').val(res.jenis_pelapis);
        $('#jarak_kaki').val(res.jarak_kaki);
        $('#sudut_stub').val(res.sudut_stub);
        $('#tinggi_body').val(res.tinggi_body);
        $('#pjg_crossarm').val(res.pjg_crossarm);
        $('#jarak_fasak').val(res.jarak_fasa);
        $('#jml_kawat').val(res.jml_kawat);
        $('#berat_total').val(res.berat_total);
        $('#isolator').val(res.isolator);
        $('#kode_teg').val(res.kode_teg);
        $('#nomor_aset').val(res.nomor_aset);
        $('#cons_type').val(res.cons_type);
        $('#teg_oprs').val(res.teg_oprs);
        $('#techidentno').val(res.techidentno);
        $('#asset').val(res.asset);
        $('#eq_number').val(res.eq_number);
        $('#kode_pst').val(res.kode_pst);
        $('#serial_id').val(res.serial_id);
        $('#konstruksi').val(res.konstruksi);
        $('#equipment_number').val(res.equipment_number);
        $('#id_functloc').val(res.id_functloc);
        $('#id_bay').val(res.id_bay);
        $('#wil_kerja').val(res.wil_kerja);
        $('#tipe_isolator').val(res.tipe_isolator);
        $('#tipe').val(res.tipe);
        $('#phasa').val(res.phasa);
        $('#penempatan').val(res.penempatan);
        if (res.foto1 != '' && res.foto1) {
          $('#foto1').parent().find('.custom-file-label').text(res.foto1);
        }
        if (res.foto2 != '' && res.foto2) {
          $('#foto2').parent().find('.custom-file-label').text(res.foto2);
        }
        if (res.foto3 != '' && res.foto3) {
          $('#foto3').parent().find('.custom-file-label').text(res.foto3);
        }
        if (res.foto4 != '' && res.foto4) {
          $('#foto4').parent().find('.custom-file-label').text(res.foto4);
        }
      }
    })
    $('#tableContainer').slideUp(500)
    $('#formContainer').slideDown(500)
  }



  // FUNGSI delete menggunakan ARROW FUNCTION JS
  const del = id => {
    Swal.fire({
      title: 'Apa Anda yakin menghapus Data ini?',
      text: "Data yang terhapus tidak bisa dikembalikan!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Ya, Hapus!',
      cancelButtonText: 'Tidak, Batal!',
    }).then((result) => {
      if (result.isConfirmed) {
        $.ajax({
          url: '<?= base_url() ?>krisis/delete/' + id,
          cache: false,
          dataType: 'json',
          success: res => {
            if (res) {
              Swal.fire(
                'Deleted!',
                'Data telah dihapus.',
                'success'
              )
              loadTbl()
            }
          }
        })
      }
    })
  }


  // FUNGSI reset form menggunakan ARROW FUNCTION JS
  const resetForm = () => {
    $('#act').val("add");
    $('#form_vendor')[0].reset();
    $('#form_vendor').validate().resetForm();
    $('.custom-file-label').html("Pilih file");
  }


  // FUNGSI loadTbl menggunakan ARROW FUNCTION JS
  const loadTbl = () => {
    $("#refrensi-table").dataTable().fnDraw();
  }

  $(".tanggal").datepicker({
    autoclose: true,
    dateFormat: "dd-mm-yy"
  })

  $('.custom-file-input').on('change', function() {
    //get the file name
    var fileName = $(this).val().replace('C:\\fakepath\\', " ");
    //replace the "Choose a file" label
    $(this).parent().find('.custom-file-label').html(fileName);
    $('#form_vendor').validate().resetForm();
    $('#form_vendor').validate().element($(this));
  })

  $('.input-doc').on('change', function() {
    if ($(this).val() == "OK") {
      $('#' + $(this).data('id-file')).show()
    } else {
      $('#' + $(this).data('id-file')).hide()
    }
  })
</script>