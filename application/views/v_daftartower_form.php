<table class="table table-reposive">
  <form action="" method="POST" enctype="multipart/form-data">
    <tr>
      <th>UPT</th>
      <td><select class="form-control" name="upt" required="">
          <option value="UPT Malang">UPT Malang</option>
          <option value="UPT Bali">UPT Bali</option>
          <option value="UPT Gresik">UPT Gresik</option>
          <option value="UPT Madiun">UPT Madiun</option>
          <option value="UPT Probolinggo">UPT Probolinggo</option>
          <option value="UPT Surabaya">UPT Surabaya</option>
        </select></td>
    </tr>
    <tr>
      <th>Bay</th>
      <td><input type="text" name="bay" class="form-control" value="<?= $bay ?>"></td>
    </tr>
    <tr>
      <th>Penghantar</th>
      <td><select class="form-control" name="penghantar" required="">
          <option value="TOWER SUTT 70KV TARIK-SKTIH #0002">TOWER SUTT 70KV TARIK-SKTIH #0002</option>
          <option value="TOWER SUTT 70KV TARIK-SKTIH #0005">TOWER SUTT 70KV TARIK-SKTIH #0005</option>
          <option value="TOWER SUTT 70KV TARIK-SKTIH #0008">TOWER SUTT 70KV TARIK-SKTIH #0008</option>
          <option value="TOWER SUTT 70KV TARIK-SKTIH #0012">TOWER SUTT 70KV TARIK-SKTIH #0012</option>
          <option value="TOWER SUTT 70KV TARIK-SKTIH #0013">TOWER SUTT 70KV TARIK-SKTIH #0013</option>
          <option value="TOWER SUTT 70kV TRK-MWN+DRJO #0002">TOWER SUTT 70kV TRK-MWN+DRJO #0002</option>
          <option value="TOWER SUTT 70kV TRK-MWN+DRJO #0005">TOWER SUTT 70kV TRK-MWN+DRJO #0005</option>
          <option value="TOWER SUTT 70kV MDL-SLJ-SKL #0025">TOWER SUTT 70kV MDL-SLJ-SKL #0025</option>
          <option value="TOWER SUTT 70kV INC SMN-MDL+SKT #0005A">TOWER SUTT 70kV INC SMN-MDL+SKT #0005A</option>
          <option value="TOWER SUTT 70kV INC SMN-MDL+SKT #0005B">TOWER SUTT 70kV INC SMN-MDL+SKT #0005B</option>
          <option value="TOWER SUTT 70kV MDL+SMN-SKT #0008A">TOWER SUTT 70kV MDL+SMN-SKT #0008A</option>
          <option value="TOWER SUTT 70kV MDL+SMN-SKT #0030">TOWER SUTT 70kV MDL+SMN-SKT #0030</option>
          <option value="TOWER SUTT 70kV MDL+SMN-SKT #0035">TOWER SUTT 70kV MDL+SMN-SKT #0035</option>
          <option value="TOWER SUTT 70kV MDL+SMN-SKT #0038">TOWER SUTT 70kV MDL+SMN-SKT #0038</option>
          <option value="TOWER SUTT 70kV MDL+SMN-SKT #0055">TOWER SUTT 70kV MDL+SMN-SKT #0055</option>
          <option value="TOWER SUTT 70kV MDL+SMN-SKT #0060">TOWER SUTT 70kV MDL+SMN-SKT #0060</option>
          <option value="TOWER SUTT 70kV MDL+SMN-SKT #0067A">TOWER SUTT 70kV MDL+SMN-SKT #0067A</option>
          <option value="TOWER SUTT 70kV MDL+SMN-SKT #0070A">TOWER SUTT 70kV MDL+SMN-SKT #0070A</option>
          <option value="TOWER SUTT 70kV MDL+SMN-SKT #0081">TOWER SUTT 70kV MDL+SMN-SKT #0060</option>
          <option value="TOWER SUTT 70kV MDL+SMN-SKT #0002">TOWER SUTT 70kV MDL+SMN-SKT #0002</option>
          <option value="TOWER SUTT 70kV MDL+SMN-SKT #0005">TOWER SUTT 70kV MDL+SMN-SKT #0005</option>
          <option value="TOWER SUTT 70kV MNYAR-MPISB #0005">TOWER SUTT 70kV MNYAR-MPISB #0005</option>
          <option value="TOWER SUTT 70kV KBA-TRN+SGR+GPN #0056">TOWER SUTT 70kV KBA-TRN+SGR+GPN #0056</option>
          <option value="TOWER SUTET 500kV UNGRN-KRIAN #0286">TOWER SUTET 500kV UNGRN-KRIAN #0286</option>
          <option value="TOWER SUTET 500kV UNGRN-KRIAN #0311">TOWER SUTET 500kV UNGRN-KRIAN #0311</option>
          <option value="TOWER SUTET 500kV UNGRN-KRIAN #0317">TOWER SUTET 500kV UNGRN-KRIAN #0317</option>
          <option value="TOWER SUTET 500kV UNGRN-KRIAN #0376">TOWER SUTET 500kV UNGRN-KRIAN #0376</option>
          <option value="TOWER SUTET 500kV PAITN-KDIRI #0027">TOWER SUTET 500kV PAITN-KDIRI #0027</option>
          <option value="TOWER SUTET 500kV PAITN-KDIRI #0090">TOWER SUTET 500kV PAITN-KDIRI #0090</option>
          <option value="TOWER SUTET 500kV PAITN-GRATI #0088">TOWER SUTET 500kV PAITN-GRATI #0088</option>
        </select></td>
    </tr>
    <tr>
      <th>Status Alat</th>
      <td><input type="text" name="status_alat" class="form-control" value="<?= $status_alat ?>"></td>
    </tr>
    <tr>
      <th>Tower</th>
      <td><input type="number" name="tower" class="form-control" value="<?= $tower ?>"></td>
    </tr>
    <tr>
    <th>Buatan</th>
      <td><input type="text" name="buatan" class="form-control" value="<?= $buatan ?>"></td>
    </tr>
    <tr>
    <th>Tahun Buat</th>
      <td><input type="number" name="tahun_buat" class="form-control" value="<?= $tahun_buat ?>"></td>
    </tr>
     <th>Tanggal Operasi</th>
      <td><input type="date" name="tgl_operasi" class="form-control" value="<?= $tgl_operasi ?>"></td>
    </tr>
     <th>Latitude</th>
      <td><input type="text" name="lat" class="form-control" value="<?= $lat ?>"></td>
    </tr>
    <th>Langitude</th>
      <td><input type="text" name="lang" class="form-control" value="<?= $lang ?>"></td>
    </tr>
    <th>Luas TPK</th>
      <td><input type="text" name="luas_tpk" class="form-control" value="<?= $luas_tpk ?>"></td>
    </tr>
    <th>Kelurahan</th>
      <td><input type="text" name="kelurahan" class="form-control" value="<?= $kelurahan ?>"></td>
    </tr>
    <th>Kecamatan</th>
      <td><input type="text" name="kecamatan" class="form-control" value="<?= $kecamatan ?>"></td>
    </tr>
    <th>Keterangan</th>
      <td><input type="text" name="keterangan" class="form-control" value="<?= $keterangan ?>"></td>
    </tr>
    <th>Merk</th>
      <td><input type="text" name="merk" class="form-control" value="<?= $merk ?>"></td>
    </tr>
    <th>Jenis Tower</th>
      <td><input type="text" name="jenis_tower" class="form-control" value="<?= $jenis_tower ?>"></td>
    </tr>
    <th>Tipe Tower</th>
      <td><input type="text" name="tipe_tower" class="form-control" value="<?= $tipe_tower ?>"></td>
    </tr>
    <th>Phasa</th>
      <td><input type="text" name="phasa" class="form-control" value="<?= $phasa ?>"></td>
    </tr>
    <tr>
      <td></td>
      <th><input type="submit" name="kirim" value="Submit" class="btn btn-primary"></th>
    </tr>
  </form>
</table>
<?php
if ($aksi == "edit") :
?>
<?php endif; ?>