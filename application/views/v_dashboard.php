<style type="text/css">
  #mapid {
    height: 400px;
  }
</style>

<div id="layoutSidenav">
  <div id="layoutSidenav_content">
    <main style="padding-bottom:80px">
      <div class="container-fluid">
        <h1 class="mt-2">Pemetaan Tower</h1>
        <ol class="breadcrumb mb-4">
          <li class="breadcrumb-item"><a href="<?= base_url('dashboard'); ?>">Beranda</a></li>
          <li class="breadcrumb-item active">Pemetaan</li>
        </ol>
        <div id="mapid"></div>
      </div>
    </main>

  </div>
</div>

<!-- <div class="row">
  <?php if ($this->session->userdata('group_id') == 1) : ?>
    <div class="col-12">
      <div class="small-box bg-aqua">
        <div class="inner">
          <div class="box">
            <div class="box-header">
              <h3>Jumlah Pegawai</h3>
            </div>
            <div class="box-body">
              <canvas id="dataJumlahUser" style="max-height:380px"></canvas>
            </div>
          </div>
        </div>
        <a href="<?php echo base_url('krisis') ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
  <?php endif; ?>
</div> -->
<div class="card">
  <div class="card-header">
    Data Klasifikasi UPT
  </div>
  <div class="card-body">
    <div class="row mb-2">
      <div class="col-lg-1">
        <h5 class="card-title">UPT</h5>
      </div>
      <div class="col-lg-3">
        <select class="form-control" name="f_upt_id" id="f_upt_id">
          <?php foreach ($upt as $item) : ?>
            <option value="<?= $item->upt_id ?>"><?= $item->upt_nama ?></option>
          <?php endforeach; ?>
        </select>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-6">
        <h5 class="card-title">Lingkungan</h5>
        <canvas id="dataKlaLi"></canvas>
      </div>
      <div class="col-lg-6">
        <h5 class="card-title">Pondasi</h5>
        <canvas id="dataKlaPo"></canvas>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-6">
        <h5 class="card-title">Prioritas</h5>
        <canvas id="dataPrioritas"></canvas>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <h5 class="card-title"></h5>
        <figure class="highcharts-figure">
          <div id="list_upt_lingkungan"></div>
        </figure>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <!-- <h5 class="card-title">Pondasi</h5> -->
        <figure class="highcharts-figure">
          <div id="list_upt_pondasi"></div>
        </figure>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <!-- <h5 class="card-title">Pondasi</h5> -->
        <figure class="highcharts-figure">
          <div id="list_upt_prioritas"></div>
        </figure>
      </div>
    </div>
  </div>
</div>



<script src="https://kit.fontawesome.com/10e53f85be.js" crossorigin="anonymous"></script>
<script src="<?php echo base_url('assets/leaflet/leaflet.js') ?> "></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>

<script>
  var pieKlaLi = document.getElementById('dataKlaLi').getContext('2d');
  var pieChartKL = new Chart(pieKlaLi, {
    type: 'bar',
    data: {
      datasets: [{
        data: [],
        hoverOffset: 4
      }],
      labels: []
    },
    options: {
      responsive: true,
      scales: {
        yAxes: [{
          display: true,
          ticks: {
            beginAtZero: true,
            userCallback: function(label, index, labels) {
              if (Math.floor(label) === label) {
                return label;
              }
            },
          }
        }]
      }
    }
  })

  var pieKlaPo = document.getElementById('dataKlaPo').getContext('2d');
  var pieChartKP = new Chart(pieKlaPo, {
    type: 'bar',
    data: {
      datasets: [{
        data: [],
        hoverOffset: 4
      }],
      labels: []
    },
    options: {
      responsive: true,
      scales: {
        yAxes: [{
          display: true,
          ticks: {
            beginAtZero: true,
            userCallback: function(label, index, labels) {
              if (Math.floor(label) === label) {
                return label;
              }
            },
          }
        }]
      }
    }
  })

  var piePrioritas = document.getElementById('dataPrioritas').getContext('2d');
  var pieChartP = new Chart(piePrioritas, {
    type: 'bar',
    data: {
      datasets: [{
        data: [],
        hoverOffset: 4
      }],
      labels: []
    },
    options: {
      responsive: true,
      scales: {
        yAxes: [{
          display: true,
          ticks: {
            beginAtZero: true,
            userCallback: function(label, index, labels) {
              if (Math.floor(label) === label) {
                return label;
              }
            },
          }
        }]
      }
    }
  })


  // var barUserCanvas = document.getElementById('dataJumlahUser').getContext('2d');
  // var barUser = new Chart(barUserCanvas, {
  //   type: 'bar',
  //   data: {
  //     datasets: [{
  //       data: [],
  //       borderWidth: 1,
  //       label: 'Jumlah User',
  //     }],
  //     labels: []
  //   },
  //   scales: {
  //     y: {
  //       beginAtZero: true
  //     }
  //   }
  // })
  // pieChart.Doughnut(PieData, pieOptions)
  const getDataChart = (pie, url = "") => {
    $.ajax({
      url: url,
      dataType: "json",
      data: {
        upt_id: $("#f_upt_id").val()
      },
      type: 'post',
      cache: false,
      success: function(res) {
        if (res.length > 0) {
          let newObjectData = new Object();
          newObjectData.data = []
          newObjectData.backgroundColor = []
          newObjectData.labels = []
          $.each(res, function(index, item) {
            newObjectData.data.push(parseInt(item.value))
            if (item.label == "Kritis") {
              newObjectData.backgroundColor.push('rgb(231, 74, 59)')
            } else if (item.label == "Waspada") {
              newObjectData.backgroundColor.push('rgb(246, 194, 62)')
            } else if (item.label == "Aman") {
              newObjectData.backgroundColor.push('rgb(28, 200, 138)')
            } else if (item.label == "P0") {
              newObjectData.backgroundColor.push('rgb(231, 74, 59)')
            } else if (item.label == "P1") {
              newObjectData.backgroundColor.push('rgb(246, 194, 62)')
            } else if (item.label == "P2") {
              newObjectData.backgroundColor.push('rgb(28, 200, 138)')
            } else {
              newObjectData.backgroundColor.push('rgb(90, 92, 105)')
            }
            newObjectData.labels.push(item.label)
            if (res.length - 1 == index) {
              pie.data.datasets[0].data = newObjectData.data;
              pie.data.datasets[0].backgroundColor = newObjectData.backgroundColor;
              pie.data.labels = newObjectData.labels;
              pie.update();
            }
          })
        }
      }
    })
  }


  // function getJumlahUser() {
  //   $.ajax({
  //     url: "<?= base_url() ?>dashboard/get_total_user",
  //     cache: false,
  //     dataType: "json",
  //     success: function(res) {
  //       if (res.length > 0) {
  //         let new_data = new Object();
  //         new_data.data = []
  //         new_data.backgroundColor = []
  //         new_data.labels = []
  //         $.each(res, function(index, item) {
  //           new_data.data.push(parseInt(item.value))
  //           new_data.backgroundColor.push('rgb(' + Math.floor(Math.random() * 255) + ',' + Math.floor(Math.random() * 255) + ',' + Math.floor(Math.random() * 255) + ')')
  //           new_data.labels.push(item.label)
  //           if (res.length - 1 == index) {
  //             barUser.data.datasets[0].data = new_data.data;
  //             barUser.data.datasets[0].backgroundColor = new_data.backgroundColor;
  //             barUser.data.labels = new_data.labels;
  //             barUser.update();
  //           }
  //         })
  //       }
  //     }
  //   })
  // }

  var klaliChart = Highcharts.chart('list_upt_lingkungan', {
    chart: {
      type: 'column'
    },
    title: {
      text: 'Data UPT Klasifikasi Lingkungan'
    },
    xAxis: {
      categories: [],
      crosshair: true
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Jumlah kondisi'
      }
    },
    tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
        '<td style="padding:0"><b>{point.y:.0f}</b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true
    },
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0
      }
    },
    series: []
  });


  var klapoChart = Highcharts.chart('list_upt_pondasi', {
    chart: {
      type: 'column'
    },
    title: {
      text: 'Data Klasifikasi Pondasi UPT'
    },
    xAxis: {
      categories: [],
      crosshair: true
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Jumlah Kondisi'
      }
    },
    tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
        '<td style="padding:0"><b>{point.y:.0f}</b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true
    },
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0
      }
    },
    series: []
  });

  var prioritasChart = Highcharts.chart('list_upt_prioritas', {
    chart: {
      type: 'column'
    },
    title: {
      text: 'Data Prioritas UPT'
    },
    xAxis: {
      categories: [],
      crosshair: true
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Jumlah Kondisi'
      }
    },
    tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
        '<td style="padding:0"><b>{point.y:.0f}</b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true
    },
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0
      }
    },
    series: []
  });


  // FUNGSI get data all upt
  function get_data_all_upt(jenis = "") {
    if (jenis != "") {
      $.ajax({
        url: "<?= base_url() ?>dashboard/get_data_all",
        cache: false,
        dataType: "json",
        data: {
          jenis: jenis
        },
        success: function(res) {
          if (jenis == "klali") {
            klaliChart.xAxis[0].setCategories(res.list_upt);
            if (klaliChart.series.length > 0) {
              $.each(res.data, function(index, item) {
                klaliChart.series[index].setData(item);
              })
            } else {
              $.each(res.data, function(index, item) {
                klaliChart.addSeries(item);
              })
            }
          } else if (jenis == "klapo") {
            klapoChart.xAxis[0].setCategories(res.list_upt);
            if (klapoChart.series.length > 0) {
              $.each(res.data, function(index, item) {
                klapoChart.series[index].setData(item);
              })
            } else {
              $.each(res.data, function(index, item) {
                klapoChart.addSeries(item);
              })
            }
          } else if (jenis == "prioritas") {
            prioritasChart.xAxis[0].setCategories(res.list_upt);
            if (prioritasChart.series.length > 0) {
              $.each(res.data, function(index, item) {
                prioritasChart.series[index].setData(item);
              })
            } else {
              $.each(res.data, function(index, item) {
                prioritasChart.addSeries(item);
              })
            }
          }
        }
      })
    }
  }

  $(document).ready(function() {
    getDataChart(pieChartKL, "<?= base_url() ?>dashboard/get_data/klali")
    getDataChart(pieChartKP, "<?= base_url() ?>dashboard/get_data/klapo")
    getDataChart(pieChartP, "<?= base_url() ?>dashboard/get_data/prioritas")
    // getJumlahUser()
    $("#f_upt_id").change(function() {
      getDataChart(pieChartKL, "<?= base_url() ?>dashboard/get_data/klali")
      getDataChart(pieChartKP, "<?= base_url() ?>dashboard/get_data/klapo")
      getDataChart(pieChartP, "<?= base_url() ?>dashboard/get_data/prioritas")
    })
    get_data_all_upt("klali")
    get_data_all_upt("klapo")
    get_data_all_upt("prioritas")

  })
</script>

<script src="https://kit.fontawesome.com/10e53f85be.js" crossorigin="anonymous"></script>
<script src="<?php echo base_url('assets/leaflet/leaflet.js') ?> "></script>

<script type="text/javascript">
  var map = L.map('mapid').setView([-7.4471541, 112.6718016], 12);

  L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
  }).addTo(map);

  $.getJSON("<?= base_url() ?>pemetaan/tower_json", function(data) {
    $.each(data, function(i, field) {

      var v_lat = parseFloat(data[i].lat);
      var v_long = parseFloat(data[i].lang);

      L.marker([v_lat, v_long]).addTo(map)
        .bindPopup('Penghantar : ' + data[i].penghantar + '</br>' + 'No Tower : ' + data[i].tower + '</br>' + 'Longitude : ' + data[i].lang + '</br>' + 'Latitude : ' + data[i].lat + '</br>' + 'Prioritas : ' + data[i].prioritas).openPopup();
    });
  });
</script>