<style type="text/css">
    .notice {
        padding: 15px;
        background-color: #fafafa;
        border-left: 6px solid #7f7f84;
        width: 500px;
        height: 200px;
        margin-bottom: 10px;
        -webkit-box-shadow: 0 5px 8px -6px rgba(0, 0, 0, .2);
        -moz-box-shadow: 0 5px 8px -6px rgba(0, 0, 0, .2);
        box-shadow: 0 5px 8px -6px rgba(0, 0, 0, .2);
    }

    .notice-sm {
        padding: 10px;
        font-size: 80%;
    }

    .notice-lg {
        padding: 35px;
        font-size: large;
    }

    .notice-success {
        border-color: #80D651;
    }

    .notice-success>strong {
        color: #80D651;
    }

    .notice-info {
        border-color: #45ABCD;
    }

    .notice-info>strong {
        color: #45ABCD;
    }

    .notice-warning {
        border-color: #FEAF20;
    }

    .notice-warning>strong {
        color: #FEAF20;
    }

    .notice-danger {
        border-color: #d73814;
    }

    .notice-danger>strong {
        color: #d73814;
    }
</style>

<?= $this->session->flashdata('pesan');
?>

<div class="card">
    <div class="card shadow">
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h4 class="m-0 font-weight-bold text-primary">Detail Tower Kritis<br> <?= $data->tower ?> / <?= $data->penghantar ?> <?= $data->kv ?> KV</h4>
        </div>
        <div class="card-body">
            <div class="container">
                <div class="row">
                    <div class="col-6 col-sm-4">
                        <font size="3"><b>Tanggal Justifikasi</b> </font></b><br><br>
                    </div>
                    <div class="col-6 col-sm-4">
                        <font size="3"><?= date("d-m-Y", strtotime($data->tgl)) ?> </font></b><br><br>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 col-sm-4">
                        <font size="3"><b>Tanggal Update</b> </font></b><br><br>
                    </div>
                    <div class="col-6 col-sm-4">
                        <font size="3"><?= date("d-m-Y", strtotime($data->update)) ?> </font></b><br><br>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 col-sm-4">
                        <font size="3"><b>Wilayah UPT</b> </font>
                    </div>
                    <div class="col-6 col-sm-4">
                        <font size="3"> <b> Wilayah ULTG </font></b>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 col-sm-4">
                        <font size="3"><?= $data->upt_nama ?></font>
                    </div>
                    <div class="col-6 col-sm-4">
                        <font size="3"> <?= $data->ultg_nama ?> </font>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-6 col-sm-4">
                        <font size="3"><b>Koordinat X </b></font>
                    </div>
                    <div class="col-6 col-sm-4">
                        <?= $data->lat ?>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-6 col-sm-4">
                        <font size="3"><b>Koordinat Y </b></font>
                    </div>
                    <div class="col-6 col-sm-4">
                        <?= $data->lang ?>
                    </div>
                </div><br>
                 <div class="row">
                    <div class="col-6 col-sm-4">
                        <font size="3"><b>Tautan </b></font>
                    </div>
                    <div class="col-6 col-sm-4">
                        <a href= "<?= $data->tautan ?>"> </a>
                    </div>
                </div><br>

                <div class="row">
                    <div class="col-6 col-sm-4">
                        <font size="3"><b>Document Kelengkapan KKP</b></font>
                    </div>
                    <div class="col-6 col-sm-4">
                        <font size="3"><b> Document Kelengkapan Lingkungan </b></font>
                    </div>
                </div>

                <div class="row">
                    <div class="col-6 col-sm-4">
                        <?php if ($data->kkp == "OK") : ?>
                            <?php if (!empty($data->kkp_file)) : ?>
                                <a href="<?= base_url() . "assets/documents/" . $data->kkp_file ?>" target="_blank">Lihat</a>
                            <?php else : ?>
                                Belum ada file
                            <?php endif; ?>
                        <?php else : ?>
                            -
                        <?php endif; ?>
                    </div>
                    <div class="col-6 col-sm-4">
                        <?php if ($data->kelling == "OK") : ?>
                            <?php if (!empty($data->kelling_file)) : ?>
                                <a href="<?= base_url() . "assets/documents/" . $data->kelling_file ?>" target="_blank">Lihat</a>
                            <?php else : ?>
                                Belum ada file
                            <?php endif; ?>
                        <?php else : ?>
                            -
                        <?php endif; ?>
                    </div>
                </div>
                <br>
                 <div class="row">
                    <div class="col-6 col-sm-4">
                        <font size="3"><b>Document Kelengkapan RAB</b></font>
                    </div>
                    <div class="col-6 col-sm-4">
                        <font size="3"><b> Document Kelengkapan Desain </b></font>
                    </div>
                </div>

                <div class="row">
                    <div class="col-6 col-sm-4">
                        <?php if ($data->kelrab == "OK") : ?>
                            <?php if (!empty($data->kelrab_file)) : ?>
                                <a href="<?= base_url() . "assets/documents/" . $data->kelrab_file ?>" target="_blank">Lihat</a>
                            <?php else : ?>
                                Belum ada file
                            <?php endif; ?>
                        <?php else : ?>
                            -
                        <?php endif; ?>
                    </div>
                    <div class="col-6 col-sm-4">
                        <?php if ($data->kelde == "OK") : ?>
                            <?php if (!empty($data->kelde_file)) : ?>
                                <a href="<?= base_url() . "assets/documents/" . $data->kelde_file ?>" target="_blank">Lihat</a>
                            <?php else : ?>
                                Belum ada file
                            <?php endif; ?>
                        <?php else : ?>
                            -
                        <?php endif; ?>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-6 col-sm-4">
                        <font size="3"><b>Document Kelengkapan Pondasi</b></font>
                    </div>
                    <div class="col-6 col-sm-4">
                        <font size="3"><b> Document Kelengkapan Foto </b></font>
                    </div>
                </div>

                <div class="row">
                    <div class="col-6 col-sm-4">
                        <?php if ($data->kelpo == "OK") : ?>
                            <?php if (!empty($data->kelpo_file)) : ?>
                                <a href="<?= base_url() . "assets/documents/" . $data->kelpo_file ?>" target="_blank">Lihat</a>
                            <?php else : ?>
                                Belum ada file
                            <?php endif; ?>
                        <?php else : ?>
                            -
                        <?php endif; ?>
                    </div>
                    <div class="col-6 col-sm-4">
                        <?php if ($data->kelfo == "OK") : ?>
                            <?php if (!empty($data->kelfo_file)) : ?>
                                <a href="<?= base_url() . "assets/documents/" . $data->kelfo_file ?>" target="_blank">Lihat</a>
                            <?php else : ?>
                                Belum ada file
                            <?php endif; ?>
                        <?php else : ?>
                            -
                        <?php endif; ?>
                    </div>
                </div>
                <br>


                <div>
                    <div class="notice notice-success"><strong>Skor / Penilaian</strong>
                        <div>
                            <font size="3">Assesmen Lingkungan : <?= $data->skoli ?></font>
                        </div>
                        <div>
                            <font size="3">Assesmen Pondasi : <?= $data->skopo ?></font>
                        </div>
                        <div>
                            <font size="3">Sifat Hujan : <?= $data->skohu ?></font>
                        </div>
                    </div>
                </div><br>

                <div>
                    <div class="notice notice-success"><strong>Klasifikasi</strong>
                        <div>
                            <font size="3">Ancaman Lingkungan : <?= ($data->klali != '0') ? ($data->klali != '1') ? ($data->klali != '2') ? "KRITIS" : "WASPADA" : "AMAN" : '-' ?></font>
                        </div>
                        <div>
                            <font size="3">Ancaman Pondasi : <?= $data->klapo != '0' ? $data->klapo != '1' ? $data->klapo != '2' ? "KRITIS" : "WASPADA" : "AMAN" : '-' ?></font>
                        </div>
                        <div>
                            <font size="3">Sifat Hujan : <?= $data->klahu != '0' ? $data->klahu != '1' ?  $data->klahu != '2' ? "ATAS NORMAL" :"NORMAL" : 'BAWAH NORMAL' : '-' ?></font>
                        </div>
                         <div>
                            <font size="3">Kategori Penanganan : <?= $data->kategori != '0' ? $data->kategori != '1' ? $data->kategori != '2' ? "KRITIS" :"WASPADA" : "AMAN" : '-' ?></font>
                        </div>
                    </div><br>

                    <div>
                        <div class="notice notice-info"><strong>Anomali</strong>
                            <div>
                                <font size="2"><?= $data->anomali ?></font>
                            </div>
                        </div><br>
                    </div>

                    <div>
                        <div class="notice notice-info"><strong>Keterangan</strong>
                            <div>
                                <font size="2"><?= $data->keterangan ?></font>
                            </div>
                        </div><br>
                    </div>

                    <div>
                        <div class="notice notice-info"><strong>Penanganan Sementara</strong>
                            <div>
                                <font size="2"><?= $data->penanganan ?></font>
                            </div>
                        </div><br>
                    </div>

                     <div>
                        <div class="notice notice-info"><strong>Risiko</strong>
                            <div>
                                <font size="2"><?= $data->risiko ?></font>
                            </div>
                        </div><br>
                    </div>

                     <div>
                        <div class="notice notice-info"><strong>Mitigasi</strong>
                            <div>
                                <font size="2"><?= $data->mitigasi ?></font>
                            </div>
                        </div><br>
                    </div>

                   
                                <?php if (!empty($data->foto1)) : ?>
                                    <a href="#" class="thumbnail">
                                        <img align="middle" src="<?= base_url() ?>assets/images/<?= $data->foto1 ?>" alt="foto">
                                    </a><br><br>
                                <?php endif; ?>
                                <?php if (!empty($data->foto2)) : ?>
                                    <a href="#" class="thumbnail">
                                        <img align="middle" src="<?= base_url() ?>assets/images/<?= $data->foto2 ?>" alt="foto">
                                    </a><br><br>
                                <?php endif; ?>
                                <?php if (!empty($data->foto3)) : ?>
                                    <a href="#" class="thumbnail">
                                        <img align="middle" src="<?= base_url() ?>assets/images/<?= $data->foto3 ?>" alt="foto">
                                    </a><br><br>
                                <?php endif; ?>
                                <?php if (!empty($data->foto4)) : ?>
                                    <a href="#" class="thumbnail">
                                        <img align ="middle" src="<?= base_url() ?>assets/images/<?= $data->foto4 ?>" alt="foto">
                                    </a>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>