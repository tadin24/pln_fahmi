<style type="text/css">
    .notice {
        padding: 20px;
        background-color: #fafafa;
        border-left: 6px solid #7f7f84;
        width: 800px;
        height: 400px;
        margin-bottom: 10px;
        -webkit-box-shadow: 0 5px 8px -6px rgba(0, 0, 0, .2);
        -moz-box-shadow: 0 5px 8px -6px rgba(0, 0, 0, .2);
        box-shadow: 0 5px 8px -6px rgba(0, 0, 0, .2);
    }

    .notice-sm {
        padding: 20px;
        font-size: 80%;
    }

    .notice-lg {
        padding: 35px;
        font-size: large;
    }

    .notice-success {
        border-color: #80D651;
    }

    .notice-success>strong {
        color: #80D651;
    }

    .notice-info {
        border-color: #45ABCD;
    }

    .notice-info>strong {
        color: #45ABCD;
    }

    .notice-warning {
        border-color: #FEAF20;
    }

    .notice-warning>strong {
        color: #FEAF20;
    }

    .notice-danger {
        border-color: #d73814;
    }

    .notice-danger>strong {
        color: #d73814;
    }
</style>

<?= $this->session->flashdata('pesan');
?>

<div class="card">
    <div class="card shadow">
        <div class="card-body">
            <div class="container">
                <div class="row">
                    <div class="col-6 col-sm-4">
                        <font size="3"><b>Tanggal Pengukuran</b> </font></b><br><br>
                    </div>
                    <div class="col-6 col-sm-4">
                        <font size="3">: <?= date("d-m-Y", strtotime($data->tgl)) ?> </font></b><br><br>
                    </div>
                </div>
               <div class="row">
                    <div class="col-6 col-sm-4">
                        <font size="3"><b>Gardu Induk </b></font>
                    </div>
                    <div class="col-6 col-sm-4">
                       : <?= $data->gi ?>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-6 col-sm-4">
                        <font size="3"><b>Tipe Tower </b></font>
                    </div>
                    <div class="col-6 col-sm-4">
                        : <?= $data->tipe_tower ?>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-6 col-sm-4">
                        <font size="3"><b>Desa </b></font>
                    </div>
                    <div class="col-6 col-sm-4">
                        : <?= $data->desa ?>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-6 col-sm-4">
                        <font size="3"><b>Kecamatan/Kabupaten </b></font>
                    </div>
                    <div class="col-6 col-sm-4">
                        : <?= $data->kecamatan ?>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-6 col-sm-4">
                        <font size="3"><b>Tembok Penahan </b></font>
                    </div>
                    <div class="col-6 col-sm-4">
                        : <?= $data->tembok ?>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-6 col-sm-4">
                        <font size="3"><b>Luas M2 </b></font>
                    </div>
                    <div class="col-6 col-sm-4">
                        : <?= $data->luas ?> M2
                    </div>
                </div><br>
                 <div class="row">
                    <div class="col-6 col-sm-4">
                        <font size="3"><b>Kondisi Tanah </b></font>
                    </div>
                    <div class="col-6 col-sm-4">
                        : <?= $data->kondisi_tanah ?>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-6 col-sm-4">
                        <font size="3"><b>Nilai Arde Sebelemnunya </b></font>
                    </div>
                    <div class="col-6 col-sm-4">
                        : <?= $data->nilai_arde ?>
                    </div>
                </div><br>
                 <div class="row">
                    <div class="col-6 col-sm-4">
                        <font size="3"><b>Hasil Pengukuran Terbesar </b></font>
                    </div>
                    <div class="col-6 col-sm-4">
                        : <?= $data->hasil ?>
                    </div>
                </div><br>
                 <div class="row">
                    <div class="col-6 col-sm-4">
                        <font size="3"><b>Nilai Arde Sebelemnunya </b></font>
                    </div>
                    <div class="col-6 col-sm-4">
                        : <?= $data->nilai_arde ?>
                    </div>
                </div><br>
                 <div class="row">
                    <div class="col-6 col-sm-4">
                        <font size="3"><b>Metode Pemasangan</b></font>
                    </div>
                    <div class="col-6 col-sm-4">
                        : <?= $data->metode ?>
                    </div>
                </div><br>
                 <div class="row">
                    <div class="col-6 col-sm-4">
                        <font size="3"><b>Anomalo </b></font>
                    </div>
                    <div class="col-6 col-sm-4">
                        : <?= $data->anomali ?>
                    </div>
                </div><br>
               
                <div>
                    <div class="notice notice-success"><strong>Hasil Pengukuran</strong><br><br>
                         <div class="row">
                    <div class="col-6 col-sm-4">
                        <font size="3"><b>Arde & Tower (PARALEL) </b></font>
                    </div>
                    <div class="col-6 col-sm-4">
                        : <?= $data->arde_tower ?>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-6 col-sm-4">
                        <font size="3"><b>Arde Tower Kaki A</b></font>
                    </div>
                    <div class="col-6 col-sm-4">
                        : <?= $data->arde_a?>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-6 col-sm-4">
                        <font size="3"><b>Arde Tower Kaki B </b></font>
                    </div>
                    <div class="col-6 col-sm-4">
                         : <?= $data->arde_b?>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-6 col-sm-4">
                        <font size="3"><b>Arde Tower Kaki C </b></font>
                    </div>
                    <div class="col-6 col-sm-4">
                         : <?= $data->arde_c?>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-6 col-sm-4">
                        <font size="3"><b>Arde Tower Kaki D </b></font>
                    </div>
                    <div class="col-6 col-sm-4">
                         : <?= $data->arde_d?>
                    </div>
                </div><br>
                 <div class="row">
                    <div class="col-6 col-sm-4">
                        <font size="3"><b>TOWER </b></font>
                    </div>
                    <div class="col-6 col-sm-4">
                         : <?= $data->tower_arde?>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-6 col-sm-4">
                        <font size="3"><b>GSW Langsung </b></font>
                    </div>
                    <div class="col-6 col-sm-4">
                         : <?= $data->arde_gsw?>
                    </div>
                </div><br>

                    </div>
                </div><br>


                 <div>
                    <div class="notice notice-danger"><strong>Hasil Pengukuran Semen Konduktif</strong><br><br>
                         <div class="row">
                    <div class="col-6 col-sm-4">
                        <font size="3"><b>Arde & Tower (PARALEL) </b></font>
                    </div>
                    <div class="col-6 col-sm-4">
                        : <?= $data->semen_tower ?>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-6 col-sm-4">
                        <font size="3"><b>Arde Tower Kaki A</b></font>
                    </div>
                    <div class="col-6 col-sm-4">
                        : <?= $data->semen_a?>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-6 col-sm-4">
                        <font size="3"><b>Arde Tower Kaki B </b></font>
                    </div>
                    <div class="col-6 col-sm-4">
                         : <?= $data->semen_b?>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-6 col-sm-4">
                        <font size="3"><b>Arde Tower Kaki C </b></font>
                    </div>
                    <div class="col-6 col-sm-4">
                         : <?= $data->semen_c?>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-6 col-sm-4">
                        <font size="3"><b>Arde Tower Kaki D </b></font>
                    </div>
                    <div class="col-6 col-sm-4">
                         : <?= $data->semen_d?>
                    </div>
                </div><br>
                 <div class="row">
                    <div class="col-6 col-sm-4">
                        <font size="3"><b>TOWER </b></font>
                    </div>
                    <div class="col-6 col-sm-4">
                         : <?= $data->tower_semen?>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-6 col-sm-4">
                        <font size="3"><b>GSW Langsung </b></font>
                    </div>
                    <div class="col-6 col-sm-4">
                         : <?= $data->semen_gsw?>
                    </div>
                </div><br>

                    </div>
                </div><br>

                 

                

                   
                            
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>