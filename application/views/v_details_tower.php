<style type="text/css">
    .notice {
        padding: 15px;
        background-color: #fafafa;
        border-left: 6px solid #7f7f84;
        width: 500px;
        height: 200px;
        margin-bottom: 10px;
        -webkit-box-shadow: 0 5px 8px -6px rgba(0, 0, 0, .2);
        -moz-box-shadow: 0 5px 8px -6px rgba(0, 0, 0, .2);
        box-shadow: 0 5px 8px -6px rgba(0, 0, 0, .2);
    }

    .notice-sm {
        padding: 10px;
        font-size: 80%;
    }

    .notice-lg {
        padding: 35px;
        font-size: large;
    }

    .notice-success {
        border-color: #80D651;
    }

    .notice-success>strong {
        color: #80D651;
    }

    .notice-info {
        border-color: #45ABCD;
    }

    .notice-info>strong {
        color: #45ABCD;
    }

    .notice-warning {
        border-color: #FEAF20;
    }

    .notice-warning>strong {
        color: #FEAF20;
    }

    .notice-danger {
        border-color: #d73814;
    }

    .notice-danger>strong {
        color: #d73814;
    }
</style>

<?= $this->session->flashdata('pesan');
?>

<div class="card">
    <div class="card shadow">
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h4 class="m-0 font-weight-bold text-primary">Detail Tower Kritis<br> <?= $data->tower ?> / <?= $data->pht ?></h4>
        </div>
        <div class="card-body">
            <div class="container">
                 <div class="row">
                    <div class="col-6 col-sm-4">
                        <font size="3"><b>Bay</b></font>
                    </div>
                    <div class="col-6 col-sm-4">
                        <?= $data->bay ?>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-6 col-sm-4">
                        <font size="3"><b>Tanggal Operasi</b> </font></b><br><br>
                    </div>
                    <div class="col-6 col-sm-4">
                        <font size="3"><?= date("d-m-Y", strtotime($data->tgl_operasi)) ?> </font></b><br><br>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 col-sm-4">
                        <font size="3"><b>Wilayah UPT</b> </font>
                    </div>
                    <div class="col-6 col-sm-4">
                        <font size="3"> <b> Wilayah ULTG </font></b>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 col-sm-4">
                        <font size="3"><?= $data->upt_nama ?></font>
                    </div>
                    <div class="col-6 col-sm-4">
                        <font size="3"> <?= $data->ultg_nama ?> </font>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-6 col-sm-4">
                        <font size="3"><b>Status Alat </b></font>
                    </div>
                    <div class="col-6 col-sm-4">
                        <?= $data->status_alat ?>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-6 col-sm-4">
                        <font size="3"><b>Kode Sirkit</b></font>
                    </div>
                    <div class="col-6 col-sm-4">
                        <?= $data->kode_sirkit ?>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-6 col-sm-4">
                        <font size="3"><b>Kode PST Old</b></font>
                    </div>
                    <div class="col-6 col-sm-4">
                        <?= $data->kode_pst_old ?>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-6 col-sm-4">
                        <font size="3"><b>Buatan</b></font>
                    </div>
                    <div class="col-6 col-sm-4">
                        <?= $data->buatan ?>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-6 col-sm-4">
                        <font size="3"><b>Tahun Buat</b></font>
                    </div>
                    <div class="col-6 col-sm-4">
                        <?= $data->tahun_buat ?>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-6 col-sm-4">
                        <font size="3"><b>Koordinat X </b></font>
                    </div>
                    <div class="col-6 col-sm-4">
                        <?= $data->lat ?>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-6 col-sm-4">
                        <font size="3"><b>Koordinat Y </b></font>
                    </div>
                    <div class="col-6 col-sm-4">
                        <?= $data->lang ?>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-6 col-sm-4">
                        <font size="3"><b>Luas TPK </b></font>
                    </div>
                    <div class="col-6 col-sm-4">
                        <?= $data->luas_tpk ?>M
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-6 col-sm-4">
                        <font size="3"><b>Posisi Tower </b></font>
                    </div>
                    <div class="col-6 col-sm-4">
                        <?= $data->posisi_tower ?>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-6 col-sm-4">
                        <font size="3"><b>RT RW </b></font>
                    </div>
                    <div class="col-6 col-sm-4">
                        <?= $data->rtrw ?>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-6 col-sm-4">
                        <font size="3"><b>Kelurahan </b></font>
                    </div>
                    <div class="col-6 col-sm-4">
                        <?= $data->kelurahan ?>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-6 col-sm-4">
                        <font size="3"><b>Kecamatan </b></font>
                    </div>
                    <div class="col-6 col-sm-4">
                        <?= $data->kecamatan ?>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-6 col-sm-4">
                        <font size="3"><b>Kabupatan </b></font>
                    </div>
                    <div class="col-6 col-sm-4">
                        <?= $data->kabupaten ?>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-6 col-sm-4">
                        <font size="3"><b>Propinsi </b></font>
                    </div>
                    <div class="col-6 col-sm-4">
                        <?= $data->propinsi ?>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-6 col-sm-4">
                        <font size="3"><b>Keterangan Tower </b></font>
                    </div>
                    <div class="col-6 col-sm-4">
                        <?= $data->keterangan_tower ?>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-6 col-sm-4">
                        <font size="3"><b>Merk</b></font>
                    </div>
                    <div class="col-6 col-sm-4">
                        <?= $data->merk ?>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-6 col-sm-4">
                        <font size="3"><b>Tipe Tower </b></font>
                    </div>
                    <div class="col-6 col-sm-4">
                        <?= $data->tipe_tower ?>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-6 col-sm-4">
                        <font size="3"><b>Jenis Tower </b></font>
                    </div>
                    <div class="col-6 col-sm-4">
                        <?= $data->jenis ?>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-6 col-sm-4">
                        <font size="3"><b>Jenis Material</b></font>
                    </div>
                    <div class="col-6 col-sm-4">
                        <?= $data->jenis_material ?>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-6 col-sm-4">
                        <font size="3"><b>Jenis Pelapis</b></font>
                    </div>
                    <div class="col-6 col-sm-4">
                        <?= $data->jenis_pelapis ?>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-6 col-sm-4">
                        <font size="3"><b>EQ Number </b></font>
                    </div>
                    <div class="col-6 col-sm-4">
                        <?= $data->eq_number ?>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-6 col-sm-4">
                        <font size="3"><b>ID Funtloc</b></font>
                    </div>
                    <div class="col-6 col-sm-4">
                        <?= $data->id_functloc?>
                    </div>
                </div><br>
                 <div class="row">
                    <div class="col-6 col-sm-4">
                        <font size="3"><b>ID Bay</b></font>
                    </div>
                    <div class="col-6 col-sm-4">
                        <?= $data->id_bay?>
                    </div>
                </div><br>


                

               
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>