<?= $this->session->flashdata('pesan') ?>
<h3>Form Import</h3>
<hr>

<a href="<?php echo base_url("excel/tower kritis.xlsx"); ?>">Download Format</a><br><br>


<!-- <table class="table table-reposive"> -->
<form action="<?php echo base_url("krisis/form"); ?>" method="POST" enctype="multipart/form-data">
  <input type="file" name="file_excel" id="file_excel">
  <input type="submit" name="preview" value="Preview">
</form>
<!-- </table> -->


<?php
if (isset($_POST['preview'])) { // Jika user menekan tombol Preview pada form
  if (isset($upload_error)) { // Jika proses upload gagal
    echo "<div style='color: red;'>" . $upload_error . "</div>"; // Muncul pesan error upload
    die; // stop skrip
  }
  // Buat sebuah tag form untuk proses import data ke database
  echo "<form method='post' action='" . base_url("krisis/import") . "'>";

  // Buat sebuah div untuk alert validasi kosong
  echo "<div style='color: red;' id='kosong'>
    Semua data belum diisi, Ada <span id='jumlah_kosong'></span> data yang belum diisi.
    </div>";

  echo "<table id='example1' class='table table-bordered table-striped'>
    <thead>
    <tr>
      <th colspan='5'>Preview Data</th>
    </tr>
    <tr>
      <th>Tanggal</th>
      <th>Update</th>
      <th>UPT</th>
      <th>ULTG</th>
      <th>Penghantar</th>
    </tr>";
  echo "</thead><tbody>";

  $numrow = 1;
  $kosong = 0;

  // Lakukan perulangan dari data yang ada di excel
  // $sheet adalah variabel yang dikirim dari controller
  foreach ($sheet as $row) {
    // Ambil data pada excel sesuai Kolom
    $tgl = $row['A'];
    $update = $row['A'];
    $upt = $row['C'];
    $ultg = $row['D'];
    $penghantar = $row['E'];
   


    // Cek jika semua data tidak diisi
    if ($tgl == "" && $update == "" && $upt == "" && $ultg == "")
      continue; // Lewat data pada baris ini (masuk ke looping selanjutnya / baris selanjutnya)

    // Cek $numrow apakah lebih dari 1
    // Artinya karena baris pertama adalah nama-nama kolom
    // Jadi dilewat saja, tidak usah diimport
    if ($numrow > 1) {
      // Validasi apakah semua data telah diisi
      $tgl_td = (!empty($tgl)) ? "" : " style='background: #E07171;'";
      $update_td = (!empty($update)) ? "" : " style='background: #E07171;'";
      $upt_td = (!empty($upt)) ? "" : " style='background: #E07171;'";
      $ultg_td = (!empty($ultg)) ? "" : " style='background: #E07171;'";
      $penghantar_td = (!empty($penghantar)) ? "" : " style='background: #E07171;'";


      // Jika salah satu data ada yang kosong
      if ($tgl == "" && $update == "" && $upt == "" && $ultg == "") {
        $kosong++; // Tambah 1 variabel $kosong
      }

      echo "<tr>";
      echo "<td" . $tgl_td . ">" . $tgl . "</td>";
      echo "<td" . $update_td . ">" . $update . "</td>";
      echo "<td" . $upt_td . ">" . $upt . "</td>";
      echo "<td" . $ultg_td . ">" . $ultg . "</td>";
      echo "<td" . $penghantar_td . ">" . $penghantar . "</td>";
     
      echo "</tr>";
    }

    $numrow++; // Tambah 1 setiap kali looping
  }

  echo '</tbody></table> ';

  // Cek apakah variabel kosong lebih dari 0
  // Jika lebih dari 0, berarti ada data yang masih kosong
  if ($kosong > 0) {
?>
    <script>
      $(document).ready(function() {
        // Ubah isi dari tag span dengan id jumlah_kosong dengan isi dari variabel kosong
        $("#jumlah_kosong").html('<?php echo $kosong; ?>');

        $("#kosong").show(); // Munculkan alert validasi kosong


      });
    </script>
<?php
  } else { // Jika semua data sudah diisi
    echo "<hr>";

    // Buat sebuah tombol untuk mengimport data ke database
    echo "<button type='submit' name='import'>Import</button>";
    echo "<a href='" . base_url("krisis") . "'>Cancel</a>";
  }

  echo "</form>";
}
?>