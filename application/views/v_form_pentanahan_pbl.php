<?= $this->session->flashdata('pesan') ?>
<h3>Form Import</h3>
<hr>

<a href="<?php echo base_url("excel/Pengukuran Pentanahan PLN UIT JBTB 2020.xlsx"); ?>">Download Format</a><br><br>


<!-- <table class="table table-reposive"> -->
<form action="<?php echo base_url("pentanahan_pbl/form"); ?>" method="POST" enctype="multipart/form-data">
  <input type="file" name="file_excel" id="file_excel">
  <input type="submit" name="preview" value="Preview">
</form>
<!-- </table> -->


<?php
if (isset($_POST['preview'])) { // Jika user menekan tombol Preview pada form
  if (isset($upload_error)) { // Jika proses upload gagal
    echo "<div style='color: red;'>" . $upload_error . "</div>"; // Muncul pesan error upload
    die; // stop skrip
  }
  // Buat sebuah tag form untuk proses import data ke database
  echo "<form method='post' action='" . base_url("pentanahan_pbl/import") . "'>";

  // Buat sebuah div untuk alert validasi kosong
  echo "<div style='color: red;' id='kosong'>
    Semua data belum diisi, Ada <span id='jumlah_kosong'></span> data yang belum diisi.
    </div>";

  echo "<table id='example1' class='table table-bordered table-striped'>
    <thead>
    <tr>
      <th colspan='5'>Preview Data</th>
    </tr>
    <tr>
      <th>Tanggal</th>
      <th>Update</th>
      <th>UPT</th>
      <th>ULTG</th>
      <th>Penghantar</th>
    </tr>";
  echo "</thead><tbody>";

  $numrow = 1;
  $kosong = 0;

  // Lakukan perulangan dari data yang ada di excel
  // $sheet adalah variabel yang dikirim dari controller
  foreach ($sheet as $row) {
    // Ambil data pada excel sesuai Kolom
    $gi = $row['B'];
    $penghantar = $row['C'];
    $no_tower = $row['D'];
    $tipe_tower = $row['F'];
    $nilai_arde = $row['G'];
   


    // Cek jika semua data tidak diisi
    if ($gi == "" && $penghantar == "" && $no_tower == "" && $tipe_tower == "")
      continue; // Lewat data pada baris ini (masuk ke looping selanjutnya / baris selanjutnya)

    // Cek $numrow apakah lebih dari 1
    // Artinya karena baris pertama adalah nama-nama kolom
    // Jadi dilewat saja, tidak usah diimport
    if ($numrow > 1) {
      // Validasi apakah semua data telah diisi
      $gi_td = (!empty($gi)) ? "" : " style='background: #E07171;'";
      $penghantar_td = (!empty($penghantar)) ? "" : " style='background: #E07171;'";
      $no_tower_td = (!empty($no_tower)) ? "" : " style='background: #E07171;'";
      $tipe_tower_td = (!empty($tipe_tower)) ? "" : " style='background: #E07171;'";
      $nilai_arde_td = (!empty($nilai_arde)) ? "" : " style='background: #E07171;'";


      // Jika salah satu data ada yang kosong
      if ($gi == "" && $penghantar == "" && $no_tower == "" && $tipe_tower == "") {
        $kosong++; // Tambah 1 variabel $kosong
      }

      echo "<tr>";
      echo "<td" . $gi_td . ">" . $gi . "</td>";
      echo "<td" . $penghantar_td . ">" . $penghantar . "</td>";
      echo "<td" . $no_tower_td . ">" . $no_tower . "</td>";
      echo "<td" . $tipe_tower_td . ">" . $tipe_tower . "</td>";
      echo "<td" . $nilai_arde_td . ">" . $nilai_arde . "</td>";
     
      echo "</tr>";
    }

    $numrow++; // Tambah 1 setiap kali looping
  }

  echo '</tbody></table> ';

  // Cek apakah variabel kosong lebih dari 0
  // Jika lebih dari 0, berarti ada data yang masih kosong
  if ($kosong > 0) {
?>
    <script>
      $(document).ready(function() {
        // Ubah isi dari tag span dengan id jumlah_kosong dengan isi dari variabel kosong
        $("#jumlah_kosong").html('<?php echo $kosong; ?>');

        $("#kosong").show(); // Munculkan alert validasi kosong


      });
    </script>
<?php
  } else { // Jika semua data sudah diisi
    echo "<hr>";

    // Buat sebuah tombol untuk mengimport data ke database
    echo "<button type='submit' name='import'>Import</button>";
    echo "<a href='" . base_url("pentanahan_pbl") . "'>Cancel</a>";
  }

  echo "</form>";
}
?>