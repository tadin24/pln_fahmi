<div class="row" id="formContainer" style="display: none;">
  <div class="col-lg-12">
    <div class="card shadow mb-4">
      <div class="card-header py-2 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 font-weight-bold text-primary">Form Master Group</h6>
        <button class="btn btn-primary btn-icon-split" id="btnListData">
          <span class="icon text-white-50">
            <i class="fas fa-database"></i>
          </span>
          <span class="text">List Data</span>
        </button>
      </div>
      <div class="card-body">
        <form action="" method="POST" enctype="multipart/form-data" id="form_vendor">
          <input type="hidden" name="act" id="act" value="add">
          <div class="form-group row">
            <label class="control-label col-md-3">Tanggal</label>
            <div class="col-md-3">
              <input type="text" name="tgl" id="tgl" class="form-control tanggal" value="<?= date('d-m-Y') ?>">
            </div>
          </div>
          <div class="form-group row">
            <label class="control-label col-md-3">Update</label>
            <div class="col-md-3">
              <input type="text" name="update" id="update" class="form-control tanggal" value="<?= date('d-m-Y') ?>">
            </div>
          </div>
          <div class="form-group row">
            <label class="control-label col-md-3">UPT</label>
            <div class="col-md-6">
              <select name="upt" id="upt" class="form-control">
                <?php foreach ($upt as $value) : ?>
                  <option value="<?= $value->upt_id ?>"><?= $value->upt_nama ?></option>
                <?php endforeach; ?>
              </select>
            </div>
          </div>
          <div class="form-group row">
            <label class="control-label col-md-3">ULTG</label>
            <div class="col-md-6">
              <select name="ultg" id="ultg" class="form-control">
                <?php foreach ($ultg as $value) : ?>
                  <option value="<?= $value->ultg_id ?>"><?= $value->ultg_nama ?></option>
                <?php endforeach; ?>
              </select>
            </div>
          </div>
          <div class="form-group row">
            <label class="control-label col-md-3">Penghantar</label>
            <div class="col-md-6">
              <select name="penghantar" id="penghantar" class="form-control">
                <option value="Antosari - Negara">Antosari - Negara</option>
                <option value="Balongbendo - Sekarputih">Balongbendo - Sekarputih</option>
                <option value="Bangkalan - Sampang">Bangkalan - Sampang</option>
                <option value="Banyuwangi - Gilimanuk">Banyuwangi - Gilimanuk</option>
                <option value="Baturiti - Pemaron">Baturiti - Pemaron</option>
                <option value="Cerme - Manyar">Cerme - Manyar</option>
                <option value="Darmogrande - Waru">Darmogrande - Waru</option>
                <option value="Driyorejo - Babadan">Driyorejo - Babadan</option>
                <option value="Gilimanuk - Pemaron">Gilimanuk - Pemanuk</option>
                <option value="Gilitimur - Bangkalan">Gilitimur - Bangkalan</option>
                <option value="Gilitimur - Bangkalan - Kenjaren">Gilitimur - Bangkalan - Kenjaren</option>
                <option value="Gondangwetan - Rejoso">Gondangwetan - Rejoso</option>
                <option value="Grati - Krian">Grati - Krian</option>
                <option value="Gresik - Krian">Gresik - Krian</option>
                <option value="Grati - Tandes">Grati - Tandes</option>
                <option value="INC New Pacitan - Nguntoronadi">INC New Pacitan - Nguntoronadi</option>
                <option value="Jember - Banyuwangi">Jember - Banyuwangi</option>
                <option value="Jember - Genteng - Banyuwangi">Jember - Genteng - Banyuwangi</option>
                <option value="Kapal - Baturiti/Payangan">Kapal - Baturiti/Payangan</option>
                <option value="Karangpilang - Waru">Karangpilang - Waru</option>
                <option value="Kebonagung - Sengguruh">Kebonagung - Sengguruh</option>
                <option value="Kebonagung - Sutami">Kebonagung - Sutami</option>
                <option value="Kediri - Pedan">Kediri - Pedan</option>
                <option value="Lamongan - Paciran">Lamongan - Paciran</option>
                <option value="Lumajang - Tanggul">Lumajang - Tanggul</option>
                <option value="Manisrejo - Ponorogo">Manisrejo - Ponorogo</option>
                <option value="Manyar - Maspion SBM">Manyar - Maspion SBM</option>
                <option value="Maspion - Manyar">Maspion - Manyar</option>
                <option value="Negara - Gilimanuk">Negara - Gilimanuk</option>
                <option value="New Pacitan - New Ponorogo">New Pacitan - New Ponorogo</option>
                <option value="New Ponorogo - Pacitan">New Ponorogo - Pacitan</option>
                <option value="Ngimbang - Krian">Ngimbang - Krian</option>
                <option value="Paiton - Grati">Paiton - Grati</option>
                <option value="Paiton - Kediri">Paiton - Kediri</option>
                <option value="Paiton - Situbondo">Paiton - Situbondo</option>
                <option value="Pemecutan Kelod - Bandara">Pemecutan Kelod - Bandara</option>
                <option value="Pesanggaran - Nusadua">Pesanggaran - Nusadua</option>
                <option value="PLTU Gresik  - Altaprima">PLTU Gresik - Altaprima</option>
                <option value="Negara - Gilimanuk">Negara - Gilimanuk</option>
                <option value="New Pacitan - New Ponorogo">New Pacitan - New Ponorogo</option>
                <option value="New Ponorogo - Pacitan">New Ponorogo - Pacitan</option>
                <option value="Ngimbang - Krian">Ngimbang - Krian</option>
                <option value="Paiton - Grati">Paiton - Grati</option>
                <option value="Paiton - Kediri">Paiton - Kediri</option>
                <option value="Paiton - Situbondo">Paiton - Situbondo</option>
                <option value="Pemecutan Kelod - Bandara">Pemecutan Kelod - Bandara</option>
                <option value="Pesanggaran - Nusadua">Pesanggaran - Nusadua</option>
                <option value="PLTU Gresik  - Altaprima">PLTU Gresik - Altaprima</option>
                <option value="PLTU Gresik - Tandes">PLTU Gresik - Tandes</option>
                <option value="PLTU Pacitan - New Pacitan">PLTU Pacitan - New Pacitan</option>
                <option value="PLTU Pacitan - New Pacitan - Nguntoronadi">PLTU Pacitan - New Pacitan - Nguntoronadi</option>
                <option value="Probolinggo - Gondangwetan">Probolinggo - Gondangwetan</option>
                <option value="Probolinggo - Lumajang">Probolinggo - Lumajang</option>
                <option value="Sambikerep - Waru">Sambikerep - Waru</option>
                <option value="Sanur - Gianyar">Sanur - Gianyar</option>
                <option value="Segoromadu - Cerme">Segoromadu - Cerme</option>
                <option value="Segoromadu - Lamongan">Segoromadu - Lamongan</option>
                <option value="Segoromadu - Petro">Segoromadu - Petro</option>
                <option value="Sekarputih - Siman - Mendalan">Sekarputih - Siman - Mendalan</option>
                <option value="Sekarputih - Tarik">Sekarputih - Tarik</option>
                <option value="Sekarputih - Ngoro">Sekarputih - Ngoro</option>
                <option value="Sekarputih - Mojoagung">Sekarputih - Mojoagung</option>
                <option value="Sengkaling - Mendalan">Sengkaling - Mendalan</option>
                <option value="Siman - Mendalan + Sekarputih">Siman - Mendalan + Sekarputih</option>
                <option value="Sukolilo - Rungkut">Sukolilo - Rungkut</option>
                <option value="Surabaya Barat - Altaprima">Surabaya Barat - Altaprima</option>
                <option value="Surabaya Barat - Balongbendo">Surabaya Barat - Balongbendo</option>
                <option value="Surabaya Barat - Sawahan">Surabaya Barat - Sawahan</option>
                <option value="Surabaya Barat - Tandes">Surabaya Barat - Tandes</option>
                <option value="Tarik - Driyorejo + Miwon">Tarik - Driyorejo + Miwon</option>
                <option value="Ungaran - Krian">Ungaran - Krian</option>
                <option value="Ungaran - Ngimbang">Ungaran - Ngimbang</option>
                <option value="Waru - Buduran - Maspion">Waru - Buduran - Maspion</option>
                <option value="Waru - Buduran - Sidoarjo">Waru - Buduran - Sidoarjo</option>
              </select>
            </div>
          </div>
          <div class="form-group row">
            <label class="control-label col-md-3">KV</label>
            <div class="col-md-6">
              <select name="kv" id="kv" class="form-control">
                <option value="70">70</option>
                <option value="150">150</option>
                <option value="500">500</option>
              </select>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Jenis Tower</label>
            <div class="col-md-6">
              <input type="text" name="jenis" id="jenis" class="form-control">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">No Tower</label>
            <div class="col-md-6">
              <input type="text" name="tower" id="tower" class="form-control">
            </div>
          </div>
          <div class="card mb-3">
            <div class="card-header">
              KELENGKAPAN DOKUMEN
            </div>
            <div class="card-body">
              <div class="form-group row">
                <label class="col-md-3 control-label">KKP</label>
                <div class="col-md-3">
                  <select class="form-control input-doc" name="is_kkp" id="is_kkp" data-id-file="view_kkp">
                    <option value="NOK">NOK</option>
                    <option value="OK">OK</option>
                    <option value="-">-</option>
                  </select>
                </div>
                <div class="col-md-6" id="view_kkp" style="display: none;">
                  <div class="custom-file">
                    <input type="file" class="custom-file-input" id="kkp" name="kkp">
                    <label class="custom-file-label" for="kkp">Pilih file</label>
                  </div>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-md-3 control-label">Assesmen Lingkungan</label>
                <div class="col-md-3">
                  <select class="form-control input-doc" name="is_kelling" id="is_kelling" data-id-file="view_kelling">
                    <option value="NIHIL">NIHIL</option>
                    <option value="NOK">NOK</option>
                    <option value="OK">OK</option>
                    <option value="-">-</option>
                  </select>
                </div>
                <div class="col-md-6" id="view_kelling" style="display:none">
                  <div class="custom-file">
                    <input type="file" class="custom-file-input" id="kelling" name="kelling">
                    <label class="custom-file-label" for="kelling">Pilih file</label>
                  </div>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-md-3 control-label">Assesmen Pondasi</label>
                <div class="col-md-3">
                  <select class="form-control input-doc" name="is_kelpo" id="is_kelpo" data-id-file="view_kelpo">
                    <option value="NIHIL">NIHIL</option>
                    <option value="NOK">NOK</option>
                    <option value="OK">OK</option>
                    <option value="-">-</option>
                  </select>
                </div>
                <div class="col-md-6" id="view_kelpo" style="display:none">
                  <div class="custom-file">
                    <input type="file" class="custom-file-input" id="kelpo" name="kelpo">
                    <label class="custom-file-label" for="kelpo">Pilih file</label>
                  </div>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-md-3 control-label">Kelengkapan Foto</label>
                <div class="col-md-3">
                  <select class="form-control input-doc" name="is_kelfo" id="is_kelfo" data-id-file="view_kelfo">
                    <option value="NOK">NOK</option>
                    <option value="OK">OK</option>
                    <option value="-">-</option>
                  </select>
                </div>
                <div class="col-md-6" id="view_kelfo" style="display:none">
                  <div class="custom-file">
                    <input type="file" class="custom-file-input" id="kelfo" name="kelfo">
                    <label class="custom-file-label" for="kelfo">Pilih file</label>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="card mb-3">
            <div class="card-header">
              SKOR
            </div>
            <div class="card-body">
              <div class="form-group row">
                <label class="col-md-3 control-label">Assesmen Lingkungan</label>
                <div class="col-md-3">
                  <input type="text" class="form-control" name="skoli" id="skoli">
                </div>
              </div>
              <div class="form-group row">
                <label class="col-md-3 control-label">Assesmen Pondasi</label>
                <div class="col-md-3">
                  <input type="text" name="skopo" id="skopo" class="form-control" value="">
                </div>
              </div>
              <div class="form-group row">
                <label class="col-md-3 control-label">Sifat Hujan</label>
                <div class="col-md-3">
                  <input type="text" name="skohu" id="skohu" class="form-control" value="">
                </div>
              </div>
            </div>
          </div>
          <div class="card mb-3">
            <div class="card-header">
              KLASIFIKASI
            </div>
            <div class="card-body">
              <div class="form-group row">
                <label class="col-md-3 control-label">Assesmen Lingkungan</label>
                <div class="col-md-3">
                  <select class="form-control" name="klali" id="klali">
                    <option value="1">AMAN</option>
                    <option value="2">WASPADA</option>
                    <option value="3">KRITIS</option>
                    <option value="0">-</option>
                  </select>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-md-3 control-label">Assesmen Pondasi</label>
                <div class="col-md-3">
                  <select class="form-control" name="klapo" id="klapo">
                    <option value="1">AMAN</option>
                    <option value="2">WASPADA</option>
                    <option value="3">KRITIS</option>
                    <option value="0">-</option>
                  </select>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-md-3 control-label">Sifat Hujan</label>
                <div class="col-md-3">
                  <select class="form-control" name="klahu" id="klahu">
                    <option value="1">AMAN</option>
                    <option value="2">WASPADA</option>
                    <option value="3">KRITIS</option>
                    <option value="0">-</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Anomali</label>
            <div class="col-md-6">
              <input type="text" name="anomali" id="anomali" class="form-control" value="">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Keterangan</label>
            <div class="col-md-6">
              <input type="text" name="keterangan" id="keterangan" class="form-control" value="">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Tautan Folder LINK</label>
            <div class="col-md-6">
              <input type="text" name="tautan" id="tautan" class="form-control" value="">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Penanganan</label>
            <div class="col-md-6">
              <input type="text" name="penanganan" id="penanganan" class="form-control" value="">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Risiko</label>
            <div class="col-md-6">
              <input type="text" name="risiko" id="risiko" class="form-control" value="">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Mitigasi Risiko</label>
            <div class="col-md-6">
              <input type="text" name="mitigasi" id="mitigasi" class="form-control" value="">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Foto1</label>
            <div class="col-md-6">
              <div class="custom-file">
                <input type="file" class="custom-file-input" id="file1" name="file1">
                <label class="custom-file-label" for="file1">Pilih file</label>
              </div>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Foto2</label>
            <div class="col-md-6">
              <div class="custom-file">
                <input type="file" class="custom-file-input" id="file2" name="file2">
                <label class="custom-file-label" for="file1">Pilih file</label>
              </div>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Foto3</label>
            <div class="col-md-6">
              <div class="custom-file">
                <input type="file" class="custom-file-input" id="file3" name="file3">
                <label class="custom-file-label" for="file1">Pilih file</label>
              </div>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Foto4</label>
            <div class="col-md-6">
              <div class="custom-file">
                <input type="file" class="custom-file-input" id="file4" name="file4">
                <label class="custom-file-label" for="file1">Pilih file</label>
              </div>
            </div>
          </div>
        </form>
        <div class="box-footer">
          <div class="row">
            <div class="col-md-12 text-center">
              <button type="button" class="btn btn-primary" id="saveData">Save</button>
              <button type="button" class="btn btn-default" id="cancelData">Cancel</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row" id="tableContainer">
  <div class="col-lg-12">
    <div class="card shadow mb-4">
      <div class="card-header py-2 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 font-weight-bold text-primary">Master Group</h6>

        <!-- <a href="<?= base_url('Krisis/krisis_tambah/') ?>" class="btn btn-primary btn-icon-split" id="btnFormData">
          <span class="icon text-white-50">
            <i class="fas fa-plus"></i>
          </span>
          <span class="text">Tambah</span>
        </a>
        <a href="<?= base_url('Krisis/export/') ?>" class="btn btn-primary btn-icon-split" id="btnFormData">
          <span class="icon text-white-50">
            <i class="fa fa-plus"></i>
          </span>
          <span class="text">Export</span>
        </a>
        <a href="<?= base_url('Krisis/form/') ?>" class="btn btn-primary btn-icon-split" id="btnFormData">
          <span class="icon text-white-50">
            <i class="fa fa-plus"></i>
          </span>
<<<<<<< HEAD
          <span class="text">Tambah</span>
        </a> -->
        <button class="btn btn-primary btn-icon-split" id="btnFormData">
          <span class="icon text-white-50">
            <i class="fas fa-plus"></i>
          </span>
          <span class="text">Tambah</span>
        </button>
=======
          <span class="text">Import</span>
        </a>
>>>>>>> 8cbb72b5b8ac56dbd4dec788aba00ff147f51f4b
      </div>

      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered" id="refrensi-table" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th class="text-center">No</th>
                <th class="text-center">Penghantar</th>
                <th class="text-center">No Tower</th>
                <th class="text-center">Jenis</th>
                <th class="text-center">Tanggal</th>
                <th class="text-center">Update</th>
                <th class="text-center">Anomali</th>
                <th class="text-center">Penanganan</th>
                <th class="text-center">Aksi</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  var tableAdvancedInit = function() {

    var initTable1 = function() {
      var target = '#refrensi-table';
      var oTable = $(target).dataTable({
        "displayStart": 0,
        "pageLength": 10,
        "lengthMenu": [
          [10, 25, 50, 100, -1],
          [10, 25, 50, 100, "All"]
        ],
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        // Load data for the table's content from an Ajax source
        "ajax": {
          "url": "<?php echo base_url('krisis/get_data') ?>",
          "dataType": "json",
          "type": "POST",
          // "data": function(d) {
          //   d.f_mmsi = $('#f_mmsi').val();
          // },
        },
        //Set column definition initialisation properties.
        columnDefs: [{
            "targets": [0, -1, -2, -3, -4], //last column
            "orderable": false, //set not orderable
          },
          {
            "targets": [0],
            "width": "5%",
            "className": "text-center"
          },
          {
            "targets": [1],
            "width": "20%"
          },
          {
            "targets": [2],
            "width": "5%"
          },
          {
            "targets": [3],
            "width": "10%"
          },
          {
            "targets": [4],
            "width": "8%",
            "className": "text-center"
          },
          {
            "targets": [5],
            "width": "8%",
            "className": "text-center"
          },
          {
            "targets": [6],
            "width": "14%"
          },
          {
            "targets": [7],
            "width": "15%"
          },
          {
            "targets": [8],
            "width": "15%",
            "className": "text-center"
          },
        ],
        "order": [
          [1, "asc"]
        ],

        "language": {
          // language settings
          "lengthMenu": "Display _MENU_ records",
          "search": "Search _INPUT_ <a class='btn default bts' href='javascript:void(0);'><i class='fa fa-search'></i></a>",
          "processing": '<img src="assets/plugins/global/images/owl.carousel/ajax-loader.gif"/><span>&nbsp;&nbsp;Loading...</span>',
          "infoEmpty": "No records found to show",
          "ajaxRequestGeneralError": "Could not complete request. Please check your internet connection",
          "emptyTable": "No data available in table",
          "zeroRecords": "No matching records found",
          "paginate": {
            "previous": "Prev",
            "next": "Next",
            "page": "Page",
            "pageOf": "of"
          }
        },
        "autoWidth": true, // disable fixed width and enable fluid table
        "orderCellsTop": true, // make sortable only the first row in thead
        "pagingType": "full_numbers", // pagination type(bootstrap, bootstrap_full_number or bootstrap_extended)
      });

      jQuery(target + '_wrapper .dataTables_filter input').addClass("form-control input-small input-inline"); // modify table search input
      jQuery(target + '_wrapper .dataTables_length select').addClass("form-control input-small"); // modify table per page dropdown
      jQuery(target + '_wrapper .dataTables_length select').select2(); // initialize select2 dropdown
      jQuery(target + '_wrapper .dataTables_filter input').unbind();
      jQuery(target + '_wrapper .dataTables_filter input').bind('keyup', function(e) {
        if (e.keyCode == 13) {
          oTable.fnFilter(this.value);
        }
      });
      jQuery(target + '_wrapper .dataTables_filter a').bind('click', function(e) {
        var key = jQuery(target + '_wrapper .dataTables_filter input').val();
        oTable.fnFilter(key);
      });
    }

    return {
      // public functions
      init: function() {
        if (!jQuery().dataTable) {
          return;
        }
        initTable1();
      }
    };
  }();
  $(document).ready(function() {
    tableAdvancedInit.init()


    $('#btnListData,#cancelData').click(function() {
      resetForm()
      $('#tableContainer').slideDown(500)
      $('#formContainer').slideUp(500)
    })


    $('#btnFormData').click(function() {
      resetForm()
      $('.input-doc').trigger('change')
      $('#tableContainer').slideUp(500)
      $('#formContainer').slideDown(500)
    })


    $('#form_vendor').validate()
  });


  // FUNGSI reset form menggunakan ARROW FUNCTION JS
  const resetForm = () => {
    $('#act').val("add");
    $('#form_vendor')[0].reset();
    $('#form_vendor').validate().resetForm();
    $('.custom-file-label').html("Pilih file");
  }

  $(".tanggal").datepicker({
    autoclose: true,
    dateFormat: "dd-mm-yy"
  })

  $('.custom-file-input').on('change', function() {
    //get the file name
    var fileName = $(this).val().replace('C:\\fakepath\\', " ");
    //replace the "Choose a file" label
    $(this).next('.custom-file-label').html(fileName);
  })

  $('.input-doc').on('change', function() {
    if ($(this).val() == "OK") {
      $('#' + $(this).data('id-file')).show()
    } else {
      $('#' + $(this).data('id-file')).hide()
    }
  })
</script>