    <style type="text/css">
        #mapid {
            height: 400px;
        }
    </style>

    <div id="layoutSidenav">
        <div id="layoutSidenav_content">
            <main style="padding-bottom:80px">
                <div class="container-fluid">
                    <h1 class="mt-2">Pemetaan Tower</h1>
                    <ol class="breadcrumb mb-4">
                        <li class="breadcrumb-item"><a href="<?= base_url('home'); ?>">Beranda</a></li>
                        <li class="breadcrumb-item active">Pemetaan</li>
                    </ol>
                    <div id="mapid"></div>
                </div>
            </main>
            
        </div>
    </div>

   <div class="row" id="formContainer" style="display: none;">
  <div class="col-lg-12">
    <div class="card shadow mb-4">
      <div class="card-header py-2 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 font-weight-bold text-primary">Form Master Group</h6>
        <button class="btn btn-primary btn-icon-split" id="btnListData">
          <span class="icon text-white-50">
            <i class="fas fa-database"></i>
          </span>
          <span class="text">List Data</span>
        </button>
      </div>
      <div class="card-body">
        <form action="" method="POST" enctype="multipart/form-data" id="form_vendor">
          <input type="hidden" name="act" id="act" value="add">
          <input type="hidden" name="id_krisis" id="id_krisis" value="">
          <div class="form-group row">
            <label class="control-label col-md-3">Tanggal</label>
            <div class="col-md-3">
              <input type="text" name="tgl" id="tgl" class="form-control tanggal" value="<?= date('d-m-Y') ?>">
            </div>
          </div>
          <div class="form-group row">
            <label class="control-label col-md-3">Update</label>
            <div class="col-md-3">
              <input type="text" name="update" id="update" class="form-control tanggal" value="<?= date('d-m-Y') ?>">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">No Folder</label>
            <div class="col-md-6">
              <input type="text" name="no_folder" id="no_folder" class="form-control">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">No Regional</label>
            <div class="col-md-6">
              <input type="text" name="no_reg" id="no_reg" class="form-control">
            </div>
          </div>
          <div class="form-group row">
            <label class="control-label col-md-3">UPT</label>
            <div class="col-md-6">
              <select name="upt" id="upt" class="form-control">
                <?php foreach ($upt as $value) : ?>
                  <option value="<?= $value->upt_id ?>"><?= $value->upt_nama ?></option>
                <?php endforeach; ?>
              </select>
            </div>
          </div>
          <div class="form-group row">
            <label class="control-label col-md-3">ULTG</label>
            <div class="col-md-6">
              <select name="ultg" id="ultg" class="form-control">
                <?php foreach ($ultg as $value) : ?>
                  <option value="<?= $value->ultg_id ?>"><?= $value->ultg_nama ?></option>
                <?php endforeach; ?>
              </select>
            </div>
          </div>
          <div class="form-group row">
            <label class="control-label col-md-3">Penghantar</label>
            <div class="col-md-6">
              <select name="penghantar_id" id="penghantar_id" class="form-control">
                <?php foreach ($penghantar as $value) : ?>
                  <option value="<?= $value->penghantar_id ?>"><?= $value->penghantar_nama ?></option>
                <?php endforeach; ?>
              </select>
            </div>
          </div>
          <div class="form-group row">
            <label class="control-label col-md-3">KV</label>
            <div class="col-md-6">
              <select name="kv" id="kv" class="form-control">
                <option value="70">70</option>
                <option value="150">150</option>
                <option value="500">500</option>
              </select>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Tipe Tower</label>
            <div class="col-md-6">
              <input type="text" name="jenis" id="jenis" class="form-control">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">No Tower</label>
            <div class="col-md-6">
              <input type="text" name="tower" id="tower" class="form-control">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Koordinat X</label>
            <div class="col-md-6">
              <input type="text" name="lat" id="lat" class="form-control">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Koordinat Y</label>
            <div class="col-md-6">
              <input type="text" name="lang" id="lang" class="form-control">
            </div>
          </div>
          <div class="card mb-3">
            <div class="card-header">
              KELENGKAPAN DOKUMEN
            </div>
            <div class="card-body">
              <div class="form-group row">
                <label class="col-md-3 control-label">KKP</label>
                <div class="col-md-3">
                  <select class="form-control input-doc" name="kkp" id="kkp" data-id-file="view_kkp">
                    <option value="NOK">NOK</option>
                    <option value="OK">OK</option>
                    <option value="-">-</option>
                  </select>
                </div>
                <div class="col-md-6" id="view_kkp" style="display: none;">
                  <div class="custom-file">
                    <input type="file" class="custom-file-input" id="kkp_file" name="kkp_file" accept="application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/pdf">
                    <label class="custom-file-label" for="kkp_file">Pilih file</label>
                  </div>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-md-3 control-label">Assesmen Lingkungan</label>
                <div class="col-md-3">
                  <select class="form-control input-doc" name="kelling" id="kelling" data-id-file="view_kelling">
                    <option value="NIHIL">NIHIL</option>
                    <option value="NOK">NOK</option>
                    <option value="OK">OK</option>
                    <option value="-">-</option>
                  </select>
                </div>
                <div class="col-md-6" id="view_kelling" style="display:none">
                  <div class="custom-file">
                    <input type="file" class="custom-file-input" id="kelling_file" name="kelling_file" accept="application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/pdf">
                    <label class="custom-file-label" for="kelling_file">Pilih file</label>
                  </div>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-md-3 control-label">Assesmen Pondasi</label>
                <div class="col-md-3">
                  <select class="form-control input-doc" name="kelpo" id="kelpo" data-id-file="view_kelpo">
                    <option value="NIHIL">NIHIL</option>
                    <option value="NOK">NOK</option>
                    <option value="OK">OK</option>
                    <option value="-">-</option>
                  </select>
                </div>
                <div class="col-md-6" id="view_kelpo" style="display:none">
                  <div class="custom-file">
                    <input type="file" class="custom-file-input" id="kelpo_file" name="kelpo_file" accept="application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/pdf">
                    <label class="custom-file-label" for="kelpo_file">Pilih file</label>
                  </div>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-md-3 control-label">Kelengkapan Foto</label>
                <div class="col-md-3">
                  <select class="form-control input-doc" name="kelfo" id="kelfo" data-id-file="view_kelfo">
                    <option value="NOK">NOK</option>
                    <option value="OK">OK</option>
                    <option value="-">-</option>
                  </select>
                </div>
                <div class="col-md-6" id="view_kelfo" style="display:none">
                  <div class="custom-file">
                    <input type="file" class="custom-file-input" id="kelfo_file" name="kelfo_file" accept="application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/pdf">
                    <label class="custom-file-label" for="kelfo_file">Pilih file</label>
                  </div>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-md-3 control-label">Assesmen RAB</label>
                <div class="col-md-3">
                  <select class="form-control input-doc" name="kelrab" id="kelrab" data-id-file="view_kelrab">
                    <option value="NIHIL">NIHIL</option>
                    <option value="NOK">NOK</option>
                    <option value="OK">OK</option>
                    <option value="-">-</option>
                  </select>
                </div>
                <div class="col-md-6" id="view_kelrab" style="display:none">
                  <div class="custom-file">
                    <input type="file" class="custom-file-input" id="kelrab_file" name="kelrab_file" accept="application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/pdf">
                    <label class="custom-file-label" for="kelrab_file">Pilih file</label>
                  </div>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-md-3 control-label">Assesmen Desain</label>
                <div class="col-md-3">
                  <select class="form-control input-doc" name="kelde" id="kelde" data-id-file="view_kelde">
                    <option value="NIHIL">NIHIL</option>
                    <option value="NOK">NOK</option>
                    <option value="OK">OK</option>
                    <option value="-">-</option>
                  </select>
                </div>
                <div class="col-md-6" id="view_kelde" style="display:none">
                  <div class="custom-file">
                    <input type="file" class="custom-file-input" id="kelde_file" name="kelde_file" accept="application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/pdf">
                    <label class="custom-file-label" for="kelde_file">Pilih file</label>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="card mb-3">
            <div class="card-header">
              SKOR
            </div>
            <div class="card-body">
              <div class="form-group row">
                <label class="col-md-3 control-label">Assesmen Lingkungan</label>
                <div class="col-md-3">
                  <input type="text" class="form-control" name="skoli" id="skoli">
                </div>
              </div>
              <div class="form-group row">
                <label class="col-md-3 control-label">Assesmen Pondasi</label>
                <div class="col-md-3">
                  <input type="text" name="skopo" id="skopo" class="form-control" value="">
                </div>
              </div>
              <div class="form-group row">
                <label class="col-md-3 control-label">Sifat Hujan</label>
                <div class="col-md-3">
                  <input type="text" name="skohu" id="skohu" class="form-control" value="">
                </div>
              </div>
            </div>
          </div>
          <div class="card mb-3">
            <div class="card-header">
              KLASIFIKASI
            </div>
            <div class="card-body">
              <div class="form-group row">
                <label class="col-md-3 control-label">Assesmen Lingkungan</label>
                <div class="col-md-3">
                  <select class="form-control" name="klali" id="klali">
                    <option value="1">AMAN</option>
                    <option value="2">WASPADA</option>
                    <option value="3">KRITIS</option>
                    <option value="0">-</option>
                  </select>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-md-3 control-label">Assesmen Pondasi</label>
                <div class="col-md-3">
                  <select class="form-control" name="klapo" id="klapo">
                    <option value="1">AMAN</option>
                    <option value="2">WASPADA</option>
                    <option value="3">KRITIS</option>
                    <option value="0">-</option>
                  </select>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-md-3 control-label">Sifat Hujan</label>
                <div class="col-md-3">
                  <select class="form-control" name="klahu" id="klahu">
                    <option value="1">ATAS NORMAL</option>
                    <option value="2">NORMAL</option>
                    <option value="3">BAWAH NORMAL</option>
                    <option value="0">-</option>
                  </select>
                </div>
              </div>
               <div class="form-group row">
                <label class="col-md-3 control-label">Kategori</label>
                <div class="col-md-3">
                  <select class="form-control" name="kategori" id="kategori">
                    <option value="1">AMAN</option>
                    <option value="2">WASPADA</option>
                    <option value="3">KRITIS</option>
                    <option value="0">-</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Anomali</label>
            <div class="col-md-6">
              <input type="text" name="anomali" id="anomali" class="form-control" value="">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Keterangan</label>
            <div class="col-md-6">
              <input type="text" name="keterangan" id="keterangan" class="form-control" value="">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Tautan Folder LINK</label>
            <div class="col-md-6">
              <input type="text" name="tautan" id="tautan" class="form-control" value="">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Penanganan</label>
            <div class="col-md-6">
              <input type="text" name="penanganan" id="penanganan" class="form-control" value="">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Risiko</label>
            <div class="col-md-6">
              <input type="text" name="risiko" id="risiko" class="form-control" value="">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Mitigasi Risiko</label>
            <div class="col-md-6">
              <input type="text" name="mitigasi" id="mitigasi" class="form-control" value="">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Foto1</label>
            <div class="col-md-6">
              <div class="custom-file">
                <input type="file" class="custom-file-input" id="foto1" name="foto1" accept="image/*">
                <label class="custom-file-label" for="foto1">Pilih file</label>
              </div>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Foto2</label>
            <div class="col-md-6">
              <div class="custom-file">
                <input type="file" class="custom-file-input" id="foto2" name="foto2" accept="image/*">
                <label class="custom-file-label" for="foto2">Pilih file</label>
              </div>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Foto3</label>
            <div class="col-md-6">
              <div class="custom-file">
                <input type="file" class="custom-file-input" id="foto3" name="foto3" accept="image/*">
                <label class="custom-file-label" for="foto3">Pilih file</label>
              </div>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Foto4</label>
            <div class="col-md-6">
              <div class="custom-file">
                <input type="file" class="custom-file-input" id="foto4" name="foto4" accept="image/*">
                <label class="custom-file-label" for="foto4">Pilih file</label>
              </div>
            </div>
          </div>
        </form>
        <div class="box-footer">
          <div class="row">
            <div class="col-md-12 text-center">
              <button type="button" class="btn btn-primary" id="saveData">Save</button>
              <button type="button" class="btn btn-default" id="cancelData">Cancel</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row" id="tableContainer">
  <div class="col-lg-12">
    <div class="card shadow mb-4">
      <div class="card-header py-2 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 font-weight-bold text-primary">Master Group</h6>

        <!-- <a href="<?= base_url('Krisis/krisis_tambah/') ?>" class="btn btn-primary btn-icon-split" id="btnFormData">
          <span class="icon text-white-50">
            <i class="fas fa-plus"></i>
          </span>
          <span class="text">Tambah</span>
        </a>
        
        <a href="<?= base_url('Krisis/form/') ?>" class="btn btn-primary btn-icon-split" id="btnFormData">
          <span class="icon text-white-50">
            <i class="fa fa-plus"></i>
          </span>
          <span class="text">Tambah</span>
        </a> -->
       <!--  <button class="btn btn-primary btn-icon-split" id="btnFormData">
          <span class="icon text-white-50">
            <i class="fas fa-plus"></i>
          </span>
              <span class="text">Tambah</span>
        </button> -->
      </div>

      <div class="card-body">
        <div class="row mb-2">
          <div class="col-md-2">
            <a href="<?= base_url('krisis/form/') ?>" class="btn btn-success btn-icon-split" id="btnFormData">
              <span class="icon text-white-50">
                <i class="fa fa-file-excel"></i>
              </span>
              <span class="text">Import</span>
            </a>
          </div>
          <div class="row mb-2">
        <div class="table-responsive">
          <table class="table table-bordered" id="refrensi-table" width="100%" cellspacing="0">
            <thead>
              <tr>

                <th class="text-center">No</th>
                <th class="text-center">Penghantar</th>
                <th class="text-center">BAY</th>
                <th class="text-center">PHT</th>
                <th class="text-center">Latitude</th>
                <th class="text-center">Longitude</th>
                <th class="text-center">Lingkungan</th>
                <th class="text-center">Pondasi</th>
                
                <th class="text-center">Aksi</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  var tableAdvancedInit = function() {

    var initTable1 = function() {
      var target = '#refrensi-table';
      var oTable = $(target).dataTable({
        "displayStart": 0,
        "pageLength": 10,
        "lengthMenu": [
          [10, 25, 50, 100, -1],
          [10, 25, 50, 100, "All"]
        ],
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        // Load data for the table's content from an Ajax source
        "ajax": {
          "url": "<?php echo base_url('pemetaan/get_data') ?>",
          "dataType": "json",
          "type": "POST",
          // "data": function(d) {
          //   d.f_mmsi = $('#f_mmsi').val();
          // },
        },
        //Set column definition initialisation properties.
        columnDefs: [{
            "targets": [0, -1, -2, -3, -4], //last column
            "orderable": false, //set not orderable
          },
          {
            "targets": [0],
            "width": "5%",
            "className": "text-center"
          },
          {
            "targets": [1],
            "width": "15%"
          },
          {
            "targets": [2],
            "width": "15%"
          },
          {
            "targets": [3],
            "width": "18%"
          },
          {
            "targets": [4],
            "width": "10%",
            "className": "text-center"
          },
          {
            "targets": [5],
            "width": "10%",
            "className": "text-center"
          },
          {
            "targets": [6],
            "width": "10%"
          },
          {
            "targets": [7],
            "width": "10%",
            "className": "text-center"
          },
          {
            "targets": [8],
            "width": "10%"
          },
        ],
        "order": [
          [1, "asc"]
        ],

        "language": {
          // language settings
          "lengthMenu": "Display _MENU_ records",
          "search": "Search _INPUT_ <a class='btn default bts' href='javascript:void(0);'><i class='fa fa-search'></i></a>",
          "processing": '<img src="assets/plugins/global/images/owl.carousel/ajax-loader.gif"/><span>&nbsp;&nbsp;Loading...</span>',
          "infoEmpty": "No records found to show",
          "ajaxRequestGeneralError": "Could not complete request. Please check your internet connection",
          "emptyTable": "No data available in table",
          "zeroRecords": "No matching records found",
          "paginate": {
            "previous": "Prev",
            "next": "Next",
            "page": "Page",
            "pageOf": "of"
          }
        },
        "autoWidth": true, // disable fixed width and enable fluid table
        "orderCellsTop": true, // make sortable only the first row in thead
        "pagingType": "full_numbers", // pagination type(bootstrap, bootstrap_full_number or bootstrap_extended)
      });

      jQuery(target + '_wrapper .dataTables_filter input').addClass("form-control input-small input-inline"); // modify table search input
      jQuery(target + '_wrapper .dataTables_length select').addClass("form-control input-small"); // modify table per page dropdown
      jQuery(target + '_wrapper .dataTables_length select').select2(); // initialize select2 dropdown
      jQuery(target + '_wrapper .dataTables_filter input').unbind();
      jQuery(target + '_wrapper .dataTables_filter input').bind('keyup', function(e) {
        if (e.keyCode == 13) {
          oTable.fnFilter(this.value);
        }
      });
      jQuery(target + '_wrapper .dataTables_filter a').bind('click', function(e) {
        var key = jQuery(target + '_wrapper .dataTables_filter input').val();
        oTable.fnFilter(key);
      });
    }

    return {
      // public functions
      init: function() {
        if (!jQuery().dataTable) {
          return;
        }
        initTable1();
      }
    };
  }();
  $(document).ready(function() {
    tableAdvancedInit.init()


    $('#btnListData,#cancelData').click(function() {
      resetForm()
      $('#tableContainer').slideDown(500)
      $('#formContainer').slideUp(500)
    })


    $('#btnFormData').click(function() {
      resetForm()
      $('.input-doc').trigger('change')
      $('#tableContainer').slideUp(500)
      $('#formContainer').slideDown(500)
    })


    // #saveData on click
    $('#saveData').click(e => {
      $('#form_vendor').submit()
    })


    // validasi form_vendor
    $("#form_vendor").validate({
      // define validation rules
      rules: {
        foto1: {
          filesize: 1000000,
          extension: "png|jpg|jpeg|gif"
        },
        foto2: {
          filesize: 1000000,
          extension: "png|jpg|jpeg|gif"
        },
        foto3: {
          filesize: 1000000,
          extension: "png|jpg|jpeg|gif"
        },
        foto4: {
          filesize: 1000000,
          extension: "png|jpg|jpeg|gif"
        },
      },
      messages: {
        foto1: {
          filesize: "Ukuran File Tidak boleh melebihi 1MB"
        },
        foto2: {
          filesize: "Ukuran File Tidak boleh melebihi 1MB"
        },
        foto3: {
          filesize: "Ukuran File Tidak boleh melebihi 1MB"
        },
        foto4: {
          filesize: "Ukuran File Tidak boleh melebihi 1MB"
        },
      },
      submitHandler: function(form) {
        $('#saveData').attr('disabled', true).text("Loading...");
        const formData = new FormData();
        const params = $('#form_vendor').serializeArray()
        const files = $('#form_vendor').find('.custom-file-input');

        $.each(files, (i, item) => {
          formData.append($(item).attr('name'), $(item)[0].files[0])
        })
        $.each(params, (i, item) => {
          formData.append(item.name, item.value)
        })
        formData.append('penghantar', $('#penghantar_id option:selected').text())
        $.ajax({
          url: '<?php echo base_url('krisis/save') ?>',
          type: "POST",
          data: formData,
          dataType: "json",
          cache: false,
          mimeType: "multipart/form-data",
          success: function(response, status, xhr, $form) {
            if (response > 0) {
              $('#saveData').attr('disabled', false).text("Save");
              $("#cancelData").click();
              Swal.fire({
                icon: 'success',
                title: 'Success!',
                text: 'Data Berhasil Disimpan!',
                showConfirmButton: false,
                timer: 1500
              })
              loadTbl();
            }
          },
          contentType: false,
          processData: false
        }, 'json');
      }
    });
  });


  // FUNGSI edit menggunakan ARROW FUNCTION JS
  const set_val = id => {
    resetForm();
    $('.input-doc').trigger('change')
    $('#act').val("edit");
    $.ajax({
      url: '<?= base_url() ?>krisis/edit/' + id,
      dataType: 'json',
      cache: false,
      success: res => {
        console.log(res);
        $('#id_krisis').val(res.id_krisis);
        $('#tgl').val(res.tgl);
        $('#update').val(res.update);
        $('#no_folder').val(res.no_folder);
        $('#no_reg').val(res.no_reg);
        $('#upt_id').val(res.upt_id);
        $('#ultg_id').val(res.ultg_id);
        $('#penghantar_id').val(res.penghantar_id);
        $('#jenis').val(res.jenis);
        $('#tower').val(res.tower);
        $('#lat').val(res.lat);
         $('#lang').val(res.lang);
        $('#kkp').val(res.kkp);
        $('#kkp').trigger('change');
        if (res.kkp == 'OK' && res.kkp_file != '' && res.kkp_file) {
          $('#kkp_file').parent().find('.custom-file-label').text(res.kkp_file);
        }
        $('#kelling').val(res.kelling);
        $('#kelling').trigger('change');
        if (res.kelling == 'OK' && res.kelling_file != '' && res.kelling_file) {
          $('#kelling_file').parent().find('.custom-file-label').text(res.kelling_file);
        }
        $('#kelpo').val(res.kelpo);
        $('#kelpo').trigger('change');
        if (res.kelpo == 'OK' && res.kelpo_file != '' && res.kelpo_file) {
          $('#kelpo_file').parent().find('.custom-file-label').text(res.kelpo_file);
        }
        $('#kelfo').val(res.kelfo);
        $('#kelfo').trigger('change');
        if (res.kelfo == 'OK' && res.kelfo_file != '' && res.kelfo_file) {
          $('#kelfo_file').parent().find('.custom-file-label').text(res.kelfo_file);
        }
        $('#kelrab').val(res.kelrab);
        $('#kelrab').trigger('change');
        if (res.kelrab == 'OK' && res.kelrab_file != '' && res.kelrab_file) {
          $('#kelrab_file').parent().find('.custom-file-label').text(res.kelrab_file);
        }
        $('#kelde').val(res.kelde);
        $('#kelde').trigger('change');
        if (res.kelde == 'OK' && res.kelde_file != '' && res.kelde_file) {
          $('#kelde_file').parent().find('.custom-file-label').text(res.kelde_file);
        }
        $('#skoli').val(res.skoli);
        $('#skopo').val(res.skopo);
        $('#skohu').val(res.skohu);
        $('#klali').val(res.klali);
        $('#klapo').val(res.klapo);
        $('#klahu').val(res.klahu);
        $('#kategori').val(res.kategori);
        $('#anomali').val(res.anomali);
        $('#keterangan').val(res.keterangan);
        $('#tautan').val(res.tautan);
        $('#penanganan').val(res.penanganan);
        $('#risiko').val(res.risiko);
        $('#mitigasi').val(res.mitigasi);
        if (res.foto1 != '' && res.foto1) {
          $('#foto1').parent().find('.custom-file-label').text(res.foto1);
        }
        if (res.foto2 != '' && res.foto2) {
          $('#foto2').parent().find('.custom-file-label').text(res.foto2);
        }
        if (res.foto3 != '' && res.foto3) {
          $('#foto3').parent().find('.custom-file-label').text(res.foto3);
        }
        if (res.foto4 != '' && res.foto4) {
          $('#foto4').parent().find('.custom-file-label').text(res.foto4);
        }
      }
    })
    $('#tableContainer').slideUp(500)
    $('#formContainer').slideDown(500)
  }


  // FUNGSI delete menggunakan ARROW FUNCTION JS
  const del = id => {
    Swal.fire({
      title: 'Apa Anda yakin menghapus Data ini?',
      text: "Data yang terhapus tidak bisa dikembalikan!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Ya, Hapus!',
      cancelButtonText: 'Tidak, Batal!',
    }).then((result) => {
      if (result.isConfirmed) {
        $.ajax({
          url: '<?= base_url() ?>krisis/delete/' + id,
          cache: false,
          dataType: 'json',
          success: res => {
            if (res) {
              Swal.fire(
                'Deleted!',
                'Data telah dihapus.',
                'success'
              )
              loadTbl()
            }
          }
        })
      }
    })
  }


  // FUNGSI reset form menggunakan ARROW FUNCTION JS
  const resetForm = () => {
    $('#act').val("add");
    $('#form_vendor')[0].reset();
    $('#form_vendor').validate().resetForm();
    $('.custom-file-label').html("Pilih file");
  }


  // FUNGSI loadTbl menggunakan ARROW FUNCTION JS
  const loadTbl = () => {
    $("#refrensi-table").dataTable().fnDraw();
  }

  $(".tanggal").datepicker({
    autoclose: true,
    dateFormat: "dd-mm-yy"
  })

  $('.custom-file-input').on('change', function() {
    //get the file name
    var fileName = $(this).val().replace('C:\\fakepath\\', " ");
    //replace the "Choose a file" label
    $(this).parent().find('.custom-file-label').html(fileName);
    $('#form_vendor').validate().resetForm();
    $('#form_vendor').validate().element($(this));
  })

  $('.input-doc').on('change', function() {
    if ($(this).val() == "OK") {
      $('#' + $(this).data('id-file')).show()
    } else {
      $('#' + $(this).data('id-file')).hide()
    }
  })
</script>


    <script src="https://kit.fontawesome.com/10e53f85be.js" crossorigin="anonymous"></script>
    <script src="<?php echo base_url('assets/leaflet/leaflet.js') ?> "></script>

    <script type="text/javascript">
        var map = L.map('mapid').setView([-7.4471541, 112.6718016], 12);

        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map);

        $.getJSON("<?= base_url() ?>pemetaan/tower_json", function(data) {
            $.each(data, function(i, field) {

                var v_lat = parseFloat(data[i].lat);
                var v_long = parseFloat(data[i].lang);

                L.marker([v_lat, v_long]).addTo(map)
                    .bindPopup(data[i].penghantar)
            });
        });
    </script>
