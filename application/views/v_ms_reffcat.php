<div class="row" id="formContainer" style="display: none;">
  <div class="col-lg-12">
    <!-- general form elements -->
    <div class="card shadow mb-4">
      <!-- .card-header -->
      <div class="card-header py-2 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 font-weight-bold text-primary">Form Master Kategori Referensi</h6>
        <button class="btn btn-primary btn-icon-split" id="btnListData">
          <span class="icon text-white-50">
            <i class="fas fa-database"></i>
          </span>
          <span class="text">List Data</span>
        </button>
      </div>
      <!-- /.card-header -->
      <!-- form start -->
      <div class="card-body">
        <form id="form_vendor">
          <input type="hidden" name="reffcat_id" id="reffcat_id">
          <input type="hidden" name="act" id="act" value="add">
          <div class="form-group row">
            <label class="control-label col-md-3">Nama</label>
            <div class="col-md-6">
              <input type="text" class="form-control" id="reffcat_nama" name="reffcat_nama" placeholder="Nama Kategori">
              <input type="hidden" id="reffcat_nama_lama" name="reffcat_nama_lama">
            </div>
          </div>
          <div class="form-group row">
            <label class="control-label col-md-3">Status</label>
            <div class="col-md-6">
              <select class="form-control" id="reffcat_status" name="reffcat_status">
                <option value="1">Aktif</option>
                <option value="0">Non Aktif</option>
              </select>
            </div>
          </div>
        </form>
      </div>
      <!-- /.card-body -->

      <div class="card-footer">
        <div class="row">
          <div class="col-md-12 text-center">
            <button type="button" class="btn btn-primary" id="saveData">Save</button>
            <button type="button" class="btn btn-default" id="cancelData">Cancel</button>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>
<div class="row" id="tableContainer">
  <div class="col-md-12">
    <div class="card shadow mb-4">
      <!-- .card-header -->
      <div class="card-header py-2 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 font-weight-bold text-primary">Master Kategori Referensi</h6>
        <button class="btn btn-primary btn-icon-split" id="btnFormData">
          <span class="icon text-white-50">
            <i class="fas fa-plus"></i>
          </span>
          <span class="text">Tambah</span>
        </button>
      </div>
      <!-- /.card-header -->
      <!-- .card-body -->
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered" id="table-reference" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th class="text-center">No</th>
                <th class="text-center">Nama</th>
                <th class="text-center">Status</th>
                <th class="text-center">Aksi</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
      <!-- /.card-body -->
    </div>
  </div>
</div>

<script>
  var tableAdvancedInit = function() {

    var initTable1 = function() {
      var target = '#table-reference';
      var oTable = $(target).dataTable({
        "displayStart": 0,
        "pageLength": 10,
        "lengthMenu": [
          [10, 25, 50, 100, -1],
          [10, 25, 50, 100, "All"]
        ],
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        // Load data for the table's content from an Ajax source
        "ajax": {
          "url": "<?php echo base_url('ms_reffcat/get_data') ?>",
          "dataType": "json",
          "type": "POST",
          // "data": function(d) {
          //   d.f_mmsi = $('#f_mmsi').val();
          // },
        },
        //Set column definition initialisation properties.
        columnDefs: [{
            "targets": [0, -1], //last column
            "orderable": false, //set not orderable
          },
          {
            "targets": [0],
            "width": "10%",
            "className": "text-center"
          },
          {
            "targets": [1],
            "width": "65%"
          },
          {
            "targets": [2],
            "width": "10%",
            "className": "text-center"
          },
          {
            "targets": [3],
            "width": "15%",
            "className": "text-center"
          },
        ],
        "order": [
          [1, "asc"]
        ],

        "language": {
          // language settings
          "lengthMenu": "Display _MENU_ records",
          "search": "Search _INPUT_ <a class='btn default bts' href='javascript:void(0);'><i class='fa fa-search'></i></a>",
          "processing": '<img src="assets/plugins/global/images/owl.carousel/ajax-loader.gif"/><span>&nbsp;&nbsp;Loading...</span>',
          "infoEmpty": "No records found to show",
          "ajaxRequestGeneralError": "Could not complete request. Please check your internet connection",
          "emptyTable": "No data available in table",
          "zeroRecords": "No matching records found",
          "paginate": {
            "previous": "Prev",
            "next": "Next",
            "page": "Page",
            "pageOf": "of"
          }
        },
        "autoWidth": true, // disable fixed width and enable fluid table
        "orderCellsTop": true, // make sortable only the first row in thead
        "pagingType": "full_numbers", // pagination type(bootstrap, bootstrap_full_number or bootstrap_extended)
      });

      jQuery(target + '_wrapper .dataTables_filter input').addClass("form-control input-small input-inline"); // modify table search input
      jQuery(target + '_wrapper .dataTables_length select').addClass("form-control input-small"); // modify table per page dropdown
      jQuery(target + '_wrapper .dataTables_length select').select2(); // initialize select2 dropdown
      jQuery(target + '_wrapper .dataTables_filter input').unbind();
      jQuery(target + '_wrapper .dataTables_filter input').bind('keyup', function(e) {
        if (e.keyCode == 13) {
          oTable.fnFilter(this.value);
        }
      });
      jQuery(target + '_wrapper .dataTables_filter a').bind('click', function(e) {
        var key = jQuery(target + '_wrapper .dataTables_filter input').val();
        oTable.fnFilter(key);
      });
    }

    return {
      // public functions
      init: function() {
        if (!jQuery().dataTable) {
          return;
        }
        initTable1();
      }
    };
  }();

  function loadTbl() {
    $("#table-reference").dataTable().fnDraw();
  }

  $(document).ready(function() {
    tableAdvancedInit.init();
  })


  // FUNGSI save
  $('#saveData').click(function(e) {
    $('#form_vendor').submit()
  })


  $("#form_vendor").validate({
    // define validation rules
    rules: {
      reffcat_nama: {
        required: true,
        remote: {
          url: "<?= base_url() ?>ms_reffcat/cek_reffcat_nama",
          type: "post",
          data: {
            reffcat_nama: function() {
              return $("#reffcat_nama").val();
            },
            act: function() {
              return $("#act").val();
            },
            reffcat_nama_lama: function() {
              return $("#reffcat_nama_lama").val();
            },
          }
        }

      },
    },
    submitHandler: function(form) {
      $('#saveData').attr('disabled', true).text("Loading...");
      $.ajax({
        url: '<?php echo base_url('ms_reffcat/save') ?>',
        type: "POST",
        data: $('#form_vendor').serialize(),
        dataType: "json",
        cache: false,
        success: function(response, status, xhr, $form) {
          var d = response;
          if (d == 'true') {
            Swal.fire({
              icon: 'success',
              title: 'Success!',
              text: 'Data Berhasil Disimpan!',
              showConfirmButton: false,
              timer: 1500
            })
            $('#saveData').attr('disabled', false).text("Save");
            $("#cancelData").click();
            loadTbl();
          }
        }
      }, 'json');
    }
  });


  $('#btnListData,#cancelData').click(function() {
    resetForm()
    $('#tableContainer').slideDown(500)
    $('#formContainer').slideUp(500)
  })


  $('#btnFormData').click(function() {
    resetForm()
    $('#tableContainer').slideUp(500)
    $('#formContainer').slideDown(500)
  })


  // FUNGSI reset form menggunakan ARROW FUNCTION JS
  const resetForm = () => {
    $('#act').val("add");
    $('#reffcat_id,#reffcat_nama_lama').val("");
    $('#form_vendor')[0].reset();
    $('#form_vendor').validate().resetForm();
  }


  // FUNGSI delete menggunakan ARROW FUNCTION JS
  const del = id => {
    Swal.fire({
      title: 'Apa Anda yakin menghapus Data ini?',
      text: "Data yang terhapus tidak bisa dikembalikan!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Ya, Hapus!',
      cancelButtonText: 'Tidak, Batal!',
    }).then((result) => {
      if (result.isConfirmed) {
        $.ajax({
          url: "<?php echo base_url() ?>ms_reffcat/delete/" + id,
          cache: false,
          dataType: 'json',
          success: res => {
            if (res == "true") {
              Swal.fire(
                'Deleted!',
                'Data telah dihapus.',
                'success'
              )
              loadTbl()
            }
          }
        })
      }
    })
  }


  // FUNGSI edit
  function edit(params) {
    const isi = params.split('|')
    $('#act').val("edit")
    $('#reffcat_id').val(isi[0])
    $('#reffcat_nama').val(isi[1])
    $('#reffcat_nama_lama').val(isi[1])
    $('#reffcat_status').val(isi[2])
    $('#tableContainer').slideUp(500)
    $('#formContainer').slideDown(500)
  }
</script>