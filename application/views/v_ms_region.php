<div class="row" id="formContainer" style="display: none;">
  <div class="col-lg-12">
    <!-- general form elements -->
    <div class="card shadow mb-4">
      <!-- .card-header -->
      <div class="card-header py-2 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 font-weight-bold text-primary">Form Master Region</h6>
        <button class="btn btn-primary btn-icon-split" id="btnListData">
          <span class="icon text-white-50">
            <i class="fas fa-database"></i>
          </span>
          <span class="text">List Data</span>
        </button>
      </div>
      <!-- /.card-header -->
      <!-- form start -->
      <div class="card-body">
        <form id="form_vendor">
          <input type="hidden" name="act" id="act" value="add">
          <input type="hidden" name="reg_level" id="reg_level" value="1">
          <input type="hidden" name="reg_level_lama" id="reg_level_lama" value="1">
          <div class="form-group row">
            <label class="control-label col-md-3">Parent</label>
            <div class="col-md-6">
              <select class="form-control" id="reg_parent" name="reg_parent" placeholder="Parent">
                <option value="0" level="0">0 - ROOT</option>
              </select>
              <input type="hidden" id="reg_parent_lama" name="reg_parent_lama" value="">
            </div>
          </div>
          <div class="form-group row">
            <label class="control-label col-md-3">Kode</label>
            <div class="col-md-6">
              <input type="text" class="form-control" id="reg_code" name="reg_code" placeholder="Kode Region">
              <input type="hidden" id="reg_code_lama" name="reg_code_lama" value="">
            </div>
          </div>
          <div class="form-group row">
            <label class="control-label col-md-3">Nama</label>
            <div class="col-md-6">
              <input type="text" class="form-control" id="reg_name" name="reg_name" placeholder="Nama Region">
            </div>
          </div>
          <div class="form-group row">
            <label class="control-label col-md-3">Status</label>
            <div class="col-md-6">
              <select class="form-control" id="reg_active" name="reg_active" placeholder="Status Region">
                <option value="1">Aktif</option>
                <option value="0">Non Aktif</option>
              </select>
            </div>
          </div>
        </form>
      </div>
      <!-- /.card-body -->

      <div class="card-footer">
        <div class="row">
          <div class="col-md-12 text-center">
            <button type="button" class="btn btn-primary" id="saveData">Save</button>
            <button type="button" class="btn btn-default" id="cancelData">Cancel</button>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>
<div class="row" id="tableContainer">
  <div class="col-md-12">
    <div class="card shadow mb-4">
      <!-- .card-header -->
      <div class="card-header py-2 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 font-weight-bold text-primary">Master Region</h6>
        <button class="btn btn-primary btn-icon-split" id="btnFormData">
          <span class="icon text-white-50">
            <i class="fas fa-plus"></i>
          </span>
          <span class="text">Tambah</span>
        </button>
      </div>
      <!-- /.card-header -->
      <!-- .card-body -->
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered" id="table-reference" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th class="text-center">No</th>
                <th class="text-center">Kode</th>
                <th class="text-center">Nama</th>
                <th class="text-center">Parent</th>
                <th class="text-center">Status</th>
                <th class="text-center">Aksi</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
      <!-- /.card-body -->
    </div>
  </div>
</div>

<script>
  var tableAdvancedInit = function() {

    var initTable1 = function() {
      var target = '#table-reference';
      var oTable = $(target).dataTable({
        "displayStart": 0,
        "pageLength": 10,
        "lengthMenu": [
          [10, 25, 50, 100, -1],
          [10, 25, 50, 100, "All"]
        ],
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        // Load data for the table's content from an Ajax source
        "ajax": {
          "url": "<?php echo base_url('ms_region/get_data') ?>",
          "dataType": "json",
          "type": "POST",
          // "data": function(d) {
          //   d.f_mmsi = $('#f_mmsi').val();
          // },
        },
        //Set column definition initialisation properties.
        columnDefs: [{
            "targets": [0, -1], //last column
            "orderable": false, //set not orderable
          },
          {
            "targets": [0],
            "width": "5%",
            "className": "text-center"
          },
          {
            "targets": [1],
            "width": "15%",
            "className": "text-left"
          },
          {
            "targets": [2],
            "width": "30%",
            "className": "text-left"
          },
          {
            "targets": [3],
            "width": "20%",
          },
          {
            "targets": [4],
            "width": "15%",
            "className": "text-center"
          },
          {
            "targets": [5],
            "width": "15%",
            "className": "text-center"
          },
        ],
        "order": [
          [1, "asc"]
        ],

        "language": {
          // language settings
          "lengthMenu": "Display _MENU_ records",
          "search": "Search _INPUT_ <a class='btn default bts' href='javascript:void(0);'><i class='fa fa-search'></i></a>",
          "processing": '<img src="assets/plugins/global/images/owl.carousel/ajax-loader.gif"/><span>&nbsp;&nbsp;Loading...</span>',
          "infoEmpty": "No records found to show",
          "ajaxRequestGeneralError": "Could not complete request. Please check your internet connection",
          "emptyTable": "No data available in table",
          "zeroRecords": "No matching records found",
          "paginate": {
            "previous": "Prev",
            "next": "Next",
            "page": "Page",
            "pageOf": "of"
          }
        },
        "autoWidth": true, // disable fixed width and enable fluid table
        "orderCellsTop": true, // make sortable only the first row in thead
        "pagingType": "full_numbers", // pagination type(bootstrap, bootstrap_full_number or bootstrap_extended)
      });

      jQuery(target + '_wrapper .dataTables_filter input').addClass("form-control input-small input-inline"); // modify table search input
      jQuery(target + '_wrapper .dataTables_length select').addClass("form-control input-small"); // modify table per page dropdown
      jQuery(target + '_wrapper .dataTables_length select').select2(); // initialize select2 dropdown
      jQuery(target + '_wrapper .dataTables_filter input').unbind();
      jQuery(target + '_wrapper .dataTables_filter input').bind('keyup', function(e) {
        if (e.keyCode == 13) {
          oTable.fnFilter(this.value);
        }
      });
      jQuery(target + '_wrapper .dataTables_filter a').bind('click', function(e) {
        var key = jQuery(target + '_wrapper .dataTables_filter input').val();
        oTable.fnFilter(key);
      });
    }

    return {
      // public functions
      init: function() {
        if (!jQuery().dataTable) {
          return;
        }
        initTable1();
      }
    };
  }();

  function loadTbl() {
    $("#table-reference").dataTable().fnDraw();
  }

  $(document).ready(function() {
    tableAdvancedInit.init();
  })



  // FUNGSI save
  $('#saveData').click(function(e) {
    $('#form_vendor').submit()
  })


  $("#form_vendor").validate({
    // define validation rules
    rules: {
      reg_parent: {
        remote: {
          url: "<?= base_url() ?>ms_region/cek_parent",
          type: "post",
          data: {
            reg_parent: function() {
              return $("#reg_parent").val();
            },
            reg_code: function() {
              return $("#reg_code").val();
            },
          }
        }
      },
      reg_code: {
        required: true,
        remote: {
          url: "<?= base_url() ?>ms_region/cek_reg_code",
          type: "post",
          data: {
            reg_code: function() {
              return $("#reg_code").val();
            },
            act: function() {
              return $("#act").val();
            },
            reg_code_lama: function() {
              return $("#reg_code_lama").val();
            },
          }
        }
      },
      reg_name: {
        required: true,
      },
    },
    messages: {
      reg_code: {
        required: "Isi Kode Region!",
      },
      reg_name: {
        required: "Isi Nama Region!",
      },
    },
    submitHandler: function(form) {
      $('#saveData').attr('disabled', true).text("Loading...");
      $.ajax({
        url: '<?php echo base_url('ms_region/save') ?>',
        type: "POST",
        data: $('#form_vendor').serialize(),
        dataType: "json",
        cache: false,
        success: function(response, status, xhr, $form) {
          var d = response;
          if (d == 'true') {
            $('#saveData').attr('disabled', false).text("Save");
            Swal.fire({
              icon: 'success',
              title: 'Success!',
              text: 'Data Berhasil Disimpan!',
              showConfirmButton: false,
              timer: 1500
            })
            $("#cancelData").click();
            loadTbl();
          }
        }
      }, 'json');
    }
  });


  $('#btnListData,#cancelData').click(function() {
    resetForm()
    $('#tableContainer').slideDown(500)
    $('#formContainer').slideUp(500)
  })


  $('#btnFormData').click(function() {
    resetForm()
    getRegion()
    $('#tableContainer').slideUp(500)
    $('#formContainer').slideDown(500)
  })


  // FUNGSI reset form menggunakan ARROW FUNCTION JS
  const resetForm = () => {
    $('#act').val("add");
    $('#reg_code').val("");
    $('#reg_level').val(1);
    $('#reg_level_lama').val(1);
    $('#reg_parent').val("0").trigger("change.select2");
    $('#reg_parent_lama,#reg_code_lama').val("");
    $('#form_vendor')[0].reset();
    $('#form_vendor').validate().resetForm();
    $('#saveData').attr('disabled', false).text("Save");
  }


  // FUNGSI delete menggunakan ARROW FUNCTION JS
  const del = id => {
    Swal.fire({
      title: 'Apa Anda yakin menghapus Data ini?',
      text: "Data yang terhapus tidak bisa dikembalikan!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Ya, Hapus!',
      cancelButtonText: 'Tidak, Batal!',
    }).then((result) => {
      if (result.isConfirmed) {
        $.ajax({
          url: "<?php echo base_url() ?>ms_region/delete/" + id,
          cache: false,
          dataType: 'json',
          success: res => {
            if (res == "true") {
              Swal.fire(
                'Deleted!',
                'Data telah dihapus.',
                'success'
              )
              loadTbl()
            }
          }
        })
      }
    })
  }


  // FUNGSI edit
  function edit(params) {
    const isi = params.split('|')
    $('#act').val("edit")
    $('#reg_code').val(isi[0])
    $('#reg_code_lama').val(isi[0])
    $('#reg_name').val(isi[1])
    $('#reg_active').val(isi[2])
    $('#reg_level').val(isi[3])
    $('#reg_level_lama').val(isi[3])
    getRegion(isi[4])
    $('#reg_parent_lama').val(isi[4])
    $('#tableContainer').slideUp(500)
    $('#formContainer').slideDown(500)
  }


  // FUNGSI get Region parent
  function getRegion(selectedItem = 0) {
    $('#reg_parent option[value!="0"]').remove()
    $.ajax({
      url: "<?= base_url() ?>ms_region/get_region",
      cache: false,
      dataType: "json",
      success: function(res) {
        let opt = selected = "";
        if (res.length > 0) {
          $.each(res, function(index, item) {
            if (item.reg_code == selectedItem) {
              selected = "selected"
            } else {
              selected = ""
            }
            opt += `<option level="${item.reg_level}" value="${item.reg_code}" ${selected}>${item.reg_code} - ${item.reg_name}</option>`
            if (res.length - 1 == index) {
              $("#reg_parent").append(opt)
            }
          })
        }
      }
    })
  }


  // fungsi select2
  $("#reg_parent").select2().on("select2:select", function(event) {
    $('#reg_level').val(parseInt($(event.currentTarget).find("option:selected").attr("level")) + 1);
  });
</script>