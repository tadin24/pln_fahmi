<div class="row" id="formContainer" style="display: none;">
  <div class="col-lg-12">
    <div class="card shadow mb-4">
      <div class="card-header py-2 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 font-weight-bold text-primary">Form Master Penghantar</h6>
        <button class="btn btn-primary btn-icon-split" id="btnListData">
          <span class="icon text-white-50">
            <i class="fas fa-database"></i>
          </span>
          <span class="text">List Data</span>
        </button>
      </div>
      <div class="card-body">
        <form action="" method="POST" enctype="multipart/form-data" id="form_vendor">
          <input type="hidden" name="act" id="act" value="add">
          <input type="hidden" name="penghantar_id" id="penghantar_id" value="">
          <div class="form-group row">
            <label class="col-md-3 control-label">Provinsi</label>
            <div class="col-md-6">
              <select name="prov_kode" id="prov_kode" class="form-control">
                <?php foreach ($opt_prov as $v) : ?>
                  <option value="<?= $v->reg_code ?>"><?= $v->reg_name ?></option>
                <?php endforeach; ?>
              </select>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Kab / Kota</label>
            <div class="col-md-6">
              <select name="kab_kode" id="kab_kode" class="form-control">
              </select>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Kecamatan</label>
            <div class="col-md-6">
              <select name="kec_kode" id="kec_kode" class="form-control">
              </select>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Nama penghantar</label>
            <div class="col-md-6">
              <input type="text" name="penghantar_nama" id="penghantar_nama" class="form-control">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Status</label>
            <div class="col-md-6">
              <select name="penghantar_status" id="penghantar_status" class="form-control">
                <option value="1">Aktif</option>
                <option value="0">Non Aktif</option>
              </select>
            </div>
          </div>
        </form>
        <div class="box-footer">
          <div class="row">
            <div class="col-md-12 text-center">
              <button type="button" class="btn btn-primary" id="saveData">Save</button>
              <button type="button" class="btn btn-default" id="cancelData">Cancel</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row" id="tableContainer">
  <div class="col-lg-12">
    <div class="card shadow mb-4">
      <div class="card-header py-2 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 font-weight-bold text-primary">Master Penghantar</h6>
        <button class="btn btn-primary btn-icon-split" id="btnFormData">
          <span class="icon text-white-50">
            <i class="fas fa-plus"></i>
          </span>
          <span class="text">Tambah</span>
        </button>
      </div>

      <div class="card-body">
        <div class="table-responsive">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-md-3 control-label">Kondisi</label>
                <div class="col-md-9">
                  <select name="f_kondisi" id="f_kondisi" class="form-control">
                    <option value="1">Selesai</option>
                    <option value="0">Belum Lengkap</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-md-3 control-label">Provinsi</label>
                <div class="col-md-9">
                  <select name="f_prov_kode" id="f_prov_kode" class="form-control">
                    <?php foreach ($opt_prov as $v) : ?>
                      <option value="<?= $v->reg_code ?>"><?= $v->reg_name ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-md-3 control-label">Kab / Kota</label>
                <div class="col-md-9">
                  <select name="f_kab_kode" id="f_kab_kode" class="form-control"></select>
                </div>
              </div>
            </div>
          </div>
          <table class="table table-bordered" id="refrensi-table" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th class="text-center">No</th>
                <th class="text-center">Nama penghantar</th>
                <th class="text-center">Kecamatan</th>
                <th class="text-center">Status</th>
                <th class="text-center">Aksi</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  var tableAdvancedInit = function() {

    var initTable1 = function() {
      var target = '#refrensi-table';
      var oTable = $(target).dataTable({
        "displayStart": 0,
        "pageLength": 10,
        "lengthMenu": [
          [10, 25, 50, 100, -1],
          [10, 25, 50, 100, "All"]
        ],
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        // Load data for the table's content from an Ajax source
        "ajax": {
          "url": "<?php echo base_url('penghantar_gi/get_data') ?>",
          "dataType": "json",
          "type": "POST",
          "data": function(d) {
            d.f_prov_kode = $('#f_prov_kode').val();
            d.f_kab_kode = $('#f_kab_kode').val();
            d.f_kondisi = $('#f_kondisi').val();
          },
        },
        //Set column definition initialisation properties.
        columnDefs: [{
            "targets": [0, -1], //last column
            "orderable": false, //set not orderable
          },
          {
            "targets": [0],
            "width": "10%",
            "className": "text-center"
          },
          {
            "targets": [1],
            "width": "50%"
          },
          {
            "targets": [2],
            "width": "20%",
          },
          {
            "targets": [3],
            "width": "10%",
            "className": "text-center"
          },
          {
            "targets": [4],
            "width": "10%",
            "className": "text-center"
          },

        ],
        "order": [
          [1, "asc"]
        ],

        "language": {
          // language settings
          "lengthMenu": "Display _MENU_ records",
          "search": "Search _INPUT_ <a class='btn default bts' href='javascript:void(0);'><i class='fa fa-search'></i></a>",
          "processing": '<img src="assets/plugins/global/images/owl.carousel/ajax-loader.gif"/><span>&nbsp;&nbsp;Loading...</span>',
          "infoEmpty": "No records found to show",
          "ajaxRequestGeneralError": "Could not complete request. Please check your internet connection",
          "emptyTable": "No data available in table",
          "zeroRecords": "No matching records found",
          "paginate": {
            "previous": "Prev",
            "next": "Next",
            "page": "Page",
            "pageOf": "of"
          }
        },
        "autoWidth": true, // disable fixed width and enable fluid table
        "orderCellsTop": true, // make sortable only the first row in thead
        "pagingType": "full_numbers", // pagination type(bootstrap, bootstrap_full_number or bootstrap_extended)
      });

      jQuery(target + '_wrapper .dataTables_filter input').addClass("form-control input-small input-inline"); // modify table search input
      jQuery(target + '_wrapper .dataTables_length select').addClass("form-control input-small"); // modify table per page dropdown
      jQuery(target + '_wrapper .dataTables_length select').select2(); // initialize select2 dropdown
      jQuery(target + '_wrapper .dataTables_filter input').unbind();
      jQuery(target + '_wrapper .dataTables_filter input').bind('keyup', function(e) {
        if (e.keyCode == 13) {
          oTable.fnFilter(this.value);
        }
      });
      jQuery(target + '_wrapper .dataTables_filter a').bind('click', function(e) {
        var key = jQuery(target + '_wrapper .dataTables_filter input').val();
        oTable.fnFilter(key);
      });
    }

    return {
      // public functions
      init: function() {
        if (!jQuery().dataTable) {
          return;
        }
        initTable1();
      }
    };
  }();
  $(document).ready(function() {
    tableAdvancedInit.init()
    getWilFilter('kab', $('#f_prov_kode').val())


    $('#btnListData,#cancelData').click(function() {
      resetForm()
      $('#tableContainer').slideDown(500)
      $('#formContainer').slideUp(500)
    })


    $('#btnFormData').click(function() {
      resetForm()
      $('#kab_kode').val($('#f_kab_kode').val())
      getWil('kec', $('#f_kab_kode').val())
      $('#tableContainer').slideUp(500)
      $('#formContainer').slideDown(500)
    })


    // #saveData on click
    $('#saveData').click(e => {
      e.preventDefault();
      $('#form_vendor').submit()
    })


    // validasi form_vendor
    $("#form_vendor").validate({
      rules: {
        penghantar_nama: {
          required: true
        },
      },
      // define validation rules
      submitHandler: function(form) {
        $('#saveData').attr('disabled', true).text("Loading...");
        const formData = $("#form_vendor").serialize()

        $.ajax({
          url: '<?php echo base_url('penghantar_gi/save') ?>',
          type: "POST",
          data: formData,
          dataType: "json",
          cache: false,
          success: function(response, status, xhr, $form) {
            if (response > 0) {
              $('#saveData').attr('disabled', false).text("Save");
              $("#cancelData").click();
              Swal.fire({
                icon: 'success',
                title: 'Success!',
                text: 'Data Berhasil Disimpan!',
                showConfirmButton: false,
                timer: 1500
              })
              loadTbl();
            }
          },
        }, 'json');
      }
    });
  });


  // FUNGSI edit menggunakan ARROW FUNCTION JS
  const set_val = id => {
    resetForm();
    $('#act').val("edit");
    $.ajax({
      url: '<?= base_url() ?>penghantar_gi/edit/' + id,
      dataType: 'json',
      cache: false,
      success: res => {
        $('#penghantar_id').val(res.penghantar_id);
        $('#penghantar_nama').val(res.penghantar_nama);
        $('#penghantar_status').val(res.penghantar_status);
        $('#prov_kode').val(res.prov_kode);
        $('#kab_kode').val(res.kab_kode);
        $('#kec_kode').val(res.kec_kode);
      }
    })
    $('#tableContainer').slideUp(500)
    $('#formContainer').slideDown(500)
  }


  // FUNGSI delete menggunakan ARROW FUNCTION JS
  const del = id => {
    Swal.fire({
      title: 'Apa Anda yakin menghapus Data ini?',
      text: "Data yang terhapus tidak bisa dikembalikan!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Ya, Hapus!',
      cancelButtonText: 'Tidak, Batal!',
    }).then((result) => {
      if (result.isConfirmed) {
        $.ajax({
          url: '<?= base_url() ?>penghantar_gi/delete/' + id,
          cache: false,
          dataType: 'json',
          success: res => {
            if (res) {
              Swal.fire(
                'Deleted!',
                'Data telah dihapus.',
                'success'
              )
              loadTbl()
            }
          }
        })
      }
    })
  }


  // FUNGSI reset form menggunakan ARROW FUNCTION JS
  const resetForm = () => {
    $('#act').val("add");
    $('#form_vendor')[0].reset();
    $('#form_vendor').validate().resetForm();
  }


  // FUNGSI loadTbl menggunakan ARROW FUNCTION JS
  const loadTbl = () => {
    $("#refrensi-table").dataTable().fnDraw();
  }


  // FUNGSI mengambil wilayah ARROW FUNCTION JS
  const getWil = (tingkat = 'prov', reg_parent_code = '', selectedWil = '') => {

    let parent = '';
    switch (tingkat) {
      case 'prov':
        parent = reg_parent_code.substr(0, 2);
        break;
      case 'kab':
        parent = reg_parent_code.substr(0, 4);
        break;
      case 'kec':
        parent = reg_parent_code.substr(0, 6);
        break;
        // case 'desa':
        //   parent = reg_parent_code;
        //   break;
      default:
        break;
    }

    $.ajax({
      cache: false,
      url: '<?= base_url() ?>penghantar_gi/get_wil',
      data: {
        parent: parent
      },
      dataType: 'json',
      type: 'post',
      success: res => {
        let opt = ''
        let selected = ''
        if (res.length > 0) {
          $.each(res, (index, i) => {
            if (selectedWil == i.reg_code) {
              selected = "selected"
            } else {
              selected = ""
            }
            opt += `<option value="${i.reg_code}" ${selected}>${i.reg_name}</option>`
          })
        }

        $('#' + tingkat + '_kode').html(opt)

        switch (tingkat) {
          case 'prov':
            getWil('kab', $('#' + tingkat + '_kode').val(), selectedWil)
            break;
          case 'kab':
            getWil('kec', $('#' + tingkat + '_kode').val(), selectedWil)
            break;

          default:
            break;
        }
      }
    })
  }


  // FUNGSI mengambil wilayah untuk filter ARROW FUNCTION JS
  const getWilFilter = (tingkat = 'prov', reg_parent_code = '', selectedWil = '') => {

    let parent = '';
    switch (tingkat) {
      case 'prov':
        parent = reg_parent_code.substr(0, 2);
        break;
      case 'kab':
        parent = reg_parent_code.substr(0, 4);
        break;
      default:
        break;
    }

    $.ajax({
      cache: false,
      url: '<?= base_url() ?>penghantar_gi/get_wil',
      data: {
        parent: parent
      },
      dataType: 'json',
      type: 'post',
      success: res => {
        let opt = ''
        let selected = ''
        if (res.length > 0) {
          $.each(res, (index, i) => {
            if (selectedWil == i.reg_code) {
              selected = "selected"
            } else {
              selected = ""
            }
            opt += `<option value="${i.reg_code}" ${selected}>${i.reg_name}</option>`
          })
        }

        $('#f_' + tingkat + '_kode').html(opt)
        $('#' + tingkat + '_kode').html(opt)

        switch (tingkat) {
          case 'prov':
            getWilFilter('kab', $('#f_' + tingkat + '_kode').val(), selectedWil)
            break;
          case 'kab':
            getWil('kec', $('#f_' + tingkat + '_kode').val(), selectedWil)
            loadTbl()
            break;

          default:
            break;
        }
      }
    })
  }


  $('#f_kondisi').change(function() {
    if ($(this).val() == '1') {
      $('#f_prov_kode').removeAttr('disabled', 'disabled')
      $('#f_kab_kode').removeAttr('disabled', 'disabled')
    } else {
      $('#f_prov_kode').attr('disabled', 'disabled')
      $('#f_kab_kode').attr('disabled', 'disabled')
    }
    loadTbl()
  })


  $('#f_prov_kode').change(function() {
    // $('#prov_kode').val($(this).val())
    getWilFilter('kab', $(this).val())
    loadTbl()
  })


  $('#f_kab_kode').change(function() {
    loadTbl()
  })


  $('#prov_kode').change(function() {
    getWil('kab', $(this).val())
  })


  $('#kab_kode').change(function() {
    getWil('kec', $(this).val())
  })
</script>