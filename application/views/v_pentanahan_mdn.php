<div class="row" id="formContainer" style="display: none;">
  <div class="col-lg-12">
    <div class="card shadow mb-4">
      <div class="card-header py-2 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 font-weight-bold text-primary">Form Master Pengukuran Pentanahan Madiun</h6>
        <button class="btn btn-primary btn-icon-split" id="btnListData">
          <span class="icon text-white-50">
            <i class="fas fa-database"></i>
          </span>
          <span class="text">List Data</span>
        </button>
      </div>
      <div class="card-body">
        <form action="" method="POST" enctype="multipart/form-data" id="form_vendor">
          <input type="hidden" name="act" id="act" value="add">
          <input type="hidden" name="id_pentanahan" id="id_pentanahan" value="">
          <div class="form-group row">
            <label class="control-label col-md-3">Gardu Induk</label>
            <div class="col-md-6">
              <select name="gi_id" id="gi_id" class="form-control">
                <?php foreach ($gi as $value) : ?>
                  <option value="<?= $value->gi_id ?>"><?= $value->gi_nama ?></option>
                <?php endforeach; ?>
              </select>
            </div>
          </div>
          <div class="form-group row">
            <label class="control-label col-md-3">Penghantar</label>
            <div class="col-md-6">
              <select name="penghantar_id" id="penghantar_id" class="form-control">
                <?php foreach ($penghantar as $value) : ?>
                  <option value="<?= $value->penghantar_id ?>"><?= $value->penghantar_nama ?></option>
                <?php endforeach; ?>
              </select>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">No Tower</label>
            <div class="col-md-6">
              <input type="text" name="no_tower" id="no_tower" class="form-control">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Tipe Tower</label>
            <div class="col-md-6">
              <input type="text" name="tipe_tower" id="tipe_tower" class="form-control">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Nilai Arde Terbesar</label>
            <div class="col-md-6">
              <input type="number" name="nilai_arde" id="nilai_arde" class="form-control">
            </div>
          </div>
          <div class="card mb-3">
            <div class="card-header">
              Hasil Pengukuran
            </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Nilai Arde & Tower (Paralel)</label>
            <div class="col-md-6">
              <input type="number" name="arde_tower" id="arde_tower" class="form-control">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Arde Kaki Tower A</label>
            <div class="col-md-6">
              <input type="number" name="arde_a" id="arde_a" class="form-control">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Arde Kaki Tower B</label>
            <div class="col-md-6">
              <input type="number" name="arde_b" id="arde_b" class="form-control">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Arde Kaki Tower C</label>
            <div class="col-md-6">
              <input type="number" name="arde_c" id="arde_c" class="form-control">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Arde Kaki Tower D</label>
            <div class="col-md-6">
              <input type="number" name="arde_d" id="arde_d" class="form-control">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label"> Tower </label>
            <div class="col-md-6">
              <input type="number" name="tower_arde" id="tower_arde" class="form-control">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">GSW Langsung</label>
            <div class="col-md-6">
              <input type="number" name="arde_gsw" id="arde_gsw" class="form-control">
            </div>
          </div>
        </div>
          <div class="card mb-3">
            <div class="card-header">
              Hasil Pengukuran Semen Konduktif
            </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Nilai Arde & Tower (Paralel)</label>
            <div class="col-md-6">
              <input type="number" name="semen_tower" id="semen_tower" class="form-control">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Arde Kaki Tower A</label>
            <div class="col-md-6">
              <input type="number" name="semen_a" id="semen_a" class="form-control">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Arde Kaki Tower B</label>
            <div class="col-md-6">
              <input type="number" name="semen_b" id="semen_b" class="form-control">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Arde Kaki Tower C</label>
            <div class="col-md-6">
              <input type="number" name="semen_c" id="semen_c" class="form-control">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Arde Kaki Tower D</label>
            <div class="col-md-6">
              <input type="number" name="semen_d" id="semen_d" class="form-control">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label"> Tower </label>
            <div class="col-md-6">
              <input type="number" name="tower_semen" id="tower_semen" class="form-control">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">GSW Langsung</label>
            <div class="col-md-6">
              <input type="number" name="semen_gsw" id="semen_gsw" class="form-control">
            </div>
          </div>
        </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Hasil Pengukuran</label>
            <div class="col-md-6">
              <input type="number" name="hasil" id="hasil" class="form-control" value="">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Metode Pemasangan</label>
            <div class="col-md-6">
              <input type="text" name="metode" id="metode" class="form-control" value="">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Anomali</label>
            <div class="col-md-6">
              <input type="text" name="anomali" id="anomali" class="form-control" value="">
            </div>
          </div>
          <div class="form-group row">
            <label class="control-label col-md-3">Tanggal Pengukuran</label>
            <div class="col-md-3">
              <input type="text" name="tgl" id="tgl" class="form-control tanggal" value="<?= date('d-m-Y') ?>">
            </div>
          </div>
           <div class="card mb-3">
            <div class="card-header">
              Keadaan Tanah Tower
            </div>
           <div class="form-group row">
            <label class="col-md-3 control-label">Tembok Penahan Tanah</label>
            <div class="col-md-6">
              <input type="text" name="tembok" id="tembok" class="form-control" value="">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Luas M2</label>
            <div class="col-md-6">
              <input type="number" name="luas" id="luas" class="form-control" value="">
            </div>
          </div>
          <div class="form-group row">
            <label class="control-label col-md-3">Kondisi Tanah</label>
            <div class="col-md-6">
              <select name="kondisi_tanah" id="kondisi_tanah" class="form-control">
                <option value="Basah">Basah</option>
                <option value="Kering">Kering</option>
                <option value="Padat">Padat</option>
              </select>
            </div>
          </div>
        </div>
         <div class="card mb-3">
            <div class="card-header">
              Keadaan Tanah Tower
            </div>
           <div class="form-group row">
            <label class="col-md-3 control-label">Desa</label>
            <div class="col-md-6">
              <input type="text" name="desa" id="desa" class="form-control" value="">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Kecamatan/Kabupaten</label>
            <div class="col-md-6">
              <input type="text" name="kecamatan" id="kecamatan" class="form-control" value="">
            </div>
          </div>
        </div>
            

        </form>
        <div class="box-footer">
          <div class="row">
            <div class="col-md-12 text-center">
              <button type="button" class="btn btn-primary" id="saveData">Save</button>
              <button type="button" class="btn btn-default" id="cancelData">Cancel</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row" id="tableContainer">
  <div class="col-lg-12">
    <div class="card shadow mb-4">
      <div class="card-header py-2 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 font-weight-bold text-primary">Master Group Pengukuran Pentanahan Madiun</h6>
        <button class="btn btn-primary btn-icon-split" id="btnFormData">
          <span class="icon text-white-50">
            <i class="fas fa-plus"></i>
          </span>
          <span class="text">Tambah</span>
        </button>
      </div>

      <div class="card-body">
        <div class="row mb-2">
          <div class="col-md-2">
            <a href="<?= base_url('pentanahan_mdn/form/') ?>" class="btn btn-success btn-icon-split" id="btnImport">
              <span class="icon text-white-50">
                <i class="fa fa-file-import"></i>
              </span>
              <span class="text">Import</span>
            </a>
          </div>
          <div class="col-md-2">
            <a href="<?= base_url('pentanahan_mdn/export/') ?>" class="btn btn-success btn-icon-split" id="btnExport">
              <span class="icon text-white-50">
                <i class="fa fa-file-export"></i>
              </span>
              <span class="text">Export</span>
            </a>
          </div>
          <div class="col-md-2">
            <button class="btn btn-danger btn-icon-split" id="btnDeleteData">
              <span class="icon text-white-50">
                <i class="fa fa-trash"></i>
              </span>
              <span class="text">Delete</span>
            </button>
          </div>
        </div>
        <div class="table-responsive">
          <table class="table table-bordered" id="refrensi-table" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th class="text-center">No</th>
                <th class="text-center">
                  <div class="form-check"><input class="form-check-input position-static" type="checkbox" id="checkSemua"></div>
                </th>
                 <th class="text-center">Tanggal Pengukuran</th>
                <th class="text-center">Gardu Induk</th>
                <th class="text-center">Penghantar</th>
                <th class="text-center">No Tower</th>
                <th class="text-center">Hasil Pengukuran</th>
                <th class="text-center">Metode Pemasangan</th>
                <th class="text-center">Anomali</th>
                <th class="text-center">Aksi</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  var tableAdvancedInit = function() {

    var initTable1 = function() {
      var target = '#refrensi-table';
      var oTable = $(target).dataTable({
        "displayStart": 0,
        "pageLength": 10,
        "lengthMenu": [
          [10, 25, 50, 100, -1],
          [10, 25, 50, 100, "All"]
        ],
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        // Load data for the table's content from an Ajax source
        "ajax": {
          "url": "<?php echo base_url('pentanahan_mdn/get_data') ?>",
          "dataType": "json",
          "type": "POST",
          // "data": function(d) {
          //   d.f_mmsi = $('#f_mmsi').val();
          // },
        },
        //Set column definition initialisation properties.
        columnDefs: [{
            "targets": [0, 1, -1, -2, -3, -4], //last column
            "orderable": false, //set not orderable
          },
          // {
          //   "targets": [1],
          //   'checkboxes': {
          //     'selectRow': true
          //   }
          // },
          {
            "targets": [0],
            "width": "5%",
            "className": "text-center"
          },
          {
            "targets": [1],
            "width": "5%",
            "className": "text-center"
          },
          {
            "targets": [2],
            "width": "13%"
          },
          {
            "targets": [3],
            "width": "5%"
          },
          {
            "targets": [4],
            "width": "10%"
          },
          {
            "targets": [5],
            "width": "6%",
            "className": "text-center"
          },
          {
            "targets": [6],
            "width": "8%",
            "className": "text-center"
          },
          {
            "targets": [7],
            "width": "16%"
          },
          {
            "targets": [8],
            "width": "22%"
          },
          {
            "targets": [9],
            "width": "10%",
            "className": "text-center"
          },
        ],
        "order": [
          [2, "asc"]
        ],

        "language": {
          // language settings
          "lengthMenu": "Display _MENU_ records",
          "search": "Search _INPUT_ <a class='btn default bts' href='javascript:void(0);'><i class='fa fa-search'></i></a>",
          "processing": '<img src="assets/plugins/global/images/owl.carousel/ajax-loader.gif"/><span>&nbsp;&nbsp;Loading...</span>',
          "infoEmpty": "No records found to show",
          "ajaxRequestGeneralError": "Could not complete request. Please check your internet connection",
          "emptyTable": "No data available in table",
          "zeroRecords": "No matching records found",
          "paginate": {
            "previous": "Prev",
            "next": "Next",
            "page": "Page",
            "pageOf": "of"
          }
        },
        "autoWidth": true, // disable fixed width and enable fluid table
        "orderCellsTop": true, // make sortable only the first row in thead
        "pagingType": "full_numbers", // pagination type(bootstrap, bootstrap_full_number or bootstrap_extended)
        // "fnInitComplete": function(oSettings, json) {
        //   inputCheckOnChange()
        // },
        // "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
        //   inputCheckOnChange()
        // }
      });

      jQuery(target + '_wrapper .dataTables_filter input').addClass("form-control input-small input-inline"); // modify table search input
      jQuery(target + '_wrapper .dataTables_length select').addClass("form-control input-small"); // modify table per page dropdown
      jQuery(target + '_wrapper .dataTables_length select').select2(); // initialize select2 dropdown
      jQuery(target + '_wrapper .dataTables_filter input').unbind();
      jQuery(target + '_wrapper .dataTables_filter input').bind('keyup', function(e) {
        if (e.keyCode == 13) {
          oTable.fnFilter(this.value);
        }
      });
      jQuery(target + '_wrapper .dataTables_filter a').bind('click', function(e) {
        var key = jQuery(target + '_wrapper .dataTables_filter input').val();
        oTable.fnFilter(key);
      });
    }

    return {
      // public functions
      init: function() {
        if (!jQuery().dataTable) {
          return;
        }
        initTable1();
      }
    };
  }();
  $(document).ready(function() {
    tableAdvancedInit.init()
    // inputCheckOnChange()


    $('#btnListData,#cancelData').click(function() {
      resetForm()
      $('#tableContainer').slideDown(500)
      $('#formContainer').slideUp(500)
    })


    $('#btnFormData').click(function() {
      resetForm()
      $('.input-doc').trigger('change')
      $('#tableContainer').slideUp(500)
      $('#formContainer').slideDown(500)
    })


    // #saveData on click
    $('#saveData').click(e => {
      $('#form_vendor').submit()
    })


    // validasi form_vendor
    $("#form_vendor").validate({
      // define validation rules
      rules: {
        foto1: {
          filesize: 1000000,
          extension: "png|jpg|jpeg|gif"
        },
        foto2: {
          filesize: 1000000,
          extension: "png|jpg|jpeg|gif"
        },
        foto3: {
          filesize: 1000000,
          extension: "png|jpg|jpeg|gif"
        },
        foto4: {
          filesize: 1000000,
          extension: "png|jpg|jpeg|gif"
        },
      },
      messages: {
        foto1: {
          filesize: "Ukuran File Tidak boleh melebihi 1MB"
        },
        foto2: {
          filesize: "Ukuran File Tidak boleh melebihi 1MB"
        },
        foto3: {
          filesize: "Ukuran File Tidak boleh melebihi 1MB"
        },
        foto4: {
          filesize: "Ukuran File Tidak boleh melebihi 1MB"
        },
      },
      submitHandler: function(form) {
        $('#saveData').attr('disabled', true).text("Loading...");
        const formData = new FormData();
        const params = $('#form_vendor').serializeArray()
        const files = $('#form_vendor').find('.custom-file-input');

        $.each(files, (i, item) => {
          formData.append($(item).attr('name'), $(item)[0].files[0])
        })
        $.each(params, (i, item) => {
          formData.append(item.name, item.value)
        })
        formData.append('gi', $('#gi_id option:selected').text())
        formData.append('penghantar', $('#penghantar_id option:selected').text())
        $.ajax({
          url: '<?php echo base_url('pentanahan_mdn/save') ?>',
          type: "POST",
          data: formData,
          dataType: "json",
          cache: false,
          mimeType: "multipart/form-data",
          success: function(response, status, xhr, $form) {
            if (response > 0) {
              $('#saveData').attr('disabled', false).text("Save");
              $("#cancelData").click();
              Swal.fire({
                icon: 'success',
                title: 'Success!',
                text: 'Data Berhasil Disimpan!',
                showConfirmButton: false,
                timer: 1500
              })
              loadTbl();
            }
          },
          contentType: false,
          processData: false
        }, 'json');
      }
    });
  });


  // FUNGSI edit menggunakan ARROW FUNCTION JS
  const set_val = id => {
    resetForm();
    $('.input-doc').trigger('change')
    $('#act').val("edit");
    $.ajax({
      url: '<?= base_url() ?>pentanahan_mdn/edit/' + id,
      dataType: 'json',
      cache: false,
      success: res => {
        console.log(res);
        $('#id_pentanahan').val(res.id_pentanahan);
        $('#tgl').val(res.tgl);
        $('#gi_id').val(res.gi_id);
        $('#penghantar_id').val(res.penghantar_id);
        $('#no_tower').val(res.no_tower);
        $('#tipe_tower').val(res.tipe_tower);
        $('#nilai_arde').val(res.nilai_arde);
        $('#arde_tower').val(res.arde_tower);
        $('#arde_a').val(res.arde_a);
        $('#arde_b').val(res.arde_b);
        $('#arde_c').val(res.arde_c);
        $('#arde_d').val(res.arde_d);
        $('#tower_arde').val(res.tower_arde);
        $('#arde_gsw').val(res.arde_gsw);
        $('#semen_tower').val(res.semen_tower);
        $('#semen_a').val(res.semen_a);
        $('#semen_b').val(res.semen_b);
        $('#semen_c').val(res.semen_c);
        $('#semen_d').val(res.semen_d);
        $('#tower_semen').val(res.tower_semen);
        $('#semen_gsw').val(res.semen_gsw);
        $('#hasil').val(res.hasil);
        $('#metode').val(res.metode);
        $('#anomali').val(res.anomali);
        $('#tembok').val(res.tembok);
        $('#luas').val(res.luas);
        $('#kondisi_tanah').val(res.kondisi_tanah);
        $('#desa').val(res.desa);
        $('#kecamatan').val(res.kecamatan);


      }
    })
    $('#tableContainer').slideUp(500)
    $('#formContainer').slideDown(500)
  }


  // FUNGSI delete menggunakan ARROW FUNCTION JS
  const del = id => {
    Swal.fire({
      title: 'Apa Anda yakin menghapus Data ini?',
      text: "Data yang terhapus tidak bisa dikemmdnkan!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Ya, Hapus!',
      cancelButtonText: 'Tidak, Batal!',
    }).then((result) => {
      if (result.isConfirmed) {
        $.ajax({
          url: '<?= base_url() ?>pentanahan_mdn/delete/' + id,
          cache: false,
          dataType: 'json',
          success: res => {
            if (res) {
              Swal.fire(
                'Deleted!',
                'Data telah dihapus.',
                'success'
              )
              loadTbl()
            }
          }
        })
      }
    })
  }


  // FUNGSI reset form menggunakan ARROW FUNCTION JS
  const resetForm = () => {
    $('#act').val("add");
    $('#form_vendor')[0].reset();
    $('#form_vendor').validate().resetForm();
    $('.custom-file-label').html("Pilih file");
  }


  // FUNGSI loadTbl menggunakan ARROW FUNCTION JS
  const loadTbl = () => {
    $("#refrensi-table").dataTable().fnDraw();
    // inputCheckOnChange()
  }

  $(".tanggal").datepicker({
    autoclose: true,
    dateFormat: "dd-mm-yy"
  })

  $('.custom-file-input').on('change', function() {
    //get the file name
    var fileName = $(this).val().replace('C:\\fakepath\\', " ");
    //replace the "Choose a file" label
    $(this).parent().find('.custom-file-label').html(fileName);
    $('#form_vendor').validate().resetForm();
    $('#form_vendor').validate().element($(this));
  })

  $('.input-doc').on('change', function() {
    if ($(this).val() == "OK") {
      $('#' + $(this).data('id-file')).show()
    } else {
      $('#' + $(this).data('id-file')).hide()
    }
  })


  // FUNGSI input checkbox ON change
  const inputCheckOnChange = e => {
    const banyakData = $('.input-check').length
    const banyakCentang = $('.input-check:checked').length

    if (banyakCentang == banyakData) {
      $('#checkSemua').prop('checked', true)
    } else {
      $('#checkSemua').prop('checked', false)
    }
  }


  // input #checkSemua on change
  $('#checkSemua').change(function() {
    if ($(this).prop('checked')) {
      $('.input-check').prop('checked', true)
    } else {
      $('.input-check').prop('checked', false)
    }
  })


  // #btnDeleteData on click
  $('#btnDeleteData').click(function() {
    if ($('.input-check:checked').length > 0) {
      Swal.fire({
        title: 'Apa Anda yakin menghapus Data ini?',
        text: "Data yang terhapus tidak bisa dikemmdnkan!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya, Hapus!',
        cancelButtonText: 'Tidak, Batal!',
      }).then((result) => {
        if (result.isConfirmed) {
          const data = [];
          $('.input-check:checked').each(function(index, item) {
            data.push($(item).data('id'))
          })

          $.ajax({
            url: "<?= base_url() ?>pentanahan_mdn/delete_some",
            type: 'post',
            dataType: 'json',
            data: {
              id_pentanahan: data
            },
            success: res => {
              if (res > 0) {
                Swal.fire({
                  title: 'Sukses',
                  text: "Beberapa data berhasil dihapus!",
                  icon: 'success',
                })
                loadTbl()
              }
            }
          })
        }
      })
    } else {
      Swal.fire({
        title: 'Error',
        text: "Pilih minimal satu data yang ingin dihapus!",
        icon: 'error',
      })
    }
  })
</script>