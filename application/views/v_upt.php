<div class="row" id="formContainer" style="display: none;">
  <div class="col-lg-12">
    <div class="card shadow mb-4">
      <div class="card-header py-2 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 font-weight-bold text-primary">Form Master UPT</h6>
        <button class="btn btn-primary btn-icon-split" id="btnListData">
          <span class="icon text-white-50">
            <i class="fas fa-database"></i>
          </span>
          <span class="text">List Data</span>
        </button>
      </div>
      <div class="card-body">
        <form action="" method="POST" enctype="multipart/form-data" id="form_vendor">
          <input type="hidden" name="act" id="act" value="add">
          <input type="hidden" name="upt_id" id="upt_id" value="">
          <div class="form-group row">
            <label class="col-md-3 control-label">Kode UPT</label>
            <div class="col-md-6">
              <input type="text" name="upt_kode" id="upt_kode" class="form-control">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Nama UPT</label>
            <div class="col-md-6">
              <input type="text" name="upt_nama" id="upt_nama" class="form-control">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Status</label>
            <div class="col-md-6">
              <select name="upt_status" id="upt_status" class="form-control">
                <option value="1">Aktif</option>
                <option value="0">Non Aktif</option>
              </select>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Daerah yang Diampu</label>
            <div class="col-md-8">
              <button type="button" class="btn btn-info float-right" id="btnTambahBaris"><i class="fa fa-plus"></i> Tambah</button>
            </div>
          </div>
          <div class="row">
            <div class="col-md-11">
              <table class="table table-hover table-striped table-bordered" id="tableDaerah">
                <thead>
                  <tr>
                    <th class="text-center">No</th>
                    <th class="text-center">Nama Daerah</th>
                    <th class="text-center">Hapus</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td class='text-center' colspan='3'>Belum Ada Data Daerah</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </form>
        <div class="box-footer">
          <div class="row">
            <div class="col-md-12 text-center">
              <button type="button" class="btn btn-primary" id="saveData">Save</button>
              <button type="button" class="btn btn-default" id="cancelData">Cancel</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row" id="tableContainer">
  <div class="col-lg-12">
    <div class="card shadow mb-4">
      <div class="card-header py-2 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 font-weight-bold text-primary">Master UPT</h6>
        <button class="btn btn-primary btn-icon-split" id="btnFormData">
          <span class="icon text-white-50">
            <i class="fas fa-plus"></i>
          </span>
          <span class="text">Tambah</span>
        </button>
      </div>

      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered" id="refrensi-table" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th class="text-center">No</th>
                <th class="text-center">Kode UPT</th>
                <th class="text-center">Nama UPT</th>
                <th class="text-center">Status</th>
                <th class="text-center">Aksi</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  var tableAdvancedInit = function() {

    var initTable1 = function() {
      var target = '#refrensi-table';
      var oTable = $(target).dataTable({
        "displayStart": 0,
        "pageLength": 10,
        "lengthMenu": [
          [10, 25, 50, 100, -1],
          [10, 25, 50, 100, "All"]
        ],
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        // Load data for the table's content from an Ajax source
        "ajax": {
          "url": "<?php echo base_url('upt/get_data') ?>",
          "dataType": "json",
          "type": "POST",
        },
        //Set column definition initialisation properties.
        columnDefs: [{
            "targets": [0, -1], //last column
            "orderable": false, //set not orderable
          },
          {
            "targets": [0],
            "width": "10%",
            "className": "text-center"
          },
          {
            "targets": [1],
            "width": "10%"
          },
          {
            "targets": [2],
            "width": "60%"
          },
          {
            "targets": [3],
            "width": "10%",
            "className": "text-center"
          },
          {
            "targets": [4],
            "width": "10%",
            "className": "text-center"
          },

        ],
        "order": [
          [1, "asc"]
        ],

        "language": {
          // language settings
          "lengthMenu": "Display _MENU_ records",
          "search": "Search _INPUT_ <a class='btn default bts' href='javascript:void(0);'><i class='fa fa-search'></i></a>",
          "processing": '<img src="assets/plugins/global/images/owl.carousel/ajax-loader.gif"/><span>&nbsp;&nbsp;Loading...</span>',
          "infoEmpty": "No records found to show",
          "ajaxRequestGeneralError": "Could not complete request. Please check your internet connection",
          "emptyTable": "No data available in table",
          "zeroRecords": "No matching records found",
          "paginate": {
            "previous": "Prev",
            "next": "Next",
            "page": "Page",
            "pageOf": "of"
          }
        },
        "autoWidth": true, // disable fixed width and enable fluid table
        "orderCellsTop": true, // make sortable only the first row in thead
        "pagingType": "full_numbers", // pagination type(bootstrap, bootstrap_full_number or bootstrap_extended)
      });

      jQuery(target + '_wrapper .dataTables_filter input').addClass("form-control input-small input-inline"); // modify table search input
      jQuery(target + '_wrapper .dataTables_length select').addClass("form-control input-small"); // modify table per page dropdown
      jQuery(target + '_wrapper .dataTables_length select').select2(); // initialize select2 dropdown
      jQuery(target + '_wrapper .dataTables_filter input').unbind();
      jQuery(target + '_wrapper .dataTables_filter input').bind('keyup', function(e) {
        if (e.keyCode == 13) {
          oTable.fnFilter(this.value);
        }
      });
      jQuery(target + '_wrapper .dataTables_filter a').bind('click', function(e) {
        var key = jQuery(target + '_wrapper .dataTables_filter input').val();
        oTable.fnFilter(key);
      });
    }

    return {
      // public functions
      init: function() {
        if (!jQuery().dataTable) {
          return;
        }
        initTable1();
      }
    };
  }();
  $(document).ready(function() {
    tableAdvancedInit.init()


    $('#btnListData,#cancelData').click(function() {
      resetForm()
      $('#tableContainer').slideDown(500)
      $('#formContainer').slideUp(500)
    })


    $('#btnFormData').click(function() {
      resetForm()
      $('#tableContainer').slideUp(500)
      $('#formContainer').slideDown(500)
    })


    // #saveData on click
    $('#saveData').click(e => {
      e.preventDefault();
      $('#form_vendor').submit()
    })


    // validasi form_vendor
    $("#form_vendor").validate({
      rules: {
        upt_kode: {
          required: true
        },
        upt_nama: {
          required: true
        },
      },
      // define validation rules
      submitHandler: function(form) {
        $('#saveData').attr('disabled', true).text("Loading...");

        const formData = $("#form_vendor").serialize()

        $.ajax({
          url: '<?php echo base_url('upt/save') ?>',
          type: "POST",
          data: formData,
          dataType: "json",
          cache: false,
          success: function(response, status, xhr, $form) {
            if (response > 0) {
              $('#saveData').attr('disabled', false).text("Save");
              $("#cancelData").click();
              Swal.fire({
                icon: 'success',
                title: 'Success!',
                text: 'Data Berhasil Disimpan!',
                showConfirmButton: false,
                timer: 1500
              })
              loadTbl();
            }
          },
        }, 'json');
      }
    });
  });


  // FUNGSI edit menggunakan ARROW FUNCTION JS
  const set_val = id => {
    resetForm();
    $('#act').val("edit");
    $.ajax({
      url: '<?= base_url() ?>upt/edit/' + id,
      dataType: 'json',
      cache: false,
      success: res => {
        $('#upt_id').val(res.upt_id);
        $('#upt_kode').val(res.upt_kode);
        $('#upt_nama').val(res.upt_nama);
        $('#upt_status').val(res.upt_status);
        if (res.upt_data.length > 0) {
          $('#tableDaerah tbody').html(`<tr>
              <td class="text-center" colspan="3">Loading...</td>
            </tr>
          `)
          let tr = ''
          let no = 1
          $.each(res.upt_data, function(index, item) {
            tr += `<tr>
              <td class='text-center'>${no++}</td>
              <td>
                <select name="kab_kode[]" class="form-control daerah">
                  <option value="${item.reg_code}">${item.reg_name}</option>
                </select>
              </td>
              <td class='text-center'>
                <button type="button" onclick="hapusBaris(this)" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>
              </td>
            </tr>`
          })

          $('#tableDaerah tbody').html(tr)
        }
      }
    })
    $('#tableContainer').slideUp(500)
    $('#formContainer').slideDown(500)
  }


  // FUNGSI delete menggunakan ARROW FUNCTION JS
  const del = id => {
    Swal.fire({
      title: 'Apa Anda yakin menghapus Data ini?',
      text: "Data yang terhapus tidak bisa dikembalikan!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Ya, Hapus!',
      cancelButtonText: 'Tidak, Batal!',
    }).then((result) => {
      if (result.isConfirmed) {
        $.ajax({
          url: '<?= base_url() ?>upt/delete/' + id,
          cache: false,
          dataType: 'json',
          success: res => {
            if (res) {
              Swal.fire(
                'Deleted!',
                'Data telah dihapus.',
                'success'
              )
              loadTbl()
            }
          }
        })
      }
    })
  }


  // FUNGSI reset form menggunakan ARROW FUNCTION JS
  const resetForm = () => {
    $('#act').val("add");
    $('#form_vendor')[0].reset();
    $('#form_vendor').validate().resetForm();
    const tr = "<tr><td class='text-center' colspan='3'>Belum Ada Data Daerah</td></tr>"
    $('#tableDaerah tbody').html(tr)
    $('#saveData').attr('disabled', false).text("Save");
  }


  // FUNGSI loadTbl menggunakan ARROW FUNCTION JS
  const loadTbl = () => {
    $("#refrensi-table").dataTable().fnDraw();
  }


  // FUNGSI hapus baris Tabel daerah
  const hapusBaris = e => {
    $(e).closest('tr').remove()
    if ($('#tableDaerah tbody tr').length == 0) {
      const tr = "<tr><td class='text-center' colspan='3'>Belum Ada Data Daerah</td></tr>"
      $('#tableDaerah tbody').html(tr)
    }
  }


  // FUNGSI tambah baris Tabel daerah
  const tambahBaris = () => {
    if ($('#tableDaerah tbody tr td').attr('colspan') == '3') {
      $('#tableDaerah tbody tr').remove()
    }
    const tr = `<tr>
      <td class='text-center'>${$('#tableDaerah tbody tr').length + 1}</td>
      <td><select name="kab_kode[]" class="form-control daerah"></select></td>
      <td class='text-center'>
        <button type="button" onclick="hapusBaris(this)" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>
      </td>
    </tr>`
    $('#tableDaerah tbody').append(tr)
    cariDaerah()
  }


  // btnTambahBaris on click
  $('#btnTambahBaris').click(function() {
    tambahBaris()
  })

  const cariDaerah = () => {
    $(".daerah").select2({
      ajax: {
        url: "<?= base_url() ?>upt/get_daerah",
        dataType: 'json',
        delay: 250,
        data: function(params) {
          return {
            q: params.term
          };
        },
        type: 'post',
        processResults: function(data) {
          return {
            results: data
          };
        },
        cache: true
      },
      placeholder: 'Ketikkan nama Kota / Kabupaten',
      minimumInputLength: 2,
    });
    $(".daerah").last().select2('open')
  }
</script>